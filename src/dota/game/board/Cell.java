package dota.game.board;

import dota.game.objects.alive.AliveObject;
import dota.game.objects.map.GoldMine;
import dota.utils.Coordinate;

import java.util.Vector;

public class Cell {
    private final Coordinate coordinate;
    private Vector<AliveObject> aliveObjects = new Vector<>();

    private GoldMine goldMine;

    public Cell(int x, int y) {
        this.coordinate = new Coordinate(x, y);
    }

    public Cell(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    /**
     * @return coordinate of the cell in board
     */
    public Coordinate getCoordinate() {
        return coordinate;
    }

    /**
     * @return number of row of the cell in board
     */
    public int getX() {
        return coordinate.getX();
    }

    /**
     * @return number of the column of the cell in board
     */
    public int getY() {
        return coordinate.getY();
    }

    /**
     * @return copy of the AliveObjects as performance issues
     */
    public AliveObject[] getAliveObjects() {
        return aliveObjects.toArray(new AliveObject[0]);
    }

    /**
     * function used to update alive object
     *
     * @param aliveObject alive object to add to the cell
     * @throws RuntimeException the alive object given must be alive and not dead!
     */
    public void addAliveObject(AliveObject aliveObject) throws RuntimeException {
        if (aliveObject.isDead())
            throw new RuntimeException("Added a deadObject instead of an aliveObject to the cell");
        this.aliveObjects.add(aliveObject);
    }

    /**
     * function to calculate distance from this cell
     *
     * @param cell the cell from which the distance is inquired
     * @return the distance as an integer
     */
    public int distanceFrom(Cell cell) {
        return this.distanceFrom(cell.coordinate);
    }

    /**
     * function to calculate distance from this cell
     *
     * @param coordinate the point from which the distance is inquired
     * @return the distance as an integer
     */
    public int distanceFrom(Coordinate coordinate) {
        return this.coordinate.maxDxDyDistance(coordinate);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cell cell = (Cell) o;

        if (!coordinate.equals(cell.coordinate)) return false;
        return aliveObjects.equals(cell.aliveObjects);
    }

    @Override
    public String toString() {
        return super.toString() + " X " + getX() + " Y " + getY();
    }

    /**
     * remove a list of AliveObjects away from the AliveObjects in the cell
     *
     * @param aliveObjects the list to remove from all
     */
    public void removeAllAliveObjects(Vector<AliveObject> aliveObjects) {
        this.aliveObjects.removeAll(aliveObjects);
    }

    public GoldMine getGoldMine() {
        return goldMine;
    }

    /**
     * @param goldMine the gold mine to place in the cells
     */
    public void setGoldMine(GoldMine goldMine) {
        this.goldMine = goldMine;
    }
}
