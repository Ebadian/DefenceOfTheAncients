package dota.game.board;

import dota.game.objects.alive.buildings.Ancient;
import dota.game.objects.alive.buildings.Barrack;
import dota.game.objects.map.GoldMine;
import dota.game.objects.map.Lane;
import dota.game.objects.map.Path;
import dota.game.utils.location.Direction;
import dota.utils.Coordinate;

import java.util.ArrayList;
import java.util.Vector;

public class Board {
    private Cell[][] cells;
    private Path[] paths;
    private Vector<GoldMine> goldMines = new Vector<>();
    private Team[] teams;

    /**
     * constructing board by giving it the dimensions of the rectangle and creating respective cells
     *
     * @param rows    number of rows of the map
     * @param columns number of the columns of the map
     */
    public Board(int rows, int columns) {
        cells = new Cell[rows][columns];
        for (int i = 0; i < rows; i++)
            for (int j = 0; j < columns; j++)
                cells[i][j] = new Cell(i, j);
        teams = new Team[2];
    }

    /**
     * this function should be called after the creation of the board
     *
     * @param index         index of the corresponding new team
     * @param teamID        each team should have a unique ID
     * @param ancientCells  list of cells of the ancient
     * @param barracksCells list of barracks
     */
    public void createTeam(int index, int teamID, Cell[][] ancientCells, ArrayList<Cell[][]> barracksCells) {
        Team team = new Team(teamID, ancientCells, barracksCells);
        teams[index] = team;
    }

    public int getRows() {
        return cells.length;
    }

    public int getColumns() {
        return cells[0].length;
    }

    /**
     * function used to add goldmines to map
     *
     * @param coordinate place to put new gold mine
     */
    public void addGoldMineByCoordinate(Coordinate coordinate) {
        GoldMine goldMine = new GoldMine(getCell(coordinate));
        getCell(coordinate).setGoldMine(goldMine);
        goldMines.add(goldMine);
    }

    /**
     * getting a gold mine by its index
     *
     * @param index index of the requested gold mine
     * @return the corresponding gold mine
     */
    public GoldMine getGoldMine(int index) {
        return goldMines.elementAt(index);
    }

    /**
     * new array created to maintain encapsulation
     */
    public GoldMine[] getGoldMines() {
        return goldMines.toArray(new GoldMine[0]);
    }

    public Team getTeam(int teamIndex) {
        return teams[teamIndex];
    }

    /**
     * new array created to maintain encapsulation
     */
    public Team[] getTeams() {
        return teams.clone();
    }

    /**
     * @param teamIndex index of the team requested the ancient of it
     * @return the ancient of the requested team id
     */
    public Ancient getTeamAncient(int teamIndex) {
        return teams[teamIndex].getBase().getAncient();
    }

    /**
     * @param teamIndex index of the team requested the barracks of it
     * @return the barrack of the requested team id
     */
    public Barrack[] getTeamBarracks(int teamIndex) {
        return teams[teamIndex].getBase().getBarracks();
    }

    public Path getPath(int pathIndex) {
        return paths[pathIndex];
    }

    public Path[] getPaths() {
        return paths;
    }

    /**
     * this function should be called after the creation of the board
     *
     * @param paths the paths to assign to map
     */
    public void setPaths(Path[] paths) {
        this.paths = paths;
    }

    public Lane[] getPathsLanes(int pathIndex) {
        return paths[pathIndex].getLanes();
    }

    /**
     * this function return array of cell corresponding to the coordinate given
     *
     * @return cell of the coordinate
     */
    public Cell getCell(int x, int y) {
        return cells[x][y];
    }

    /**
     * this function return array of cells corresponding to the coordinates given
     *
     * @param coordinates array of coordinates to get corresponding cells
     * @return cells of the coordinates in the same order
     */
    public Cell[] convertToCells(Coordinate[] coordinates) {
        Cell[] cells = new Cell[coordinates.length];

        for (int i = 0; i < coordinates.length; i++)
            cells[i] = getCell(coordinates[i]);

        return cells;
    }

    /**
     * this function return array of cells corresponding to the coordinates given
     *
     * @param coordinates array of coordinates to get corresponding cells
     * @return cells of the coordinates in the same order
     */
    public Cell[][] convertToCells(Coordinate[][] coordinates) {
        Cell[][] cells = new Cell[coordinates.length][];

        for (int i = 0; i < coordinates.length; i++)
            cells[i] = convertToCells(coordinates[i]);

        return cells;
    }

    /**
     * this function return array of cell corresponding to the coordinate given
     *
     * @param coordinate coordinate to get corresponding cell
     * @return cell of the coordinate
     */
    public Cell getCell(Coordinate coordinate) {
        return getCell(coordinate.getX(), coordinate.getY());
    }

    public Cell[] getVisibleCells(Cell cell, int rangeSight) {
        return getVisibleCells(cell.getCoordinate(), rangeSight);
    }

    /**
     * this function calculates the cells visible to the coordinate given
     *
     * @param coordinate the center from where we calculate visible cells
     * @param rangeSight the range sight visible from the center
     * @return array of cells which are visible from the center
     */
    public Cell[] getVisibleCells(Coordinate coordinate, int rangeSight) {
        Vector<Cell> visibleCells = new Vector<>();

        for (Cell rows[] : cells)
            for (Cell cell : rows)
                if (cell.distanceFrom(coordinate) <= rangeSight)
                    visibleCells.add(cell);

        return visibleCells.toArray(new Cell[0]);
    }

    /**
     * simple checking of presence of the coordinate in maps
     *
     * @param coordinate the coordinate to check in map
     * @return whether the coordinate is in map or not
     */
    public boolean isInMap(Coordinate coordinate) {
        return (coordinate.getX() >= 0 && coordinate.getX() < getRows() &&
                coordinate.getY() >= 0 & coordinate.getY() < getColumns());
    }

    /**
     * getting the adjacent cell
     *
     * @param cell      from which we calculate the adjacent cell
     * @param direction defining which neighbor of the cell we want
     * @return corresponding adjacent cell
     */
    public Cell getAdjCell(Cell cell, Direction direction) {
        Coordinate coordinate = cell.getCoordinate().plus(new Coordinate(direction.getX(), direction.getY()));

        if (!isInMap(coordinate))
            throw new RuntimeException("Adj cell out of bounds");

        return getCell(coordinate);
    }

    public Cell[][] convertToCells() {
        return cells;
    }
}
