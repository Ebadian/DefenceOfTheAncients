package dota.game.board;

import dota.game.defaultobjects.buildings.DefaultBuildings;
import dota.game.defaultobjects.gameforces.DefaultAttackerUnits;
import dota.game.defaultobjects.gameforces.DefaultDefenceTowers;
import dota.game.defaultobjects.gameforces.DefaultHeros;
import dota.game.objects.alive.buildings.Ancient;
import dota.game.objects.alive.buildings.Barrack;
import dota.game.objects.alive.forces.AttackerUnit;
import dota.game.objects.alive.forces.DefenceTower;
import dota.game.objects.alive.forces.Hero;
import dota.game.settings.General;
import dota.game.settings.WorthConstants;
import dota.game.utils.location.LaneDirection;
import dota.utils.types.AttackerUnitTypes;
import dota.utils.types.DefenceTowerTypes;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Logic verison of Team
 */
public class Team {
    private final int teamID;
    private final LaneDirection attackerUnitLaneDirection;
    private Base base;
    private HashMap<AttackerUnitTypes, AttackerUnit> availableAttackerUnits = new HashMap<>();
    private HashMap<DefenceTowerTypes, DefenceTower> availableDefenceTowers = new HashMap<>();
    private Hero hero;
    private int money = WorthConstants.BASE_TEAM_MONEY;

    /**
     *
     * @param teamID ID of the team
     * @param ancientCells Cells where team's Ancient is placed in
     * @param barracksCells Cells where team's Barracks ara placed in
     */
    public Team(int teamID, Cell[][] ancientCells, ArrayList<Cell[][]> barracksCells) {
        this.teamID = teamID;

        Barrack[] barracks = new Barrack[barracksCells.size()];
        for (int i = 0; i < barracksCells.size(); i++)
            barracks[i] = new Barrack(DefaultBuildings.BARRACK, this, barracksCells.get(i));
        this.base = new Base(new Ancient(DefaultBuildings.ANCIENT, this, ancientCells), barracks);

        if (teamID == General.SENTINEL) {
            attackerUnitLaneDirection = LaneDirection.FORWARDS;
            hero = new Hero(DefaultHeros.TINY, this);
        } else {
            attackerUnitLaneDirection = LaneDirection.BACKWARDS;
            hero = new Hero(DefaultHeros.VENOMANCER, this);
        }

        setAvailableForces();
    }

    public LaneDirection getAttackerUnitLaneDirection() {
        return attackerUnitLaneDirection;
    }

    private void setAvailableForces() {
        availableAttackerUnits.put(AttackerUnitTypes.INFANTRY, DefaultAttackerUnits.INFANTRY);
        availableAttackerUnits.put(AttackerUnitTypes.CAVALRY, DefaultAttackerUnits.CAVALRY);

        if (teamID == General.SENTINEL) {
            availableDefenceTowers.put(DefenceTowerTypes.FIRE_TOWER, DefaultDefenceTowers.FIRE_TOWER);
            availableDefenceTowers.put(DefenceTowerTypes.STONE_TOWER, DefaultDefenceTowers.STONE_TOWER);
        } else {
            availableDefenceTowers.put(DefenceTowerTypes.BLACK_TOWER, DefaultDefenceTowers.BLACK_TOWER);
            availableDefenceTowers.put(DefenceTowerTypes.POISON_TOWER, DefaultDefenceTowers.POISON_TOWER);
        }
    }

    /**
     *
     * @return default Cell where team's Heros spawn in
     */
    public Cell getHeroDefaultPlace() {
        return base.getAncient().getCenterCell();
    }

    public int getTeamID() {
        return teamID;
    }

    /**
     *
     * @return available AttackerUnit if AttackerUnitType type
     */
    public AttackerUnit getAvailableAttackerUnit(AttackerUnitTypes type) {
        return new AttackerUnit(availableAttackerUnits.get(type));
    }

    /**
     *
     * @return available DefenceTowerUnit if DefenceTowerType type
     */
    public DefenceTower getAvailableDefenceTower(DefenceTowerTypes type) {
//        System.out.println("Checking towers : " + availableDefenceTowers);
        return new DefenceTower(availableDefenceTowers.get(type));
    }

    public void setAvailableAttackerUnit(AttackerUnitTypes attackerUnitType, AttackerUnit attackerUnit) {
        availableAttackerUnits.put(attackerUnitType, attackerUnit);
    }

    public void setAvailableDefenceTower(DefenceTowerTypes defenceTowerType, DefenceTower defenceTower) {
        availableDefenceTowers.put(defenceTowerType, defenceTower);
    }

    /**
     *
     * @param money the amount money which should be added to team's money
     * @throws Exception if money will get negative after the adding
     */
    public void addMoney(int money) throws Exception {
        if (this.money + money < 0)
            throw new Exception("No team is allowed to use more money than it has");
        this.money += money;
    }

    public int getMoney() {
        return money;
    }

    public Hero getHero() {
        return hero;
    }

    /**
     *
     * @return team's base
     */
    public Base getBase() {
        return base;
    }
}
