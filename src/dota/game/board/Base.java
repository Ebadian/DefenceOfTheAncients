package dota.game.board;

import dota.game.objects.alive.buildings.Ancient;
import dota.game.objects.alive.buildings.Barrack;

public class Base {
    private Ancient ancient;
    private Barrack[] barracks;

    /**
     * @param ancient  ancient building of a team
     * @param barracks array of barracks of a team
     */
    public Base(Ancient ancient, Barrack[] barracks) {
        this.ancient = ancient;
        this.barracks = barracks;
    }

    public Ancient getAncient() {
        return ancient;
    }

    /**
     * @param index index of the barrack needed
     * @return the corresponding barrack to the with the right index
     */
    public Barrack getBarrack(int index) {
        return barracks[index];
    }

    public Barrack[] getBarracks() {
        return barracks;
    }
}
