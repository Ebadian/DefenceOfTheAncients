package dota.game.settings;

/**
 * Constants related to money handling
 */
public class WorthConstants {
    public static final int BASE_TEAM_MONEY = 5000;
    public static final int GOLD_MINE_MONEY = 100;
    public static final int MONEY_ADDED_PER_SECOND = 10;
    public static final double VALUE_COEF = 0.8;
}
