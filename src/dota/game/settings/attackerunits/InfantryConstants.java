package dota.game.settings.attackerunits;

/**
 * Constants to create attacker unit of type infantry as default
 */
public class InfantryConstants {
    public static final int SPEED = 500;
    public static final int COST = 10;
    public static final int RANGE_SIGHT = 4;
    public static final int RELOAD_TIME = 200;
    public static final int DAMAGE = 20;
    public static final int HP = 400;

}
