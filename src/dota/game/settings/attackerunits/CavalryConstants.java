package dota.game.settings.attackerunits;

/**
 * Constants to create attacker unit of type cavalry as default
 */
public class CavalryConstants {
    public static final int SPEED = 500;
    public static final int COST = 40;
    public static final int RANGE_SIGHT = 6;
    public static final int RELOAD_TIME = 500;
    public static final int DAMAGE = 100;
    public static final int HP = 1000;

}
