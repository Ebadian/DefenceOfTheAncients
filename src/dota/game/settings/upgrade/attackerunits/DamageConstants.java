package dota.game.settings.upgrade.attackerunits;

public class DamageConstants {
    public static final Integer MAX_LEVEL = null;
    public static final int COST = 1000;
    public static final double HP_ADD_COEF = 0.1;
    public static final double VALUE_ADD_COEF = 0.05;
}
