package dota.game.settings.upgrade.attackerunits;

public class HealthConstants {
    public static final Integer MAX_LEVEL = null;
    public static final int COST = 500;
    public static final int HP_ADD = 5;
    public static final double VALUE_ADD_COEF = 0.05;
}
