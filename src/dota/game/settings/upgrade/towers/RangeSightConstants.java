package dota.game.settings.upgrade.towers;

public class RangeSightConstants {
    public static final Integer MAX_LEVEL = 3;
    public static final double COST_COEF = 0.2;
    public static final int RANGE_SIGHT_ADD = 1;
    public static final double VALUE_ADD_COEF = 0.2;
}
