package dota.game.settings.upgrade.towers;

public class DamageConstants {
    public static final Integer MAX_LEVEL = null;
    public static final double COST_COEF = 0.15;
    public static final double HP_ADD_COEF = 0.1;
    public static final double VALUE_ADD_COEF = 0.15;
}
