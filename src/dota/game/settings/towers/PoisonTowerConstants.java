package dota.game.settings.towers;

import dota.game.settings.General;

/**
 * Constants to create poison tower
 */
public class PoisonTowerConstants {
    public static final int RANGE_SIGHT = 7;
    public static final int COST = 300;
    public static final int HP = 5000;
    public static final int RELOAD_TIME = 500;
    public static final int DAMAGE_AGAINST_INFANTRY = 100;
    public static final int DAMAGE_AGAINST_CAVALRY = 20;
    public static final int BUILDER_TEAM = General.SCOURGE;
}
