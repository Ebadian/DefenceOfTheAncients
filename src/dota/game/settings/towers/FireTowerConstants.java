package dota.game.settings.towers;

import dota.game.settings.General;

/**
 * Constants to create fire tower
 */
public class FireTowerConstants {
    public static final int RANGE_SIGHT = 7;
    public static final int COST = 300;
    public static final int HP = 5000;
    public static final int RELOAD_TIME = 2000;
    public static final int DAMAGE_AGAINST_INFANTRY = 400;
    public static final int DAMAGE_AGAINST_CAVALRY = 50;
    public static final int BUILDER_TEAM = General.SENTINEL;
}
