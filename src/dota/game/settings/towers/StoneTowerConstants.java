package dota.game.settings.towers;

import dota.game.settings.General;

/**
 * Constants to create stone tower
 */
public class StoneTowerConstants {
    public static final int RANGE_SIGHT = 7;
    public static final int COST = 500;
    public static final int HP = 4000;
    public static final int RELOAD_TIME = 800;
    public static final int DAMAGE_AGAINST_INFANTRY = 40;
    public static final int DAMAGE_AGAINST_CAVALRY = 200;
    public static final int BUILDER_TEAM = General.SENTINEL;
}
