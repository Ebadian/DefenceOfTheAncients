package dota.game.settings.buildings;

/**
 * Constants to create barracks
 */
public class BarrackConstants {
    public static final int HP = 5000;
    public static final int COST = 0;
}
