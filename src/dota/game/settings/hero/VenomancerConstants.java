package dota.game.settings.hero;

import dota.game.settings.General;

/**
 * Constants to create Venomancer hero
 */
public class VenomancerConstants {
    public static final int HP = 4000;
    public static final int RANGE_SIGHT = 7;
    public static final int RELOAD_TIME = 300;
    public static final int DAMAGE = 300;
    public static final int SPEED = 250;
    public static final int OWNER_TEAM = General.SCOURGE;
    public static final int COST = 0;
}
