package dota.game.settings.hero;

import dota.game.settings.General;

/**
 * Constants to create hero Tiny
 */
public class TinyConstants {
    public static final int HP = 5000;
    public static final int RANGE_SIGHT = 7;
    public static final int RELOAD_TIME = 400;
    public static final int DAMAGE = 400;
    public static final int SPEED = 400;
    public static final int OWNER_TEAM = General.SENTINEL;
    public static final int COST = 0;
}
