package dota.game.settings.hero;

/**
 * Constants to create abstract hero
 */
public class HeroConstants {
    public static final int DEATH_TIME = 30 * 1000;
}
