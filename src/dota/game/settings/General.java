package dota.game.settings;

public class General {
    /**
     * team ids for teams sentinel and scourge
     */
    public static final int SENTINEL = 0, SCOURGE = 1;

    public static final int PATHS = 3;
    public static final int LANES_PER_PATH = 5;

    /**
     * the time step of every cycle in milliseconds
     */
    public static final int TIME_UNIT = 50;
}
