package dota.game.events;

import dota.game.board.Board;
import dota.game.objects.alive.forces.GameForce;

import java.util.Vector;

abstract public class Event implements Comparable<Event> {
    protected final int dueTime;

    protected Board board;

    protected Vector<Event> newChoiceEvents = new Vector<>();
    protected Vector<Event> newMainEvents = new Vector<>();

    protected Vector<GameForce> newGameForces = new Vector<>();

    /**
     * @param dueTime time that the even should run
     * @param board   main board of the game
     */
    public Event(int dueTime, Board board) {
        this.dueTime = dueTime;
        this.board = board;
    }

    /**
     * runs the event and stores the subsequensies
     */
    abstract public void run();

    /**
     * newChoiceEvents are newly created first-level events after running of this event
     * which are choices to be made
     */
    public Vector<Event> getNewChoiceEvents() {
        return newChoiceEvents;
    }

    /**
     * newMainEvents are newly created second-level events after running of this event
     * which are the main events of the game
     */
    public Vector<Event> getNewMainEvents() {
        return newMainEvents;
    }

    /**
     * newGameForces are newly created GameForces after running of this event
     */
    public Vector<GameForce> getNewGameForces() {
        return newGameForces;
    }

    /**
     * @param time current time of the game
     * @return true if dueTime of this event has arrived
     */
    public boolean isTime(int time) {
        return time >= dueTime;
    }

    @Override
    public int compareTo(Event o) {
        if (o == null)
            return -1;

        return new Integer(dueTime).compareTo(new Integer(o.dueTime));
    }
}
