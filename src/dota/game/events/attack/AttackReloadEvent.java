package dota.game.events.attack;

import dota.game.board.Board;
import dota.game.board.Cell;
import dota.game.events.Event;
import dota.game.objects.alive.OffensiveObject;
import dota.game.settings.General;

/**
 * a ChoiceEvent which reloads attack, allows attacker to attack and chooses it's targetCell
 */
public class AttackReloadEvent extends Event {
    private final OffensiveObject attacker;

    public AttackReloadEvent(int dueTime, Board board, OffensiveObject attacker) {
        super(dueTime, board);
        this.attacker = attacker;
    }

    @Override
    public void run() {
        if (attacker.isDead())
            return;

        Cell targetCell = attacker.chooseAttackTarget(board);
        if (targetCell == null)
            newChoiceEvents.add(new AttackReloadEvent(dueTime + General.TIME_UNIT, board, attacker));
        else {
            newMainEvents.add(new AttackEvent(dueTime, board, attacker, targetCell));
        }
    }
}
