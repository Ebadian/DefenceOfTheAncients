package dota.game.events.attack;

import dota.game.board.Board;
import dota.game.board.Cell;
import dota.game.events.Event;
import dota.game.objects.alive.AliveObject;
import dota.game.objects.alive.OffensiveObject;

/**
 * a MainEvent for handling real attacks
 */
public class AttackEvent extends Event {
    protected final OffensiveObject attacker;
    protected final Cell targetCell;

    public AttackEvent(int dueTime, Board board, OffensiveObject attacker, Cell targetCell) {
        super(dueTime, board);
        this.attacker = attacker;
        this.targetCell = targetCell;
    }

    @Override
    public void run() throws RuntimeException {
        if (targetCell == null)
            throw new RuntimeException("targetCell to be attacked can not be NULL");

        deliverDamage();

        newChoiceEvents.add(new AttackReloadEvent(dueTime + attacker.getReloadTime(), board, attacker));
    }

    /**
     * delivering damage to the AliveObjects in the targetCell,
     * lowering their remaining hp and
     * giving money for every kill
     */
    protected void deliverDamage() {
        for (AliveObject target : targetCell.getAliveObjects()) {
            if (target.getTeam() != attacker.getTeam() && !target.isDead()) {
                target.sufferDamage(attacker.getDamage().getDamage(target.getAliveObjectType()));
                if (target.isDead()) {
                    try {
                        attacker.getTeam().addMoney(target.getValue());
                    } catch (Exception e) {
                        System.err.println(e.getMessage());
                    }
                }
            }
        }
    }
}
