package dota.game.events.move;

import dota.game.board.Board;
import dota.game.board.Cell;
import dota.game.events.Event;
import dota.game.objects.alive.forces.Hero;
import dota.game.objects.alive.forces.MovableForce;
import dota.game.settings.General;

/**
 * a ChoiceEvent which reloads move, allows movableForce to move and chooses it's targetCell
 */
public class MoveReloadEvent extends Event {
    private final MovableForce movableForce;

    /**
     * @param movableForce movableForce to be moveReloaded
     */
    public MoveReloadEvent(int dueTime, Board board, MovableForce movableForce) {
        super(dueTime, board);
        this.movableForce = movableForce;
    }

    @Override
    public void run() {
        if (movableForce.isDead())
            return;

        Cell targetCell = movableForce.chooseMoveTarget(board);
        if (targetCell == null || movableForce.chooseAttackTarget(board) != null)
            newChoiceEvents.add(new MoveReloadEvent(dueTime + General.TIME_UNIT, board, movableForce));
        else {
            newMainEvents.add(new MoveEvent(dueTime, board, movableForce, targetCell));
/*            if (movableForce instanceof Hero)
                System.err.println("Move of Hero");
*/
        }
    }
}
