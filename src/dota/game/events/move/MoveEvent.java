package dota.game.events.move;

import dota.game.board.Board;
import dota.game.board.Cell;
import dota.game.events.Event;
import dota.game.objects.alive.forces.MovableForce;
import dota.game.settings.General;

/**
 * MainEvent to attempt to move the movableForce to the targetCell
 */
public class MoveEvent extends Event {
    protected final MovableForce movableForce;
    protected final Cell targetCell;

    /**
     * @param movableForce the MovableForce which wants to move
     * @param targetCell   the Cell which movableForce should go to
     */
    public MoveEvent(int dueTime, Board board, MovableForce movableForce, Cell targetCell) {
        super(dueTime, board);
        this.movableForce = movableForce;
        this.targetCell = targetCell;
    }

    /**
     * @throws RuntimeException if the movableForce wants to attack at the same moment
     */
    @Override
    public void run() throws RuntimeException {
        try {
            if (movableForce.getLastAttackTime() != null && dueTime <= movableForce.getLastAttackTime())
                throw new RuntimeException("Cannot move because of attack");
            moveMovableForce();
            newChoiceEvents.add(new MoveReloadEvent(dueTime + movableForce.getMoveReloadTime(), board, movableForce));
        } catch (Exception e) {
            System.err.println("Error while moving " + e.getMessage());
            newChoiceEvents.add(new MoveReloadEvent(dueTime + General.TIME_UNIT, board, movableForce));
        }
    }

    private void moveMovableForce() throws Exception {
        if (targetCell == null)
            throw new Exception("targetCell to move to can not be NULL");
        movableForce.move(targetCell);
    }
}
