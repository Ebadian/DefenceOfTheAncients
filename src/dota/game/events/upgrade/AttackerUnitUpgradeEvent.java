package dota.game.events.upgrade;

import dota.game.board.Board;
import dota.game.board.Team;
import dota.game.events.Event;
import dota.game.objects.alive.forces.AttackerUnit;
import dota.utils.types.AttackerUnitTypes;
import dota.utils.types.AttackerUnitUpgradeTypes;

public class AttackerUnitUpgradeEvent extends Event {
    private final Team team;
    private final AttackerUnitUpgradeTypes attackerUnitUpgradeType;

    public AttackerUnitUpgradeEvent(int dueTime, Board board, Team team, AttackerUnitUpgradeTypes attackerUnitUpgradeType) {
        super(dueTime, board);
        this.team = team;
        this.attackerUnitUpgradeType = attackerUnitUpgradeType;
    }

    @Override
    public void run() {
        for (AttackerUnitTypes attackerUnitType : AttackerUnitTypes.values()) {
            AttackerUnit attackerUnit = team.getAvailableAttackerUnit(attackerUnitType);
            if (attackerUnitUpgradeType == AttackerUnitUpgradeTypes.DAMAGE_UPGRADE) {
                try {
                    team.addMoney((int)attackerUnit.getDamageUpgradeCost());
                } catch (Exception e) {
                    return;
                }
                attackerUnit.upgradeDamage();
            }
            else {
                try {
                    team.addMoney((int)attackerUnit.getHealthUpgradeCost());
                } catch (Exception e) {
                    return;
                }
                attackerUnit.upgradeHealth();
            }
            team.setAvailableAttackerUnit(attackerUnitType, attackerUnit);
        }
    }
}
