package dota.game.events.upgrade;

import dota.game.board.Board;
import dota.game.events.Event;
import dota.game.objects.alive.forces.DefenceTower;
import dota.utils.types.DefenceTowerUpgradeTypes;

public class DefenceTowerUpgradeEvent extends Event {
    private final DefenceTower defenceTower;
    private final DefenceTowerUpgradeTypes defenceTowerUpgradeType;

    public DefenceTowerUpgradeEvent(int dueTime, Board board, DefenceTower defenceTower, DefenceTowerUpgradeTypes defenceTowerUpgradeType) {
        super(dueTime, board);
        this.defenceTower = defenceTower;
        this.defenceTowerUpgradeType = defenceTowerUpgradeType;
    }

    @Override
    public void run() {
        if (defenceTowerUpgradeType == DefenceTowerUpgradeTypes.DAMAGE_UPGRADE) {
            try {
                defenceTower.getTeam().addMoney((int)defenceTower.getDamageUpgradeCost());
            } catch (Exception e) {
                return;
            }
            defenceTower.upgradeDamage();
        }
        else {
            try {
                defenceTower.getTeam().addMoney((int) defenceTower.getRangeSightUpgradeCost());
            } catch (Exception e) {
                return;
            }
            defenceTower.upgradeRangeSight();
        }
    }
}
