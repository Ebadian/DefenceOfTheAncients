package dota.game.events.cheat;

import dota.game.board.Board;
import dota.game.board.Cell;
import dota.game.events.attack.AttackEvent;
import dota.game.objects.alive.forces.GameForce;

// TODO: Remove this

/**
 * a cheat kind of MainEvent to attack to the targetCell anyway without any new reloads
 */
public class CheatAttackEvent extends AttackEvent {
    /**
     * @param attacker   the GameForce which wants to attack
     * @param targetCell the Cell which should be targeted by the attacker
     */
    public CheatAttackEvent(int dueTime, Board board, GameForce attacker, Cell targetCell) {
        super(dueTime, board, attacker, targetCell);
    }

    @Override
    public void run() throws RuntimeException {
        try {
            if (targetCell == null)
                throw new RuntimeException("Cheat attack to null cell");
            super.deliverDamage();
        } catch (RuntimeException e) {
            System.err.println("Invalid cheatAttack : " + e.getMessage());
        }
    }
}
