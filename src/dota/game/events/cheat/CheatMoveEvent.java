package dota.game.events.cheat;

import dota.game.board.Board;
import dota.game.board.Cell;
import dota.game.events.move.MoveEvent;
import dota.game.objects.alive.forces.MovableForce;

// TODO: Remove this

/**
 * a cheat type of MainEvent to move the movableForce to the targetCell anyway, without any reloads
 */
public class CheatMoveEvent extends MoveEvent {
    /**
     * @param movableForce the MovableForce which wants to move
     * @param targetCell   the Cell which movableForce should go to
     */
    public CheatMoveEvent(int dueTime, Board board, MovableForce movableForce, Cell targetCell) {
        super(dueTime, board, movableForce, targetCell);
    }

    @Override
    public void run() throws RuntimeException {
        try {
            movableForce.move(targetCell);
        } catch (Exception e) {
            System.err.println("Invalid cheat move " + e.getMessage());
        }
    }
}
