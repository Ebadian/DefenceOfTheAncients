package dota.game.events.create;

import dota.game.board.Board;
import dota.game.board.Team;
import dota.game.events.Event;
import dota.game.events.attack.AttackReloadEvent;
import dota.game.events.move.MoveReloadEvent;
import dota.game.objects.alive.forces.AttackerUnit;
import dota.game.objects.map.Lane;
import dota.game.settings.General;
import dota.game.utils.location.LanePosition;
import dota.utils.types.AttackerUnitTypes;

/**
 * a MainEvent for creating AttackerUnits
 */
public class CreateAttackerUnitEvent extends Event {
    private final AttackerUnit attackerUnit;

    /**
     * creates an AttackerUnit of attackerUnitType for team in the first Cell of lane
     *
     * @param attackerUnitType type of the new AttackerUnit
     * @param team             the owner of the new AttackerUnit
     * @param lane             the lane of the new AttackerUnit
     * @throws Exception if team's money is not sufficient
     */
    public CreateAttackerUnitEvent(int dueTime, Board board, AttackerUnitTypes attackerUnitType, Team team, Lane lane)
            throws Exception {
        super(dueTime, board);

        AttackerUnit currentDefaultUnit = team.getAvailableAttackerUnit(attackerUnitType);

        team.addMoney(-currentDefaultUnit.getCost());

        attackerUnit = new AttackerUnit(currentDefaultUnit, team);
        LanePosition lanePosition = new LanePosition(lane, team.getAttackerUnitLaneDirection());
        attackerUnit.setLanePosition(lanePosition);

        attackerUnit.setCell(attackerUnit.getLanePosition().getCell());
    }

    public AttackerUnit getAttackerUnit() {
        return attackerUnit;
    }

    @Override
    public void run() {
        newChoiceEvents.add(new MoveReloadEvent(dueTime + General.TIME_UNIT, board, attackerUnit));
        newChoiceEvents.add(new AttackReloadEvent(dueTime + General.TIME_UNIT, board, attackerUnit));
        newGameForces.add(attackerUnit);
    }
}
