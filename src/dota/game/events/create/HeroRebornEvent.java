package dota.game.events.create;

import dota.game.board.Board;
import dota.game.board.Cell;
import dota.game.events.Event;
import dota.game.events.move.MoveReloadEvent;
import dota.game.objects.alive.forces.Hero;
import dota.game.settings.General;

/**
 * a MainEvent for handling Hero's birth
 */
public class HeroRebornEvent extends Event {
    private final Hero hero;
    private final Cell cell;

    /**
     * @param hero to be reborn
     * @param cell the Cell hero should spawn in
     */
    public HeroRebornEvent(int dueTime, Board board, Hero hero, Cell cell) {
        super(dueTime, board);
        this.hero = hero;
        this.cell = cell;
    }

    @Override
    public void run() {
        hero.regainFullHealth();
        hero.setCell(cell);
        hero.setDeathTime(null);
        newGameForces.add(hero);
        newChoiceEvents.add(new MoveReloadEvent(dueTime + General.TIME_UNIT, board, hero));
    }
}
