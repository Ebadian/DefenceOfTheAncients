package dota.game.events.create;

import dota.game.board.Board;
import dota.game.board.Cell;
import dota.game.board.Team;
import dota.game.events.Event;
import dota.game.events.attack.AttackReloadEvent;
import dota.game.objects.alive.forces.DefenceTower;
import dota.game.settings.General;
import dota.utils.types.DefenceTowerTypes;

/**
 * a MainEvent for creating DefenceTowers
 */
public class CreateDefenceTowerEvent extends Event {
    private final DefenceTower defenceTower;

    /**
     * @param defenceTowerType type of the new DefenceTower
     * @param team             the owner of the new DefenceTower
     * @param cell             to place the new DefenceTower on
     * @throws Exception
     */
    public CreateDefenceTowerEvent(int dueTime, Board board, DefenceTowerTypes defenceTowerType, Team team, Cell cell)
            throws Exception {
        super(dueTime, board);

        DefenceTower currentDefaultUnit = team.getAvailableDefenceTower(defenceTowerType);

        team.addMoney(-currentDefaultUnit.getCost());

        defenceTower = new DefenceTower(currentDefaultUnit, team);
        defenceTower.setCell(cell);
    }

    public DefenceTower getDefenceTower() {
        return defenceTower;
    }

    @Override
    public void run() {
        newChoiceEvents.add(new AttackReloadEvent(dueTime + General.TIME_UNIT, board, defenceTower));
        newGameForces.add(defenceTower);
    }
}
