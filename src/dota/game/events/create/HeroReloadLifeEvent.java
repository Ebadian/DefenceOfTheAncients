package dota.game.events.create;

import dota.game.board.Board;
import dota.game.board.Cell;
import dota.game.events.Event;
import dota.game.objects.alive.forces.Hero;

/**
 * a ChoiceEvent to make Hero alive again and respawn
 */
public class HeroReloadLifeEvent extends Event {
    private final Hero hero;

    /**
     * @param hero to reloadLife of
     */
    public HeroReloadLifeEvent(int dueTime, Board board, Hero hero) {
        super(dueTime, board);
        this.hero = hero;
    }

    @Override
    public void run() {
        Cell cell = hero.getTeam().getHeroDefaultPlace();
        newMainEvents.add(new HeroRebornEvent(dueTime, board, hero, cell));
    }
}
