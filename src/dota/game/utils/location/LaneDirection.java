package dota.game.utils.location;

/**
 * LaneDirection Enum contains the different kinds of ways someone can move through a lane
 */
public enum LaneDirection {
    FORWARDS, BACKWARDS
}

