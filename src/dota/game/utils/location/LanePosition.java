package dota.game.utils.location;

import dota.game.board.Cell;
import dota.game.objects.map.Lane;

import java.io.Serializable;

/**
 * LanePosition Class is used for saving position of game objects which are moving along a lane with a direction
 */
public class LanePosition implements Serializable {
    private int indexInLane;
    private Lane lane;
    private LaneDirection direction;

    /**
     * LanePosition initialization
     * index in lane is initially set to the head or tail of the lane according to direction
     */
    public LanePosition(Lane lane, LaneDirection direction) {
        this.direction = direction;
        this.lane = lane;
        indexInLane = getHeadIndex();
    }

    /**
     * @return index of head cell in lane
     */
    public int getHeadIndex() {
        if (direction == LaneDirection.FORWARDS)
            return lane.getDefaultHeadIndex();
        else
            return lane.getDefaultTailIndex();
    }

    /**
     * @return index of tail cell in lane
     */
    public int getTailIndex() {
        if (direction == LaneDirection.FORWARDS)
            return lane.getDefaultTailIndex();
        else
            return lane.getDefaultHeadIndex();
    }

    /**
     * @return index of next cell of current cell in lane
     * @throws RuntimeException throws if next cell doesn't exist
     */
    private int getNextIndex() throws RuntimeException {
        if (direction == LaneDirection.BACKWARDS)
            return lane.getPreviousIndex(indexInLane);
        else
            return lane.getNextIndex(indexInLane);
    }

    /**
     * moves lane position to the next cell of current cell
     *
     * @throws Exception throws if next cell doesn't exist
     */
    public void moveNext() throws Exception {
        indexInLane = getNextIndex();
    }

    /**
     * @return next cell of current cell in lane
     * @throws RuntimeException throws if next cell doesn't exist
     */
    public Cell getNextCell() throws RuntimeException {
        return lane.getCell(getNextIndex());
    }

    /**
     * the amount someone has progressed in lane calculated by distance from head of the lane
     */
    public int progressValue() {
        return Math.abs(indexInLane - getHeadIndex());
    }

    public Lane getLane() {
        return lane;
    }

    /**
     * @return cell according to position
     */
    public Cell getCell() {
        return lane.getCell(indexInLane);
    }

}
