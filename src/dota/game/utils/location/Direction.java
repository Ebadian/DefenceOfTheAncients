package dota.game.utils.location;

import dota.utils.Coordinate;

/**
 * Direction enum which every direction is mapped to a vector
 * Addition of respective vector to a coordinate will result in neighbor of it
 */
public enum Direction {
    RIGHT(0, 1), DOWN(1, 0), LEFT(0, -1), UP(-1, 0);
    private int x, y;

    Direction(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * @return X value of vector related to direction
     */
    public int getX() {
        return x;
    }

    /**
     * @return Y value of vector related to direction
     */
    public int getY() {
        return y;
    }

    /**
     * @return the coordinate related to direction
     */
    public Coordinate asCoordinate() {
        return new Coordinate(x, y);
    }
}
