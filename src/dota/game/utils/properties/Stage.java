package dota.game.utils.properties;

import java.io.Serializable;

/**
 * upgradable object that can be leveled up upto max level
 */
public class Stage implements Serializable {
    private final Integer maxLevel;
    private Integer level = 0;

    public Stage() {
        maxLevel = null;
    }

    public Stage(Integer maxLevel) {
        this.maxLevel = maxLevel;
    }

    public Stage(Stage other) {
        this(other.maxLevel);
        this.level = other.level;
    }

    /**
     * bring this stage to it's new level
     *
     * @throws Exception it must be able to level up
     */
    public void levelUp() throws Exception {
        if (!canLevelUp())
            throw new Exception("Forbidden level UP: Level at Max");
        ++level;
    }

    /**
     * Checking that this stage can be upgraded or not
     *
     * @return boolean in order to check clean up validity
     */
    public boolean canLevelUp() {
        return maxLevel == null || level.compareTo(maxLevel) < 0;
    }

    public int getLevel() {
        return level;
    }

    private Integer getMaxLevel() {
        return maxLevel;
    }
}
