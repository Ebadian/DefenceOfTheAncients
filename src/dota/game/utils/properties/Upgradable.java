package dota.game.utils.properties;

import java.io.Serializable;

/**
 * Upgradable class is an object that can be updated a limited or unlimited times
 */
public class Upgradable implements Serializable {
    protected Stage stage;

    /**
     * with null stage you can have unlimit number of upgrades
     */
    public Upgradable() {
        this.stage = null;
    }

    public Upgradable(Stage stage) {
        if (stage != null)
            this.stage = new Stage(stage);
    }

    public Upgradable(Integer maxLevel) {
        this.stage = new Stage(maxLevel);
    }

    public Upgradable(Upgradable other) {
        this(other.stage);
    }

    /**
     * level up is used for leveling up the stage
     * it must not exceed max level
     */
    public void levelUp() {
        try {
            stage.levelUp();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public int getLevel() {
        return stage.getLevel();
    }
}
