package dota.game.utils.properties;

import java.io.Serializable;

public class Worth implements Serializable {
    private final static double VALUE_COEFFICIENT = 0.8;
    private final int cost;
    private double value;

    /**
     * value is calculated as a ratio of initial cost
     *
     * @param cost the money amount needed to buy this
     */
    public Worth(int cost) {
        this.cost = cost;
        value = VALUE_COEFFICIENT * cost;
    }

    public Worth(Worth other) {
        this.cost = other.cost;
        this.value = other.value;
    }

    public int getCost() {
        return cost;
    }

    public int getValue() {
        return (int) value;
    }

    /**
     * @param value used for adding to instance value (any double)
     */
    public void addToValue(double value) {
        this.value += value;
    }

    /**
     * @param coefficient used for multiplying in value (any double)
     */
    public void multiplyValue(double coefficient) {
        value *= coefficient;
    }
}
