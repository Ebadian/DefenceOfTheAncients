package dota.game.utils.properties;

/**
 * this class is related to range someone can see the enemy units which can be upgraded multiple times
 */
public class RangeSight extends Upgradable {
    private double rangeSight;

    public RangeSight(double rangeSight) {
        this.rangeSight = rangeSight;
    }

    public RangeSight(RangeSight other) {
        super(other);
        this.rangeSight = other.rangeSight;
    }

    public RangeSight(Integer MAX_LEVEL, double rangeSight) {
        super(MAX_LEVEL);
        this.rangeSight = rangeSight;
    }

    public RangeSight(Stage stage, double rangeSight) {
        super(stage);
        this.rangeSight = rangeSight;
    }

    public int getRangeSight() {
        return (int) rangeSight;
    }

    /**
     * @param value the value to upgrade range sight with
     */
    public void add(double value) {
        rangeSight += value;
    }
}
