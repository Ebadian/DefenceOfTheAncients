package dota.game.utils.properties;

/**
 * Health class contains heat points of alive objects which can be upgraded
 */
public class Health extends Upgradable {
    private double hp;
    private double fullHp;

    public Health(Health other) {
        super(other);
        hp = other.hp;
        fullHp = other.fullHp;
    }

    public Health(double hp) {
        super();
        this.hp = hp;
        fullHp = hp;
    }

    public Health(Stage stage, double hp) {
        super(stage);
        this.hp = hp;
        fullHp = hp;
    }

    public Health(Integer MAX_LEVEL, double hp) {
        super(MAX_LEVEL);
        this.hp = hp;
        this.fullHp = hp;
    }

    /**
     * full hp is saved in order to refill someones life points
     */
    public void regainFullHealth() {
        hp = fullHp;
    }

    /**
     * update health of object
     */
    public void add(double hp) {
        this.hp += hp;
    }

    /**
     * Checking someone is dead by positive hp
     *
     * @return true if dead else false
     */
    public boolean isDead() {
        return hp <= 0;
    }

    public double getHp() {
        return hp;
    }
}
