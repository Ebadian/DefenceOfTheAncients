package dota.game.utils.properties;

import dota.utils.types.AliveObjectTypes;

/**
 * Damage class is used for saving and handling damage got by others when the container hits them
 * it can be upgraded several numbers
 */
public class Damage extends Upgradable {
    private double harm[];

    public Damage(Damage other) {
        super(other);
        harm = other.harm;
    }

    /**
     * @param harm the harm values to different alive objects (size must be equal to 2)
     * @throws RuntimeException harm array size must be equal to 2
     */
    public Damage(double[] harm) throws RuntimeException {
        if (harm.length != 2)
            throw new RuntimeException(
                    "Damage constructor: Hp size given (= " + harm.length + ") is not equal to 2");
        this.harm = harm;
    }

    /**
     * Damage can be initialized with a maximum number of upgrades
     * maxlevel set to null will become a limitless damage
     *
     * @param maxLevel related to maximum level this can be upgraded
     */
    public Damage(Integer maxLevel, double[] harm) {
        super(maxLevel);
        this.harm = harm;
    }

    /**
     * @param type alive object type to get proper damage
     * @return proper damage (double) done to this alive object type
     */
    public double getDamage(AliveObjectTypes type) {
        double damage;
        if (type == AliveObjectTypes.CAVALRY)
            damage = harm[1];
        else
            damage = harm[0];
        return damage;
    }

    /**
     * @param value coefficient to multiply to all damage values
     */
    public void multiply(double value) {
        for (int i = 0; i < harm.length; ++i)
            this.harm[i] *= value;
    }
}
