package dota.game.defaultobjects.buildings;

import dota.game.objects.alive.buildings.Ancient;
import dota.game.objects.alive.buildings.Barrack;
import dota.game.settings.buildings.AncientConstants;
import dota.game.settings.buildings.BarrackConstants;
import dota.game.utils.properties.Health;
import dota.game.utils.properties.Worth;
import dota.utils.types.AliveObjectTypes;

public class DefaultBuildings {
    /**
     * Default ancient in game
     */
    public final static Ancient ANCIENT = new Ancient(
            new Worth(AncientConstants.COST),
            new Health(AncientConstants.HP),
            AliveObjectTypes.ANCIENT
    );

    /**
     * Default barrack in game
     */
    public final static Barrack BARRACK = new Barrack(
            new Worth(BarrackConstants.COST),
            new Health(BarrackConstants.HP),
            AliveObjectTypes.BARRACK
    );
}
