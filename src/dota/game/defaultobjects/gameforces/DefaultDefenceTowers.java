package dota.game.defaultobjects.gameforces;

import dota.game.objects.alive.forces.DefenceTower;
import dota.game.settings.towers.BlackTowerConstants;
import dota.game.settings.towers.FireTowerConstants;
import dota.game.settings.towers.PoisonTowerConstants;
import dota.game.settings.towers.StoneTowerConstants;
import dota.game.settings.upgrade.towers.DamageConstants;
import dota.game.settings.upgrade.towers.RangeSightConstants;
import dota.game.utils.properties.Damage;
import dota.game.utils.properties.Health;
import dota.game.utils.properties.RangeSight;
import dota.game.utils.properties.Worth;
import dota.utils.types.AliveObjectTypes;
import dota.utils.types.DefenceTowerTypes;

public class DefaultDefenceTowers {
    /**
     * Default Fire tower in game
     */
    public static final DefenceTower FIRE_TOWER = new DefenceTower(
            new Worth(FireTowerConstants.COST), new Health(FireTowerConstants.HP),
            new RangeSight(RangeSightConstants.MAX_LEVEL, FireTowerConstants.RANGE_SIGHT),
            new Damage(DamageConstants.MAX_LEVEL, new double[]{
                    FireTowerConstants.DAMAGE_AGAINST_INFANTRY,
                    FireTowerConstants.DAMAGE_AGAINST_CAVALRY
            }),
            FireTowerConstants.RELOAD_TIME,
            DefenceTowerTypes.FIRE_TOWER,
            AliveObjectTypes.DEFENCE_TOWER
    );

    /**
     * Default stone tower in game
     */
    public static final DefenceTower STONE_TOWER = new DefenceTower(
            new Worth(StoneTowerConstants.COST), new Health(StoneTowerConstants.HP),
            new RangeSight(RangeSightConstants.MAX_LEVEL, StoneTowerConstants.RANGE_SIGHT),
            new Damage(DamageConstants.MAX_LEVEL, new double[]{
                    StoneTowerConstants.DAMAGE_AGAINST_INFANTRY,
                    StoneTowerConstants.DAMAGE_AGAINST_CAVALRY
            }),
            StoneTowerConstants.RELOAD_TIME,
            DefenceTowerTypes.STONE_TOWER,
            AliveObjectTypes.DEFENCE_TOWER
    );

    /**
     * Default black tower in game
     */
    public static final DefenceTower BLACK_TOWER = new DefenceTower(
            new Worth(BlackTowerConstants.COST), new Health(BlackTowerConstants.HP),
            new RangeSight(RangeSightConstants.MAX_LEVEL, BlackTowerConstants.RANGE_SIGHT),
            new Damage(DamageConstants.MAX_LEVEL, new double[]{
                    BlackTowerConstants.DAMAGE_AGAINST_INFANTRY,
                    BlackTowerConstants.DAMAGE_AGAINST_CAVALRY
            }),
            BlackTowerConstants.RELOAD_TIME,
            DefenceTowerTypes.BLACK_TOWER,
            AliveObjectTypes.DEFENCE_TOWER
    );

    /**
     * Default poison tower in game
     */
    public static final DefenceTower POISON_TOWER = new DefenceTower(
            new Worth(PoisonTowerConstants.COST),
            new Health(PoisonTowerConstants.HP),
            new RangeSight(RangeSightConstants.MAX_LEVEL, PoisonTowerConstants.RANGE_SIGHT),
            new Damage(DamageConstants.MAX_LEVEL, new double[]{
                    PoisonTowerConstants.DAMAGE_AGAINST_INFANTRY,
                    PoisonTowerConstants.DAMAGE_AGAINST_CAVALRY
            }),
            PoisonTowerConstants.RELOAD_TIME,
            DefenceTowerTypes.POISON_TOWER,
            AliveObjectTypes.DEFENCE_TOWER
    );
}
