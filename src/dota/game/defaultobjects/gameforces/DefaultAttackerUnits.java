package dota.game.defaultobjects.gameforces;

import dota.game.objects.alive.forces.AttackerUnit;
import dota.game.settings.attackerunits.CavalryConstants;
import dota.game.settings.attackerunits.InfantryConstants;
import dota.game.settings.upgrade.attackerunits.DamageConstants;
import dota.game.settings.upgrade.attackerunits.HealthConstants;
import dota.game.utils.properties.Damage;
import dota.game.utils.properties.Health;
import dota.game.utils.properties.RangeSight;
import dota.game.utils.properties.Worth;
import dota.utils.types.AliveObjectTypes;
import dota.utils.types.AttackerUnitTypes;

public class DefaultAttackerUnits {
    /**
     * Default infantry attacker unit in game
     */
    public static final AttackerUnit INFANTRY = new AttackerUnit(
            new Worth(InfantryConstants.COST),
            new Health(HealthConstants.MAX_LEVEL, InfantryConstants.HP),
            new RangeSight(InfantryConstants.RANGE_SIGHT),
            new Damage(DamageConstants.MAX_LEVEL, new double[]{
                    InfantryConstants.DAMAGE,
                    InfantryConstants.DAMAGE
            }),
            InfantryConstants.RELOAD_TIME,
            InfantryConstants.SPEED,
            AliveObjectTypes.INFANTRY,
            AttackerUnitTypes.INFANTRY
    );

    /**
     * Default cavalry attacker unit in game
     */
    public static final AttackerUnit CAVALRY = new AttackerUnit(
            new Worth(CavalryConstants.COST), new Health(HealthConstants.MAX_LEVEL, CavalryConstants.HP),
            new RangeSight(CavalryConstants.RANGE_SIGHT),
            new Damage(DamageConstants.MAX_LEVEL, new double[]{
                    CavalryConstants.DAMAGE,
                    CavalryConstants.DAMAGE
            }),
            CavalryConstants.RELOAD_TIME,
            CavalryConstants.SPEED,
            AliveObjectTypes.CAVALRY,
            AttackerUnitTypes.CAVALRY
    );
}
