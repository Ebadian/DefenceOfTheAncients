package dota.game.defaultobjects.gameforces;

import dota.game.objects.alive.forces.Hero;
import dota.game.settings.hero.TinyConstants;
import dota.game.settings.hero.VenomancerConstants;
import dota.game.utils.properties.Damage;
import dota.game.utils.properties.Health;
import dota.game.utils.properties.RangeSight;
import dota.game.utils.properties.Worth;
import dota.utils.types.AliveObjectTypes;

public class DefaultHeros {
    /**
     * Default tiny hero in game
     */
    public static final Hero TINY = new Hero(
            new Worth(TinyConstants.COST),
            new Health(TinyConstants.HP),
            new RangeSight(TinyConstants.RANGE_SIGHT),
            new Damage(new double[]{
                    TinyConstants.DAMAGE,
                    TinyConstants.DAMAGE
            }),
            TinyConstants.RELOAD_TIME,
            TinyConstants.SPEED,
            AliveObjectTypes.HERO
    );

    /**
     * Default venomancer hero in game
     */
    public static final Hero VENOMANCER = new Hero(
            new Worth(VenomancerConstants.COST),
            new Health(VenomancerConstants.HP),
            new RangeSight(VenomancerConstants.RANGE_SIGHT),
            new Damage(new double[]{
                    VenomancerConstants.DAMAGE,
                    VenomancerConstants.DAMAGE
            }),
            VenomancerConstants.RELOAD_TIME,
            VenomancerConstants.SPEED,
            AliveObjectTypes.DEFENCE_TOWER
    );
}
