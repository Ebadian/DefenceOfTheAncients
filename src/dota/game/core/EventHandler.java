package dota.game.core;

import dota.game.board.Board;
import dota.game.board.Cell;
import dota.game.board.Team;
import dota.game.events.Event;
import dota.game.events.create.HeroReloadLifeEvent;
import dota.game.objects.alive.AliveObject;
import dota.game.objects.alive.forces.GameForce;
import dota.game.objects.alive.forces.Hero;
import dota.game.objects.alive.forces.MovableForce;
import dota.game.objects.map.GoldMine;
import dota.game.settings.hero.HeroConstants;
import dota.utils.types.AliveObjectTypes;

import java.util.PriorityQueue;
import java.util.Vector;

public class EventHandler {
    private PriorityQueue<Event> choiceEvents = new PriorityQueue<>();
    private Vector<Event> mainEvents;
    private Vector<GameForce> newGameForces;
    private Board board;
    private Integer time;

    /**
     *
     * @param board the Board which the EventHandler is handling the Event of
     */
    public EventHandler(Board board) {
        this.board = board;
    }

    /**
     * runs the events which their dueTime has arrived
     * @param time the current time
     * @param mainEvents second level events that should get ran
     */
    public void run(int time, Vector<Event> mainEvents) {
        this.time = time;
        this.mainEvents = mainEvents;
        newGameForces = new Vector<>();

        runChoiceEvents();
        runMainEvents();
        cleanUp();
    }

    /**
     * run all ChoiceEvents
     * @throws RuntimeException if new GameForces were created
     */
    private void runChoiceEvents() throws RuntimeException {

        while (!choiceEvents.isEmpty() && choiceEvents.peek().isTime(time)) {
            Event event = choiceEvents.poll();
            event.run();

            choiceEvents.addAll(event.getNewChoiceEvents());
            mainEvents.addAll(event.getNewMainEvents());
            if (!event.getNewGameForces().isEmpty())
                throw new RuntimeException("A 1st lvl event had made a new AliveObject");
        }
    }

    /**
     * run all MainEvents
     * @throws RuntimeException new MainEvents were created
     */
    private void runMainEvents() throws RuntimeException {
        for (Event event : mainEvents) {
            event.run();

            choiceEvents.addAll(event.getNewChoiceEvents());
            if (!event.getNewMainEvents().isEmpty())
                throw new RuntimeException("A 2nd lvl event had made a new MainEvent");
            newGameForces.addAll(event.getNewGameForces());
        }
    }

    private void cleanUp() {
        handleMoney();
        updateCellsInfo();

        time = null;
        mainEvents = null;
    }

    private void handleMoney() {
        if (time % 1000 == 0)
            for (Team team : board.getTeams()) {
                try {
                    team.addMoney(10);
                } catch (Exception e) {
                    System.err.println(e.getMessage());
                }
            }

        updateOwnersOfGoldMines();
        handleGoldMinesMoneyTransactions();
    }

    public void addChoiceEvents(Vector<Event> choiceEvents) {
        this.choiceEvents.addAll(choiceEvents);
    }

    /**
     * update the GoldMine's owners
     */
    public void updateOwnersOfGoldMines() {
        for (Team team : board.getTeams()) {
            Hero hero = team.getHero();
            GoldMine goldMine = hero.getCell().getGoldMine();
            if (goldMine != null) {
                try {
                    goldMine.setOwner(hero.getTeam());
                } catch (Exception e) {
                    if (goldMine.getOwner() != hero.getTeam())
                        goldMine.destroy();
                }
            }
        }
    }

    public void addMainEvent(Event event) {
        if (mainEvents == null)
            mainEvents = new Vector<>();
        mainEvents.add(event);
    }

    public void addChoiceEvent(Event event) {
        choiceEvents.add(event);
    }

    private void handleGoldMinesMoneyTransactions() {
        if (time % 1000 != 0)
            return;

        for (GoldMine goldMine : board.getGoldMines()) {
            if (goldMine.isDestroyed())
                continue;

            Team owner = goldMine.getOwner();
            if (owner != null) {
                try {
                    owner.addMoney(goldMine.getValue());
                } catch (Exception e) {
                    System.err.println(e.getMessage());
                }
            }
        }
    }

    private void updateCellsInfo() {
        updateMovedMovableObjectsInfo();
        updateDeadAliveObjectsInfo();
        updateNewGameForces();
    }

    private void updateNewGameForces() {
        for (GameForce newGameForce : newGameForces)
            newGameForce.getCell().addAliveObject(newGameForce);
    }

    private void updateMovedMovableObjectsInfo() {
        for (Cell rowsOfCells[] : board.convertToCells())
            for (Cell cell : rowsOfCells) {
                Vector<AliveObject> movedObjects = new Vector<>();
                for (AliveObject aliveObject : cell.getAliveObjects())
                    if (aliveObject instanceof MovableForce && ((MovableForce) aliveObject).getCell() != cell) {
                        try {
                            ((MovableForce) aliveObject).getCell().addAliveObject(aliveObject);
                            movedObjects.add(aliveObject);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                cell.removeAllAliveObjects(movedObjects);
            }
    }

    private void updateDeadAliveObjectsInfo() {
        for (Cell rowsOfCells[] : board.convertToCells())
            for (Cell cell : rowsOfCells) {
                Vector<AliveObject> deadObjects = new Vector<>();
                for (AliveObject aliveObject : cell.getAliveObjects())
                    if (aliveObject.isDead()) {
                        if (aliveObject.getAliveObjectType() == AliveObjectTypes.HERO) {
                            Hero hero = (Hero) aliveObject;
                            choiceEvents.add(
                                    new HeroReloadLifeEvent(time + HeroConstants.DEATH_TIME, board, hero));
                            hero.setTargetCoordinate(null);
                        }
                        aliveObject.setDeathTime(time);
                        deadObjects.add(aliveObject);
                    }
                cell.removeAllAliveObjects(deadObjects);
            }
    }
}
