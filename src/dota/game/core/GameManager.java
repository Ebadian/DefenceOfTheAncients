package dota.game.core;

import dota.common.GameObjectID;
import dota.game.board.Board;
import dota.game.board.Cell;
import dota.game.board.Team;
import dota.game.events.Event;
import dota.game.events.cheat.CheatAttackEvent;
import dota.game.events.cheat.CheatMoveEvent;
import dota.game.events.create.CreateAttackerUnitEvent;
import dota.game.events.create.CreateDefenceTowerEvent;
import dota.game.events.create.HeroReloadLifeEvent;
import dota.game.objects.GameObject;
import dota.game.objects.alive.buildings.Ancient;
import dota.game.objects.alive.buildings.Barrack;
import dota.game.objects.alive.forces.Hero;
import dota.game.objects.map.Lane;
import dota.game.objects.map.Path;
import dota.game.settings.General;
import dota.game.utils.location.Direction;
import dota.utils.Coordinate;
import dota.utils.MapFileLoader;
import dota.utils.types.AttackerUnitTypes;
import dota.utils.types.DefenceTowerTypes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

public class GameManager {
    private Integer time;
    private EventHandler eventHandler;
    private Board board;
    private Vector<Event> newMainEvents;

    public GameManager() {
        time = 0;
    }

    public void setUp() {
        eventHandler = new EventHandler(board);
        newMainEvents = new Vector<>();

        for (Team team : board.getTeams())
            eventHandler.addChoiceEvent(new HeroReloadLifeEvent(time, board, team.getHero()));

        nextTimeUnit();
    }

    public void setMapDimensions(int columns, int rows) {
        board = new Board(rows, columns);
    }

    public void setPaths(ArrayList<ArrayList<ArrayList<Coordinate>>> pathCoordinates) {
        Path[] boardPaths = convertToPaths(pathCoordinates);
        board.setPaths(boardPaths);
    }

    public void addGoldMines(ArrayList<Coordinate> coordinates) {
        for (Coordinate coordinate : coordinates)
            addGoldMine(coordinate);
    }

    public void addGoldMine(Coordinate coordinate) {
        board.addGoldMineByCoordinate(coordinate);
    }

    public void setTeam(int teamID, Coordinate[][] ancient, ArrayList<Coordinate[][]> barracks) {
        Cell[][] teamAncientCells = board.convertToCells(ancient);
        ArrayList<Cell[][]> teamBarracksConverted = new ArrayList<>();

        for (Coordinate[][] barrack : barracks)
            teamBarracksConverted.add(board.convertToCells(barrack));

        board.createTeam(teamID, teamID, teamAncientCells, teamBarracksConverted);

        Ancient teamAncient = board.getTeamAncient(teamID);
        ArrayList<Coordinate> coordinates = teamAncient.getAllCoordinates();
        for (Coordinate coordinate : coordinates)
            board.getCell(coordinate).addAliveObject(teamAncient);

        Barrack[] teamBarracks = board.getTeamBarracks(teamID);
        for (Barrack barrack : teamBarracks)
            for (Coordinate coordinate : barrack.getAllCoordinates())
                board.getCell(coordinate).addAliveObject(barrack);
    }

    private Path[] convertToPaths(ArrayList<ArrayList<ArrayList<Coordinate>>> pathsCoordinates) {
        Path[] paths = new Path[pathsCoordinates.size()];

        for (int i = 0; i < paths.length; i++)
            paths[i] = convertToPath(pathsCoordinates.get(i));

        return paths;
    }

    private Path convertToPath(ArrayList<ArrayList<Coordinate>> pathCoordinates) {
        Lane[] pathLanes = new Lane[pathCoordinates.size()];

        for (int i = 0; i < pathCoordinates.size(); i++)
            pathLanes[i] = convertToLane(pathCoordinates.get(i));

        return new Path(pathLanes);
    }

    private Lane convertToLane(ArrayList<Coordinate> laneCoordinates) {
        Cell[] laneCells = new Cell[laneCoordinates.size()];

        for (int i = 0; i < laneCells.length; i++)
            laneCells[i] = board.getCell(laneCoordinates.get(i));

        return new Lane(laneCells);
    }

    public int getBoardWidth() {
        return board.getColumns();
    }

    public int getBoardHeight() {
        return board.getRows();
    }

    public GameObjectID getGoldMineID(int goldMineIndex) {
        return convertToGameObjectID(board.getGoldMine(goldMineIndex));
    }

    public GameObjectID[] getGoldMineIDs() {
        return convertToGameObjectID(board.getGoldMines());
    }

    public GameObjectID getTeamAncientID(int teamIndex) {
        return convertToGameObjectID(board.getTeamAncient(teamIndex));
    }

    public GameObjectID[] getTeamBarracksID(int teamIndex) {
        return convertToGameObjectID(board.getTeamBarracks(teamIndex));
    }

    public GameObjectID[] getPathLanesIDs(int pathIndex) {
        return convertToGameObjectID(board.getPathsLanes(pathIndex));
    }

    public GameObjectID getPathID(int pathIndex) {
        return convertToGameObjectID(board.getPath(pathIndex));
    }

    public GameObjectID[] getPathIDs() {
        return convertToGameObjectID(board.getPaths());
    }

    public ArrayList<GameObjectID[]> getAllLaneIDs() {
        ArrayList<GameObjectID[]> lanesIDs = new ArrayList<>();

        for (int i = 0; i < board.getPaths().length; i++)
            lanesIDs.add(getPathLanesIDs(i));

        return lanesIDs;
    }

    public GameObjectID getTeamHeroID(int teamID) {
        return convertToGameObjectID(board.getTeam(teamID).getHero());
    }

    private GameObjectID convertToGameObjectID(GameObject gameObject) {
        return gameObject.getID();
    }

    private GameObjectID[] convertToGameObjectID(GameObject[] gameObjects) {
        GameObjectID[] gameObjectIDs = new GameObjectID[gameObjects.length];

        for (int i = 0; i < gameObjects.length; i++)
            gameObjectIDs[i] = gameObjects[i].getID();

        return gameObjectIDs;
    }

    public HashMap<String, Integer> getInfo(GameObjectID gameObjectID) throws RuntimeException {
        return GameObject.getGameObject(gameObjectID).getJudgeInfo();
    }

    public GameObjectID createAttackerUnit(int teamID, AttackerUnitTypes attackerUnitType, GameObjectID laneID)
            throws Exception {
        Lane lane = (Lane) GameObject.getGameObject(laneID);

        Team team = board.getTeam(teamID);

        CreateAttackerUnitEvent createAttackerUnitEvent =
                new CreateAttackerUnitEvent(time, board, attackerUnitType, team, lane);

        newMainEvents.add(createAttackerUnitEvent);

        return createAttackerUnitEvent.getAttackerUnit().getID();
    }

    public GameObjectID createDefenceTower(int teamID, DefenceTowerTypes defenceTowerType, Coordinate coordinate)
            throws Exception {
        Cell cell = board.getCell(coordinate);
        Team team = board.getTeam(teamID);

        CreateDefenceTowerEvent createDefenceTowerEvent =
                new CreateDefenceTowerEvent(time, board, defenceTowerType, team, cell);

        newMainEvents.add(createDefenceTowerEvent);

        return createDefenceTowerEvent.getDefenceTower().getID();
    }

    public void moveHero(GameObjectID gameObjectID, Direction direction) throws Exception {
        Hero hero = (Hero) GameObject.getGameObject(gameObjectID);

        Cell cell = board.getAdjCell(hero.getCell(), direction);

        CheatMoveEvent moveEvent = new CheatMoveEvent(time, board, hero, cell);

        newMainEvents.add(moveEvent);
    }

    public void setHeroTarget(int teamID, Coordinate coordinate) {
        Hero hero = board.getTeam(teamID).getHero();
        hero.setTargetCoordinate(coordinate);
    }

    public void attackHero(GameObjectID gameObjectID, Coordinate target) {
        Hero hero = (Hero) GameObject.getGameObject(gameObjectID);

        Cell cell = board.getCell(target);

        CheatAttackEvent attackEvent = new CheatAttackEvent(time, board, hero, cell);

        newMainEvents.add(attackEvent);
    }

    public int getMoney(int teamIndex) {
        return board.getTeam(teamIndex).getMoney();
    }

    public void nextTimeUnit() {
        Vector<Event> currentNewEvents = new Vector<>(newMainEvents);
        eventHandler.run(time, currentNewEvents);
        time += General.TIME_UNIT;
        newMainEvents.removeAll(currentNewEvents);
/**        if (time % 1000 == 0)
            System.out.println("Here at time: " + time);
*/
    }

    public void buildFromMapFileLoader(MapFileLoader map) {
        setMapDimensions(map.getRows(), map.getColumns());
        setPaths(map.getPaths());
        addTeam(General.SENTINEL, map);
        addTeam(General.SCOURGE, map);
        addGoldMines(map.getGoldMines());
    }

    private void addTeam(int teamID, MapFileLoader map) {
        setTeam(teamID, map.getAncient(teamID), map.getBarrack(teamID));
    }

    public Board getBoard() {
        return board;
    }

    public int getTime() {
        return time;
    }
}
