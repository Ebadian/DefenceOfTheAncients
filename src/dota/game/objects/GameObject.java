package dota.game.objects;

import dota.common.GameObjectID;
import dota.utils.types.NetworkInfoTypes;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Logic version of GameObject
 */
public class GameObject implements Serializable {
    static private HashMap<GameObjectID, GameObject> gameObjects = new HashMap<>();
    protected final GameObjectID ID;

    public GameObject() {
        ID = GameObjectID.create(getClass());
        gameObjects.put(ID, this);
    }

    public static GameObject getGameObject(GameObjectID ID) {
        return gameObjects.get(ID);
    }

    /**
     * GameObjectID uniques each GameObject
     */
    public GameObjectID getID() {
        return ID;
    }

    public HashMap<String, Integer> getJudgeInfo() {
        return new HashMap<>();
    }

    public HashMap<String, Object> getNetworkInfo() {
        HashMap<String, Object> info = new HashMap<>();
        info.put(NetworkInfoTypes.GAME_ID, GameObject.this.ID);
        return info;
    }
}
