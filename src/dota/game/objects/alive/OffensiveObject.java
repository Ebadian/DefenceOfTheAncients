package dota.game.objects.alive;

import dota.game.board.Board;
import dota.game.board.Cell;
import dota.game.objects.TeamObject;
import dota.game.utils.properties.Damage;

/**
 * Interface for all Objects who attack
 */
public interface OffensiveObject extends TeamObject {
    /**
     * @param board the info needed to find the attackTarget
     * @return best Cell to attack to based on the conditions of the OffensiveObject
     */
    Cell chooseAttackTarget(Board board);

    /**
     * @return a number equal to number of miliseconds which should pass,
     * so that the OffensiveObject attacks after another attack
     */
    int getReloadTime();

    /**
     * @return true if it's life is not positive
     */
    boolean isDead();

    Damage getDamage();
}
