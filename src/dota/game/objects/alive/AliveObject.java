package dota.game.objects.alive;

import dota.game.board.Cell;
import dota.game.board.Team;
import dota.game.objects.GameObject;
import dota.game.objects.TeamObject;
import dota.game.utils.properties.Health;
import dota.game.utils.properties.Worth;
import dota.utils.Coordinate;
import dota.utils.types.AliveObjectTypes;
import dota.utils.types.JudgeInfoTypes;
import dota.utils.types.NetworkInfoTypes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Logic for AliveObjects
 */
abstract public class AliveObject extends GameObject implements TeamObject {
    private static int aliveObjectCounter;
    private final int aliveObjectID;
    private final AliveObjectTypes aliveObjectType;
    protected Team team;
    protected Worth worth;
    protected Health health;

    private Integer deathTime;

    {
        this.aliveObjectID = aliveObjectCounter++;
    }

    /**
     * @param worth           the Worth of the aliveObject
     * @param health          the Health of the aliveObject
     * @param aliveObjectType the AliveObjectType of the aliveObject
     */
    public AliveObject(Worth worth, Health health, AliveObjectTypes aliveObjectType) {
        this.worth = new Worth(worth);
        this.health = new Health(health);
        this.aliveObjectType = aliveObjectType;
    }

    /**
     * @param other CopyConstructor
     */
    public AliveObject(AliveObject other) {
        this(other.worth, other.health, other.aliveObjectType);
        team = other.team;
    }

    /**
     * @param other CopyConstructor
     * @param team  the Team of the aliveObject
     */
    public AliveObject(AliveObject other, Team team) {
        this(other);
        this.team = team;
    }

    public int getAliveObjectID() {
        return aliveObjectID;
    }

    /**
     * @return value of the aliveObject
     */
    public int getValue() {
        return worth.getValue();
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    /**
     * @param damage the value which should be lessened from the health of aliveObject
     */
    public void sufferDamage(double damage) {
        health.add(-damage);
    }

    public boolean isDead() {
        return health.isDead();
    }

    public AliveObjectTypes getAliveObjectType() {
        return aliveObjectType;
    }

    /**
     * @return the number of remaining hitpoints of aliveObject
     */
    public double getHealthHp() {
        return health.getHp();
    }

    /**
     * make the aliveObject completely healthy again
     */
    public void regainFullHealth() {
        health.regainFullHealth();
    }

    @Override
    public HashMap<String, Integer> getJudgeInfo() {
        HashMap<String, Integer> info = super.getJudgeInfo();

        info.put(JudgeInfoTypes.IS_ALIVE, isDead() ? 0 : 1);
        info.put(JudgeInfoTypes.TEAM_ID, team.getTeamID());
        info.put(JudgeInfoTypes.HEALTH, new Double(health.getHp()).intValue());

        return info;
    }

    /**
     * @return cost of aliveObject
     */
    public int getCost() {
        return worth.getCost();
    }

    abstract public boolean isInCell(Cell cell);

    /**
     * @return the time when aliveObject died
     */
    public Integer getDeathTime() {
        return deathTime;
    }

    public void setDeathTime(Integer deathTime) {
        this.deathTime = deathTime;
    }

    @Override
    public HashMap<String, Object> getNetworkInfo() {
        HashMap<String, Object> info = super.getNetworkInfo();

        info.put(NetworkInfoTypes.IS_ALIVE, isDead() ? 0 : 1);
        info.put(NetworkInfoTypes.TEAM_ID, team.getTeamID());
        info.put(NetworkInfoTypes.HEALTH, new Double(health.getHp()).intValue());
        info.put(NetworkInfoTypes.PLACE, getAllCoordinates());

        return info;
    }

    abstract public ArrayList<Coordinate> getAllCoordinates();
}
