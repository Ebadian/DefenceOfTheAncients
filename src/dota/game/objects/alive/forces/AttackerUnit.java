package dota.game.objects.alive.forces;

import dota.game.board.Board;
import dota.game.board.Cell;
import dota.game.board.Team;
import dota.game.settings.upgrade.attackerunits.DamageConstants;
import dota.game.settings.upgrade.attackerunits.HealthConstants;
import dota.game.utils.location.LanePosition;
import dota.game.utils.properties.Damage;
import dota.game.utils.properties.Health;
import dota.game.utils.properties.RangeSight;
import dota.game.utils.properties.Worth;
import dota.utils.types.AliveObjectTypes;
import dota.utils.types.AttackerUnitTypes;
import dota.utils.types.NetworkInfoTypes;

import java.util.HashMap;

/**
 * Logic version of AttackerUnit
 */
public class AttackerUnit extends MovableForce {
    private static int attackerUnitCounter;
    private final int attackerUnitID;
    private LanePosition lanePosition;
    private AttackerUnitTypes attackerUnitType;

    {
        this.attackerUnitID = attackerUnitCounter++;
    }

    /**
     * @param other copy constructor
     */
    public AttackerUnit(AttackerUnit other) {
        super(other);
        lanePosition = other.lanePosition;
    }

    /**
     * @param other copy constructor
     * @param team  the owner Team
     */
    public AttackerUnit(AttackerUnit other, Team team) {
        super(other, team);
        lanePosition = other.lanePosition;
        attackerUnitType = other.getAttackerUnitType();
    }

    /**
     * @param rangeSight       RangeSight of AttackerUnit
     * @param damage           Damage of the AttackerUnit
     * @param reloadTime       ReloadTime of the attacks of AttackerUnit in ms
     * @param speed            movement speed in tiles/ms
     * @param aliveObjectType  AliveObjectType of the AttackerUnit
     * @param attackerUnitType type of the AttackerUnit
     */
    public AttackerUnit(Worth worth, Health health, RangeSight rangeSight, Damage damage, int reloadTime, int speed,
                        AliveObjectTypes aliveObjectType, AttackerUnitTypes attackerUnitType) {
        super(worth, health, rangeSight, damage, reloadTime, speed, aliveObjectType);
        this.attackerUnitType = attackerUnitType;
    }

    /**
     * @return LanePosition of the attackerUnit
     */
    public LanePosition getLanePosition() {
        return lanePosition;
    }

    public void setLanePosition(LanePosition lanePosition) {
        this.lanePosition = lanePosition;
    }

    @Override
    public Cell chooseAttackTarget(Board board) {
        return new AttackStrategy(board).chooseTarget();
    }

    public int getAttackerUnitID() {
        return attackerUnitID;
    }

    /**
     * @return const of Upgrading attackerUnit's Damage one level
     */
    public double getDamageUpgradeCost() {
        return (damage.getLevel() + 1) * DamageConstants.COST;
    }

    /**
     * Upgrade attackerUnit's Damage one level
     */
    public void upgradeDamage() {
        damage.levelUp();
        damage.multiply(1 + DamageConstants.HP_ADD_COEF);
        worth.addToValue(DamageConstants.VALUE_ADD_COEF * worth.getCost());
    }

    /**
     * @return cost of upgrading attackerUnit's Health one level
     */
    public double getHealthUpgradeCost() {
        return (health.getLevel() + 1) * HealthConstants.COST;
    }

    /**
     * Upgrade attackerUnit's Health one level
     */
    public void upgradeHealth() {
        health.levelUp();
        health.add(HealthConstants.HP_ADD);
        worth.addToValue(HealthConstants.VALUE_ADD_COEF * worth.getCost());
    }

    @Override
    public Cell chooseMoveTarget(Board board) {
        try {
            return lanePosition.getNextCell();
        } catch (RuntimeException e) {
//            System.out.println("Error while moving " + e.getMessage());
            return null;
        }
    }

    /**
     * @param cell the Cell that the attackerUnit should move to
     * @throws Exception if the cell is not the next Cell in attackerUnit's Lane
     */
    public void move(Cell cell) throws Exception {
        if (cell != lanePosition.getNextCell())
            throw new Exception("Move is not valid. Lane move is different.");
        try {
            lanePosition.moveNext();
        } catch (Exception e) {
            throw e;
        }
        this.cell = cell;
    }

    public AttackerUnitTypes getAttackerUnitType() {
        return attackerUnitType;
    }

    @Override
    public HashMap<String, Object> getNetworkInfo() {
        HashMap<String, Object> info = super.getNetworkInfo();

        info.put(NetworkInfoTypes.ATTACKER_UNIT_TYPE, attackerUnitType);

        return info;
    }

    /**
     * an InnerClass to handle strategy of AttackerUnit's attacks
     */
    private class AttackStrategy {
        private Board board;

        /**
         * @param board the Board which the AttackerUnit is in
         */
        private AttackStrategy(Board board) {
            this.board = board;
        }

        /**
         * @return best Cell to attack to based on properties and conditions of the attackerUnit
         */
        private Cell chooseTarget() {
            Cell[] cells = board.getVisibleCells(cell, rangeSight.getRangeSight());

            Cell target = getNearestEnemyTower(cells);
            if (target != null)
                return target;

            target = getBestAttackForceCell(cells);
            if (target != null)
                return target;

            target = getEnemyBarrackCell(cells);
            if (target != null)
                return target;

            target = getEnemyAncientCell(cells);
            if (target != null)
                return target;

            return null;
        }

        /**
         * @param cells the Cells attackerUnit can attack to
         * @return a Cell with enemy Ancient in it among cells, or null if there is none
         */
        private Cell getEnemyAncientCell(Cell[] cells) {
            for (Cell cell : cells)
                if (hasEnemyAliveObjectType(cell, AliveObjectTypes.ANCIENT))
                    return cell;
            return null;
        }

        /**
         * @param cells the Cells attackerUnit can attack to
         * @return
         */
        private Cell getEnemyBarrackCell(Cell[] cells) {
            for (Cell cell : cells)
                if (hasEnemyAliveObjectType(cell, AliveObjectTypes.BARRACK))
                    return cell;
            return null;
        }

        /**
         * @return true if cell1 is better to be attacked than cell2
         * @throws RuntimeException if cell1 == null
         */
        private boolean isBetterTowerToAttack(Cell cell1, Cell cell2) throws RuntimeException {
            return compareCellByDistance(cell1, cell2) < 0;
        }

        /**
         * @param cells the Cells attackerUnit can attack to
         * @return nearest Cell with enemy DefenceTower in it among cells, or null if there is none
         */
        private Cell getNearestEnemyTower(Cell[] cells) {
            Cell targetCell = null;

            for (Cell cell : cells)
                if (hasEnemyAliveObjectType(cell, AliveObjectTypes.DEFENCE_TOWER) &&
                        isBetterTowerToAttack(cell, targetCell))
                    targetCell = cell;

            return targetCell;
        }

        /**
         * @return true if based on AttackerForces' information alone, cell1 is better to be attacked than cell2
         * @throws RuntimeException if cell1 == null
         */
        private boolean isBetterCellToHitEnemyAttackForces(Cell cell1, Cell cell2) throws RuntimeException {
            if (cell1 == null)
                throw new RuntimeException("Cell1 should not be null to compare.");
            if (cell2 == null)
                return true;

            int distComp = compareCellByDistance(cell1, cell2);
            if (distComp < 0)
                return true;
            else if (distComp != 0)
                return false;

            return compareCellByValueOfAttackForces(cell1, cell2) > 0;
        }

        /**
         * @param cells the Cells attackerUnit can attack to
         * @return best Cell to attack to, among cells, and based on AttackerForces' information alone
         */
        private Cell getBestAttackForceCell(Cell[] cells) {
            Cell targetCell = null;

            for (Cell cell : cells)
                if (cellHasEnemyAttackForce(cell) && isBetterCellToHitEnemyAttackForces(cell, targetCell))
                    targetCell = cell;

            return targetCell;
        }

    }
}
