package dota.game.objects.alive.forces;

import dota.game.board.Board;
import dota.game.board.Cell;
import dota.game.board.Team;
import dota.game.utils.properties.Damage;
import dota.game.utils.properties.Health;
import dota.game.utils.properties.RangeSight;
import dota.game.utils.properties.Worth;
import dota.utils.types.AliveObjectTypes;
import dota.utils.types.JudgeInfoTypes;

import java.util.HashMap;

public abstract class MovableForce extends GameForce {
    private static int movableForceCounter;
    private final int movableForceID;
    protected int moveReloadTime;
    protected Integer lastMovedTime;

    {
        this.movableForceID = movableForceCounter++;
    }

    /**
     * @param moveReloadTime minimum difference between two movements of the movableForce
     */
    public MovableForce(Worth worth, Health health, RangeSight rangeSight, Damage damage, int reloadTime, int moveReloadTime, AliveObjectTypes aliveObjectType) {
        super(worth, health, rangeSight, damage, reloadTime, aliveObjectType);
        this.moveReloadTime = moveReloadTime;
    }

    /**
     * @param other copy constuctor
     * @param team  the owner Team of the movableForce
     */
    public MovableForce(MovableForce other, Team team) {
        super(other, team);
        moveReloadTime = other.moveReloadTime;
    }

    /**
     * @param other copy constructor
     */
    public MovableForce(MovableForce other) {
        super(other);
        moveReloadTime = other.moveReloadTime;
    }

    public int getMovableForceID() {
        return movableForceID;
    }

    /**
     * @return targetCell to attack to based on the conditions and properties of the MovableForce
     * @param board
     */
    abstract public Cell chooseMoveTarget(Board board);

    /**
     * @param cell the Cell to move the movableForce to
     * @throws Exception if cell is not a valid Cell to move to
     */
    abstract public void move(Cell cell) throws Exception;

    public int getMoveReloadTime() {
        return moveReloadTime;
    }

    /**
     * @return lastTime the movableForce has moved
     */
    public Integer getLastMovedTime() {
        return lastMovedTime;
    }

    public void setLastMovedTime(Integer lastMovedTime) {
        this.lastMovedTime = lastMovedTime;
    }

    @Override
    public HashMap<String, Integer> getJudgeInfo() {
        HashMap<String, Integer> info = super.getJudgeInfo();

        info.put(JudgeInfoTypes.ATTACK, new Double(damage.getDamage(AliveObjectTypes.DEFENCE_TOWER)).intValue());
        info.put(JudgeInfoTypes.SPEED, speed);

        return info;
    }
}
