package dota.game.objects.alive.forces;

import dota.game.board.Board;
import dota.game.board.Cell;
import dota.game.board.Team;
import dota.game.utils.location.Direction;
import dota.game.utils.properties.Damage;
import dota.game.utils.properties.Health;
import dota.game.utils.properties.RangeSight;
import dota.game.utils.properties.Worth;
import dota.utils.Coordinate;
import dota.utils.types.AliveObjectTypes;

public class Hero extends MovableForce {
    private static int heroCounter;
    private final int heroID;

    private Coordinate targetCoordinate;

    {
        this.heroID = heroCounter++;
    }

    /**
     * @param other copy constructor
     */
    public Hero(Hero other) {
        super(other);
    }

    /**
     * @param other copy constructor
     * @param team  the owner Team of the Hero
     */
    public Hero(Hero other, Team team) {
        super(other, team);
    }

    public Hero(Worth worth, Health health, RangeSight rangeSight, Damage damage, int reloadTime, int speed, AliveObjectTypes aliveObjectType) {
        super(worth, health, rangeSight, damage, reloadTime, speed, aliveObjectType);
    }

    /**
     *
     * @return coordinate of the target set
     */
    public synchronized Coordinate getTargetCoordinate() {
        return targetCoordinate;
    }

    /**
     * set target of the hero to move to it
     * @param targetCoordinate the relative coordinate
     */
    public synchronized void setTargetCoordinate(Coordinate targetCoordinate) {
        this.targetCoordinate = targetCoordinate;
    }

    @Override
    public Cell chooseAttackTarget(Board board) {
        return null;
    }

    @Override
    public Cell chooseMoveTarget(Board board) {
        if (targetCoordinate == null || cell == null)
            return null;

        Coordinate currentPlace = cell.getCoordinate();
        if (!currentPlace.equals(targetCoordinate)) {
            if (currentPlace.getX() == targetCoordinate.getX())
                return board.getAdjCell(cell, currentPlace.getY() < targetCoordinate.getY() ?
                        Direction.RIGHT : Direction.LEFT);
            else
                return board.getAdjCell(cell, currentPlace.getX() < targetCoordinate.getX() ?
                        Direction.DOWN : Direction.UP);
        }
        return null;
    }

    @Override
    public void move(Cell cell) throws Exception {
        this.cell = cell;
    }

    public int getHeroID() {
        return heroID;
    }
}
