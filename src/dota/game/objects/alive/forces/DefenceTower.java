package dota.game.objects.alive.forces;

import dota.game.board.Board;
import dota.game.board.Cell;
import dota.game.board.Team;
import dota.game.objects.alive.AliveObject;
import dota.game.settings.upgrade.towers.DamageConstants;
import dota.game.settings.upgrade.towers.RangeSightConstants;
import dota.game.utils.properties.Damage;
import dota.game.utils.properties.Health;
import dota.game.utils.properties.RangeSight;
import dota.game.utils.properties.Worth;
import dota.utils.types.AliveObjectTypes;
import dota.utils.types.DefenceTowerTypes;
import dota.utils.types.JudgeInfoTypes;

import java.util.HashMap;


/**
 * Logic version of DefenceTower
 */
public class DefenceTower extends GameForce {
    private static int defenceTowerCounter;
    private final int defenceTowerID;
    private final DefenceTowerTypes towerType;

    {
        this.defenceTowerID = defenceTowerCounter++;
    }

    /**
     * @param other copy constructor
     */
    public DefenceTower(DefenceTower other) {
        super(other);
        towerType = other.towerType;
    }

    /**
     * @param other copy constructor
     * @param team  the owner Team
     */
    public DefenceTower(DefenceTower other, Team team) {
        super(other, team);
        towerType = other.towerType;
    }

    /**
     * @param rangeSight      RangeSight of DefenceTower
     * @param damage          Damage of the DefenceTower
     * @param reloadTime      ReloadTime of the attacks of DefenceTower in ms
     * @param aliveObjectType should be aliveObjectType.DEFENCE_TOWER
     * @param towerType type of the DefenceTower
     */
    public DefenceTower(Worth worth, Health health, RangeSight rangeSight, Damage damage, int reloadTime, DefenceTowerTypes towerType, AliveObjectTypes aliveObjectType) {
        super(worth, health, rangeSight, damage, reloadTime, aliveObjectType);
        this.towerType = towerType;
    }

    @Override
    public Cell chooseAttackTarget(Board board) {
        return new AttackStrategy(board).chooseTarget();
    }

    public int getDefenceTowerID() {
        return defenceTowerID;
    }

    /**
     * @return const of Upgrading defenceTower's Damage one level
     */
    public double getDamageUpgradeCost() {
        return DamageConstants.COST_COEF * worth.getValue();
    }

    /**
     * Upgrade defenceTower's Damage one level
     */
    public void upgradeDamage() {
        damage.levelUp();
        damage.multiply(1 + DamageConstants.HP_ADD_COEF);
        worth.multiplyValue(1 + DamageConstants.VALUE_ADD_COEF);
    }

    /**
     * @return cost of upgrading defenceTower's RangeSight one level
     */
    public double getRangeSightUpgradeCost() {
        if (rangeSight.getLevel() >= 3)
            return 1e9;
        return RangeSightConstants.COST_COEF * worth.getValue();
    }

    /**
     * upgrade defenceTower's RangeSight one level
     */
    public void upgradeRangeSight() {
        rangeSight.levelUp();
        rangeSight.add(RangeSightConstants.RANGE_SIGHT_ADD);
        worth.multiplyValue(1 + RangeSightConstants.COST_COEF);
    }

    @Override
    public HashMap<String, Integer> getJudgeInfo() {
        HashMap<String, Integer> info = super.getJudgeInfo();

        info.put(JudgeInfoTypes.INFANTRY_ATTACK, new Double(damage.getDamage(AliveObjectTypes.INFANTRY)).intValue());
        info.put(JudgeInfoTypes.TANK_ATTACK, new Double(damage.getDamage(AliveObjectTypes.CAVALRY)).intValue());

        return info;
    }

    /**
     * an InnerClass to handle strategy of DefenceTower's attacks
     */
    private class AttackStrategy {
        private Board board;

        /**
         * @param board the Board which the DefenceTower is in
         */
        private AttackStrategy(Board board) {
            this.board = board;
        }

        /**
         * @return best Cell to attack to based on properties and conditions of the defenceTower
         */
        private Cell chooseTarget() {
            Cell[] cells = board.getVisibleCells(cell, rangeSight.getRangeSight());

            Cell targetCell = getBestAttackForceCell(cells);
            if (targetCell != null)
                return targetCell;

            return getEnemyHero(cells);
        }

        /**
         * @param cells the Cells defenceTower can attack to
         * @return the cell containing enemyHero among cells or null if there is none
         */
        private Cell getEnemyHero(Cell[] cells) {
            for (Cell cell : cells)
                if (hasEnemyAliveObjectType(cell, AliveObjectTypes.HERO))
                    return cell;
            return null;
        }

        /**
         * @param cells the Cells defenceTower can attack to
         * @return best Cell to attack to, among cells, and based on AttackerForces' information alone
         */
        private Cell getBestAttackForceCell(Cell[] cells) {
            Cell targetCell = null;

            for (Cell cell : cells)
                if (cellHasEnemyAttackForce(cell) && isBetterCellToHitEnemyAttackForces(cell, targetCell))
                    targetCell = cell;

            return targetCell;
        }

        /**
         * @param attackForce attackerForce that we might want to kill
         * @return minimum number of hits to kill attackForce
         */
        private int minHitToKillAttackForce(AliveObject attackForce) {
            double attDamage = damage.getDamage(attackForce.getAliveObjectType());
            return (int) Math.ceil(attackForce.getHealthHp() / attDamage);
        }

        /**
         * @param cell the Cell which defenceTower might want to attack you
         * @return the minimum number of hits to kill at least one AttackForce in cell
         * @throws RuntimeException if there is no AttackForce in cell
         */
        private int minHitToKillAttackForceInCell(Cell cell) throws RuntimeException {
            Integer minHitToKillOne = null;

            for (AliveObject attackForce : getCellEnemyAttackForces(cell)) {
                int attackForceMinHit = minHitToKillAttackForce(attackForce);
                if (minHitToKillOne == null || attackForceMinHit < minHitToKillOne)
                    minHitToKillOne = attackForceMinHit;
            }

            if (minHitToKillOne == null)
                throw new RuntimeException("Cell without attack force to kill.");

            return minHitToKillOne;
        }

        /**
         * @return true if based on AttackerForces' information alone, cell1 is better to be attacked than cell2
         * @throws RuntimeException if cell1 == null
         */
        private boolean isBetterCellToHitEnemyAttackForces(Cell cell1, Cell cell2) throws RuntimeException {
            if (cell1 == null)
                throw new RuntimeException("Cell1 should not be null to compare.");
            if (cell2 == null)
                return true;

            int hit1 = minHitToKillAttackForceInCell(cell1);
            int hit2 = minHitToKillAttackForceInCell(cell1);
            if (hit1 < hit2)
                return true;
            else if (hit1 != hit2)
                return false;

            // TODO: Here movement progressValue should be prioritized

            return compareCellByValueOfAttackForces(cell1, cell2) > 0;
        }
    }
}
