package dota.game.objects.alive.forces;

import dota.game.board.Cell;
import dota.game.board.Team;
import dota.game.objects.TeamObject;
import dota.game.objects.alive.AliveObject;
import dota.game.objects.alive.OffensiveObject;
import dota.game.objects.map.Path;
import dota.game.utils.properties.Damage;
import dota.game.utils.properties.Health;
import dota.game.utils.properties.RangeSight;
import dota.game.utils.properties.Worth;
import dota.utils.Coordinate;
import dota.utils.types.AliveObjectTypes;
import dota.utils.types.JudgeInfoTypes;
import dota.utils.types.NetworkInfoTypes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

/**
 * Logic version of GameForce
 */
abstract public class GameForce extends AliveObject implements OffensiveObject {
    private static int gameForceCounter;
    private final int gameForceID;
    protected Cell cell;
    protected RangeSight rangeSight;
    protected Damage damage;
    protected int reloadTime;
    protected int speed;
    protected Path path;
    protected Integer lastAttackTime;

    {
        this.gameForceID = gameForceCounter++;
    }

    /**
     * @param other copy constructor
     */
    public GameForce(GameForce other) {
        super(other);
        copyData(other);
    }

    /**
     * @param other copy constructor
     * @param team  the owner of the GameForce
     */
    public GameForce(GameForce other, Team team) {
        super(other, team);
        copyData(other);
    }

    /**
     * @param rangeSight the range which the GameForce can see and attack
     * @param damage     Damage of the GameForce
     * @param reloadTime the time between two gameForce Attacks in ms
     */
    public GameForce(Worth worth, Health health, RangeSight rangeSight, Damage damage, int reloadTime, AliveObjectTypes aliveObjectType) {
        super(worth, health, aliveObjectType);
        this.rangeSight = new RangeSight(rangeSight);
        this.damage = new Damage(damage);
        this.reloadTime = reloadTime;
    }

    /**
     * @param speed movement speed of the GameForce in ms/tiles
     */
    public GameForce(Worth worth, Health health, RangeSight rangeSight, Damage damage, int reloadTime, int speed, AliveObjectTypes aliveObjectType) {
        this(worth, health, rangeSight, damage, reloadTime, aliveObjectType);
        this.speed = speed;
    }

    /**
     * Copy data of other to this gameForce
     */
    private void copyData(GameForce other) {
        this.rangeSight = new RangeSight(other.rangeSight);
        this.damage = new Damage(other.damage);
        this.reloadTime = other.reloadTime;
        this.speed = other.speed;
    }

    /**
     * @return Cell which the GameForce is in
     */
    public Cell getCell() {
        return cell;
    }

    public void setCell(Cell cell) {
        this.cell = cell;
    }

    public RangeSight getRangeSight() {
        return rangeSight;
    }

    public int getGameForceID() {
        return gameForceID;
    }

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }

    /**
     * @return the last time when the GameForce has attacked or null if it has never attacked
     */
    public Integer getLastAttackTime() {
        return lastAttackTime;
    }

    public void setLastAttackTime(int lastAttackTime) {
        this.lastAttackTime = lastAttackTime;
    }

    public int getReloadTime() {
        return reloadTime;
    }

    public Damage getDamage() {
        return damage;
    }

    @Override
    public boolean isInCell(Cell cell) {
        return this.cell == cell;
    }

    /**
     * @return diffrence of distances of cell1 and cell2 from the gameForce -
     * @throws RuntimeException if cell1 is null
     */
    protected int compareCellByDistance(Cell cell1, Cell cell2) throws RuntimeException {
        if (cell1 == null)
            throw new RuntimeException("Cells to compare by distance should not be null.");
        if (cell2 == null)
            return -1;
        int distanceCell1 = cell1.distanceFrom(cell);
        int distanceCell2 = cell2.distanceFrom(cell);
        return distanceCell1 - distanceCell2;
    }

    /**
     * @return difference of AttackForces' values of cell1 and cell2
     * @throws RuntimeException if cell1 or cell2 is null
     */
    protected int compareCellByValueOfAttackForces(Cell cell1, Cell cell2) throws RuntimeException {
        if (cell1 == null || cell2 == null)
            throw new RuntimeException("Cell1 to compare should not be null.");
        int value1 = getValueOfCellByAttackerForces(cell1);
        int value2 = getValueOfCellByAttackerForces(cell2);
        return value1 - value2;
    }

    /**
     * @return enemy AliveObjects of cell
     */
    protected AliveObject[] getCellEnemyAliveObjects(Cell cell) {
        Vector<AliveObject> aliveObjects = new Vector<>();

        for (AliveObject aliveObject : cell.getAliveObjects())
            if (!TeamObject.onTheSameTeam(this, aliveObject))
                aliveObjects.add(aliveObject);

        return aliveObjects.toArray(new AliveObject[0]);
    }

    /**
     * @return enemy AttackForces of cell
     */
    protected AliveObject[] getCellEnemyAttackForces(Cell cell) {
        Vector<AliveObject> attackForces = new Vector<>();

        for (AliveObject aliveObject : getCellEnemyAliveObjects(cell))
            if (aliveObject.getAliveObjectType() == AliveObjectTypes.INFANTRY ||
                    aliveObject.getAliveObjectType() == AliveObjectTypes.CAVALRY)
                attackForces.add(aliveObject);

        return attackForces.toArray(new AliveObject[0]);
    }

    /**
     * @return true if there is an enemy AliveObject of type aliveObjectType in cell
     */
    protected boolean hasEnemyAliveObjectType(Cell cell, AliveObjectTypes aliveObjectType) {
        for (AliveObject aliveObject : getCellEnemyAliveObjects(cell))
            if (aliveObject.getAliveObjectType() == aliveObjectType)
                return true;
        return false;
    }

    /**
     * @return true if there is an enemy AttackForce in cell
     */
    protected boolean cellHasEnemyAttackForce(Cell cell) {
        return getCellEnemyAttackForces(cell).length > 0;
    }

    /**
     * @return value of cell based on AttackerForces information only
     */
    protected int getValueOfCellByAttackerForces(Cell cell) {
        int value = 0;
        for (AliveObject attackForce : getCellEnemyAttackForces(cell))
            value += attackForce.getValue();
        return value;
    }

    @Override
    public HashMap<String, Integer> getJudgeInfo() {
        HashMap<String, Integer> info = super.getJudgeInfo();

        info.put(JudgeInfoTypes.RANGE, rangeSight.getRangeSight());
        info.put(JudgeInfoTypes.ROW, cell.getX());
        info.put(JudgeInfoTypes.COLUMN, cell.getY());
        info.put(JudgeInfoTypes.RELOAD_TIME, reloadTime);
        info.put(JudgeInfoTypes.VALUE, worth.getValue());

        return info;
    }

    @Override
    public HashMap<String, Object> getNetworkInfo() {
        HashMap<String, Object> info = super.getNetworkInfo();

        info.put(NetworkInfoTypes.VALUE, worth.getValue());

        return info;
    }

    @Override
    public ArrayList<Coordinate> getAllCoordinates() {
        ArrayList<Coordinate> coordinates = new ArrayList<>();
        coordinates.add(new Coordinate(cell.getCoordinate()));
        return coordinates;
    }
}
