package dota.game.objects.alive.buildings;

import dota.game.board.Cell;
import dota.game.board.Team;
import dota.game.utils.properties.Health;
import dota.game.utils.properties.Worth;
import dota.utils.types.AliveObjectTypes;

/**
 * Logic version of the Barrack
 */
public class Barrack extends Building {
    private static int barrackCounter;
    private final int barrackID;

    {
        barrackID = barrackCounter++;
    }

    /**
     * @param aliveObjectType should be AliveObjectType.BARRACK
     */
    public Barrack(Worth worth, Health health, AliveObjectTypes aliveObjectType) {
        super(worth, health, aliveObjectType);
    }

    /**
     * @param other copy constructor
     * @param cells cells in which the Barrack is
     */
    public Barrack(Barrack other, Cell[][] cells) {
        super(other, cells);
    }

    /**
     * @param other copy constructor
     * @param team  the owner Team
     * @param cells cells in which the Barrack is
     */
    public Barrack(Barrack other, Team team, Cell[][] cells) {
        super(other, team, cells);
    }

    public int getBarrackID() {
        return barrackID;
    }
}
