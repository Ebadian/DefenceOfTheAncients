package dota.game.objects.alive.buildings;

import dota.game.board.Cell;
import dota.game.board.Team;
import dota.game.utils.properties.Health;
import dota.game.utils.properties.Worth;
import dota.utils.types.AliveObjectTypes;

/**
 * Logic version of Ancient
 */
public class Ancient extends Building {
    private static int ancientCounter;
    private final int ancientID;

    {
        ancientID = ancientCounter++;
    }

    /**
     * @param aliveObjectType should be AliveObjectType.ANCIENT
     */
    public Ancient(Worth worth, Health health, AliveObjectTypes aliveObjectType) {
        super(worth, health, aliveObjectType);
    }

    /**
     * @param other copy constructor
     * @param cells Cells in which the Ancient is
     */
    public Ancient(Ancient other, Cell[][] cells) {
        super(other, cells);
    }

    /**
     * @param other copy constructor
     * @param team  owner Team
     * @param cells Cells in which the Ancient is
     */
    public Ancient(Ancient other, Team team, Cell[][] cells) {
        super(other, team, cells);
    }

    public int getAncientID() {
        return ancientID;
    }

    /**
     * @return the middle Cell of the Ancient
     */
    public Cell getCenterCell() {
        return cells[getRows() / 2][getColumns() / 2];
    }
}
