package dota.game.objects.alive.buildings;

import dota.game.board.Cell;
import dota.game.board.Team;
import dota.game.objects.alive.AliveObject;
import dota.game.utils.properties.Health;
import dota.game.utils.properties.Worth;
import dota.utils.Coordinate;
import dota.utils.types.AliveObjectTypes;

import java.util.ArrayList;
import java.util.List;

/**
 * Logic version of Building
 */
public class Building extends AliveObject {
    private static int buildingCounter;
    private final int buildingID;
    protected Cell[][] cells;

    {
        buildingID = buildingCounter++;
    }

    /**
     * @param aliveObjectType AliveObjectType of the ‌Building
     */
    public Building(Worth worth, Health health, AliveObjectTypes aliveObjectType) {
        super(worth, health, aliveObjectType);
    }

    /**
     * @param other copy constructor
     * @param cells Cells in which the Building is
     */
    public Building(Building other, Cell[][] cells) {
        super(other);
        this.cells = cells;
    }

    /**
     * @param other copy constructor
     * @param team  the owner Team of the Building
     * @param cells Cells in which the Building is
     */
    public Building(Building other, Team team, Cell[][] cells) {
        super(other, team);
        this.cells = cells;
    }

    /**
     * An building is shaped like a rectangle
     *
     * @return number of columns of the building
     */
    public int getColumns() {
        return cells.length;
    }

    /**
     * An building is shaped like a rectangle
     *
     * @return number of rows of the building
     */
    public int getRows() {
        return cells[0].length;
    }

    public Cell[][] getCells() {
        return cells;
    }

    @Override
    public boolean isInCell(Cell other) {
        for (Cell rowsOfCell[] : cells)
            for (Cell cell : rowsOfCell)
                if (cell == other)
                    return true;
        return false;
    }

    public int getBuildingID() {
        return buildingID;
    }

    @Override
    public ArrayList<Coordinate> getAllCoordinates() {
        ArrayList<Coordinate> coordinates = new ArrayList<>();

        for (Cell[] cells : this.cells)
            for (Cell cell : cells)
                coordinates.add(new Coordinate(cell.getCoordinate()));

        return coordinates;
    }
}
