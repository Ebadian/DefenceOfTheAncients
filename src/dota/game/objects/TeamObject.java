package dota.game.objects;

import dota.game.board.Team;

/**
 * Interface for all object who are part of a Team
 */
public interface TeamObject {
    static boolean onTheSameTeam(TeamObject teamObject1, TeamObject teamObject2) {
        if (teamObject1 == null || teamObject2 == null ||
                teamObject1.getTeam() == null || teamObject2.getTeam() == null)
            return false;
        return teamObject1.getTeam().getTeamID() == teamObject2.getTeam().getTeamID();
    }

    Team getTeam();
}
