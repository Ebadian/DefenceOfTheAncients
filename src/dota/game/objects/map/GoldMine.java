package dota.game.objects.map;

import dota.game.board.Cell;
import dota.game.board.Team;
import dota.game.objects.GameObject;
import dota.game.settings.WorthConstants;
import dota.server.NetworkMessages;
import dota.utils.Coordinate;
import dota.utils.types.NetworkInfoTypes;

import java.util.ArrayList;
import java.util.HashMap;

public class GoldMine extends GameObject {
    private final int value = WorthConstants.GOLD_MINE_MONEY;
    private Cell cell;
    private Team owner;
    private boolean destroyed = false;

    public GoldMine(Cell cell) {
        this.cell = cell;
    }

    public Cell getCell() {
        return cell;
    }

    public void destroy() {
        destroyed = true;
    }

    public Team getOwner() {
        return owner;
    }

    public void setOwner(Team owner) throws Exception {
        if (!canSetOwner())
            throw new Exception("Can not set the owner of a completely destroyed GoldMine");
        this.owner = owner;
    }

    public int getValue() {
        return value;
    }

    public boolean isDestroyed() {
        return destroyed;
    }

    public boolean canSetOwner() {
        return !destroyed && owner == null;
    }

    public HashMap<String, Object> getNetworkInfo() {
        HashMap<String, Object> info = super.getNetworkInfo();

        info.put(NetworkInfoTypes.GOLD_MINE_OWNER, getOwner() == null ? null : getOwner().getTeamID());
        info.put(NetworkInfoTypes.PLACE, getAllCoordinates());
        info.put(NetworkMessages.GOLDMINE_DESTROYED, destroyed);

        return info;
    }

    public ArrayList<Coordinate> getAllCoordinates() {
        ArrayList<Coordinate> coordinates = new ArrayList<>();
        coordinates.add(new Coordinate(cell.getCoordinate()));
        return coordinates;
    }
}
