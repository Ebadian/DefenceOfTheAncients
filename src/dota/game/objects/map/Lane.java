package dota.game.objects.map;

import dota.game.board.Cell;
import dota.game.objects.GameObject;
import dota.utils.Coordinate;

import java.util.ArrayList;
import java.util.List;

public class Lane extends GameObject {
    private static int laneCounter = 0;
    private final int laneID;
    private Cell[] cells;

    {
        laneID = laneCounter++;
    }

    public Lane(Cell[] cells) {
        this.cells = cells;
    }

    public Cell getCell(int index) {
        return cells[index];
    }

    public int getDefaultHeadIndex() {
        return 0;
    }

    public int getDefaultTailIndex() {
        return cells.length - 1;
    }

    public int getNextIndex(int ind) throws RuntimeException {
        if (ind == getDefaultTailIndex())
            throw new RuntimeException("Cannot move forwards further than tail of the lane.");
        return ind + 1;
    }

    public int getPreviousIndex(int ind) throws RuntimeException {
        if (ind == getDefaultHeadIndex())
            throw new RuntimeException("Cannot move backwards further than head of the lane.");
        return ind - 1;
    }

    public int getLaneID() {
        return laneID;
    }

    public List<Coordinate> getAllLaneCoordinates() {
        List<Coordinate> coordinates = new ArrayList<>();
        for (Cell cell : cells)
            coordinates.add(new Coordinate(cell.getCoordinate()));
        return coordinates;
    }
}
