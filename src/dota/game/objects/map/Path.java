package dota.game.objects.map;

import dota.game.objects.GameObject;

public class Path extends GameObject {
    private static int pathCounter = 0;
    private final int pathID;
    private Lane[] lanes;

    {
        pathID = pathCounter++;
    }

    public Path(Lane[] lanes) {
        this.lanes = lanes;
    }

    public Lane getLane(int index) {
        return lanes[index];
    }

    public Lane[] getLanes() {
        return lanes.clone();
    }

    public int getPathID() {
        return pathID;
    }
}
