package dota.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class AcceptPlayer extends Thread {
    private ServerSocket serverSocket;
    private DotAServer dotAServer;

    private AcceptPlayer(ServerSocket serverSocket, DotAServer dotAServer) {
        this.serverSocket = serverSocket;
        this.dotAServer = dotAServer;
        setDaemon(true);
    }

    public static AcceptPlayer newAcceptPlayer(ServerSocket serverSocket, DotAServer dotAServer) {
        return new AcceptPlayer(serverSocket, dotAServer);
    }

    @Override
    public void run() {
        try {
            while (true) {
                Socket socket = serverSocket.accept();
                Player player = new Player(dotAServer, socket);
                player.start();
            }
        } catch (IOException e) {
            System.err.println("Error on connection accepting");
            e.printStackTrace();
        } finally {
            try {
                serverSocket.close();
            } catch (IOException e) {
                System.err.println("Error on connection close");
                e.printStackTrace();
            }
        }
    }
}
