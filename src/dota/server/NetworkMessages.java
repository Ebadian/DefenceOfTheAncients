package dota.server;

public class NetworkMessages {
    public static final String MAP_PATH = "mapPath";
    public static final String PLAYER_LIST = "playerList";

    public static final String GOLD_MINES = "GoldMines";
    public static final String PATHS = "Paths";
    public static final String ANCIENT = "Ancient";
    public static final String BARRACKS = "Barracks";
    public static final String TIME = "Time";
    public static final String ALIVE_OBJECT_DATA = "AliveObjectData";
    public static final String PATH_LANE_IDS = "PathLaneIDs";
    public static final String TEAM_MONEY = "teamMoney";
    public static final String END_GAME = "END_GAME";
    public static final String GOLD_MINE_AUTHORITY_UPDATE = "GoldMineAuthorityUpdate";
    public static final String GOLDMINE_DESTROYED = "DESTROYED";
}
