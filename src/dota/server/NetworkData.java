package dota.server;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class NetworkData implements Serializable {
    Map<String, Object> data = new HashMap<>();

    public NetworkData(String key, Object value) {
        put(key, value);
    }

    public NetworkData() {
    }

    public void put(String key, Object value) {
        data.put(key, value);
    }

    public Object get(String key) {
        return data.get(key);
    }

    public boolean contains(String key) {
        return data.containsKey(key);
    }

    public Map<String, Object> getData() {
        return data;
    }

    @Override
    public String toString() {
        return data.toString();
    }
}
