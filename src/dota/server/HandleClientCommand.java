package dota.server;

import dota.client.NetworkCommandMessages;
import dota.common.GameObjectID;
import dota.game.core.GameManager;
import dota.utils.Coordinate;
import dota.utils.types.AttackerUnitTypes;
import dota.utils.types.DefenceTowerTypes;

public class HandleClientCommand {
    private static GameManager gameManager;

    public static void setGameManager(GameManager gameManager) {
        HandleClientCommand.gameManager = gameManager;
    }

    public static synchronized void handle(NetworkData data, int teamID) {
        if (data.get(NetworkCommandMessages.COMMAND_TYPE).equals(NetworkCommandMessages.CREATE_ATTACKER_UNIT))
            handleCreateAttackerUnit(data, teamID);
        if (data.get(NetworkCommandMessages.COMMAND_TYPE).equals(NetworkCommandMessages.CREATE_DEFENCE_TOWER))
            handleCreateDefenceTower(data, teamID);
        if (data.get(NetworkCommandMessages.COMMAND_TYPE).equals(NetworkCommandMessages.SET_HERO_TARGET))
            handleSetHeroTarget(data, teamID);
    }

    private static void handleSetHeroTarget(NetworkData data, int teamID) {
        Coordinate coordinate = (Coordinate) data.get(NetworkCommandMessages.ABSOLUTE_POSITION);
        gameManager.setHeroTarget(teamID, coordinate);
    }

    private static void handleCreateAttackerUnit(NetworkData data, int teamID) {
        AttackerUnitTypes attackerUnitTypes = (AttackerUnitTypes) data.get(NetworkCommandMessages.ATTACKER_UNIT_TYPE);
        GameObjectID laneID = (GameObjectID) data.get(NetworkCommandMessages.COMMAND_LANE_POSITION);
        try {
            gameManager.createAttackerUnit(teamID, attackerUnitTypes, laneID);
        } catch (Exception e) {
            System.err.println("Creating attacker unit failed");
            e.printStackTrace();
        }
    }

    private static void handleCreateDefenceTower(NetworkData data, int teamID) {
//        System.out.println("Creating tower defence");
        DefenceTowerTypes defenceTowerTypes = (DefenceTowerTypes) data.get(NetworkCommandMessages.DEFENCE_TOWER_TYPE);
        Coordinate coordinate = (Coordinate) data.get(NetworkCommandMessages.ABSOLUTE_POSITION);
        try {
            gameManager.createDefenceTower(teamID, defenceTowerTypes, coordinate);
        } catch (Exception e) {
            System.err.println("Creating defence tower failed");
            e.printStackTrace();
        }
    }
}
