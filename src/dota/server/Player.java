package dota.server;

import dota.client.NetworkCommandMessages;
import dota.client.objects.DotAUser;
import dota.game.board.Board;
import dota.game.objects.GameObject;
import dota.game.objects.alive.AliveObject;
import dota.game.objects.map.GoldMine;
import dota.game.settings.General;
import dota.utils.types.TeamTypes;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class Player extends Thread {
    private DotAUser user;

    private Socket socket;
    private ObjectInputStream in;
    private ObjectOutputStream out;
    private DotAServer dotAServer;

    public Player(DotAServer dotAServer, Socket socket) {
        this.socket = socket;
        this.dotAServer = dotAServer;
        this.setDaemon(true);
    }

    public int getTeamID() {
        return user.getTeamID();
    }

    public DotAUser getUser() {
        return user;
    }

    public void run() {
        try {
            initIO();

            initPlayerConnection();

            NetworkData data;
            while ((data = (NetworkData) in.readObject()) != null) {
//                System.out.println("new raw message " + data + " from Player #" + getPlayerID());
                if (data.contains(NetworkCommandMessages.COMMAND_TYPE))
                    HandleClientCommand.handle(data, user.getTeamID());
            }
        } catch (ClassCastException e) {
            System.err.println("Casting exception");
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        } catch (Exception e) {
            System.err.println("Irrational exception : " + e.getMessage());
            e.printStackTrace();
        } finally {
            killPlayer();
        }
    }

    private void initPlayerConnection() throws IOException, ClassNotFoundException {
        String playerName = (String) in.readObject();
        int teamID = (Integer) in.readObject();
        user = new DotAUser(teamID, playerName);
//        System.out.println("Player #" + user.getPlayerID() + "\'s name is " + playerName + " from team " + teamID + " connected " + socket);

        tellPlayer(staticMapData());
        dotAServer.addPlayer(Player.this);

        dotAServer.broadcastToAll(dotAServer.getAllPlayersNameData());
    }

    private void initIO() throws IOException {
        out = new ObjectOutputStream(socket.getOutputStream());
        out.flush();

        in = new ObjectInputStream(socket.getInputStream());
    }

    public void killPlayer() {
        // Remove from all places it is saved in
        dotAServer.removePlayer(Player.this);

        try {
            socket.close();
        } catch (IOException e) {
            System.err.println("Closing socket failed");
            e.printStackTrace();
        }

        dotAServer.broadcastToAll(dotAServer.getAllPlayersNameData());

//        System.out.println("Connection closed");
    }

    public void tellPlayer(Object object) {
        try {
            out.writeObject(object);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private NetworkData staticMapData() {
        NetworkData networkData = new NetworkData();
        networkData.put(NetworkMessages.MAP_PATH, dotAServer.getMapPath());
        networkData.put(NetworkMessages.PATH_LANE_IDS, dotAServer.getGameManager().getAllLaneIDs());
        networkData.put(NetworkMessages.GOLD_MINES, dotAServer.getGameManager().getGoldMineIDs());
        networkData.put(NetworkMessages.PATHS, dotAServer.getGameManager().getPathIDs());
        networkData.put(NetworkMessages.ANCIENT + General.SENTINEL,
                dotAServer.getGameManager().getTeamAncientID(General.SENTINEL));
        networkData.put(NetworkMessages.BARRACKS + General.SENTINEL,
                dotAServer.getGameManager().getTeamBarracksID(General.SENTINEL));
        networkData.put(NetworkMessages.ANCIENT + General.SCOURGE,
                dotAServer.getGameManager().getTeamAncientID(General.SCOURGE));
        networkData.put(NetworkMessages.BARRACKS + General.SCOURGE,
                dotAServer.getGameManager().getTeamBarracksID(General.SCOURGE));
        networkData.put(NetworkMessages.TIME, dotAServer.getGameManager().getTime());
        return networkData;
    }

    private NetworkData getBoardUpdatableObjectInfos() {
        NetworkData data = new NetworkData();

        Set<GameObject> updatableGameObjects = new HashSet<>();
        Board board = dotAServer.getGameManager().getBoard();
        for (int i = 0; i < board.getRows(); i++)
            for (int j = 0; j < board.getColumns(); j++)
                for (AliveObject aliveObject : board.getCell(i, j).getAliveObjects())
                    updatableGameObjects.add(aliveObject);

        ArrayList<HashMap> gameObjects = new ArrayList<>();

        for (GameObject game : updatableGameObjects)
            gameObjects.add(game.getNetworkInfo());

        for (GoldMine goldMine : dotAServer.getGameManager().getBoard().getGoldMines())
            gameObjects.add(goldMine.getNetworkInfo());

        data.put(NetworkMessages.ALIVE_OBJECT_DATA, gameObjects);

        return data;
    }

    public int getPlayerID() {
        return user.getPlayerID();
    }

    public String getPlayerName() {
        return user.getPlayerName();
    }

    public void sendUpdatedData() {
        tellPlayer(getUpdatedMapData());
    }

    public NetworkData getUpdatedMapData() {
        NetworkData data = getBoardUpdatableObjectInfos();

        for (TeamTypes teamTypes : TeamTypes.values())
            data.put(NetworkMessages.TEAM_MONEY + teamTypes.getNum(), dotAServer.getGameManager().getMoney(teamTypes.getNum()));

        return data;
    }
}
