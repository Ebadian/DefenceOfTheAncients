package dota.server;

import dota.UI.utililty.Responder;
import dota.game.core.GameManager;
import dota.game.settings.General;
import dota.utils.types.TeamTypes;

public class GameRunner extends Thread {
    private GameManager gameManager;
    private DotAServer dotAServer;

    private boolean paused = false;

    private GameRunner(GameManager gameManager, DotAServer dotAServer) {
        this.gameManager = gameManager;
        this.dotAServer = dotAServer;
        this.setDaemon(true);
    }

    public static GameRunner newGameRunner(GameManager gameManager, DotAServer dotAServer) {
        return new GameRunner(gameManager, dotAServer);
    }

    @Override
    public void run() {
//        System.out.println("START OF GAME RUNNER");
        gameManager.setUp();
        while (true) {
            try {
                Thread.sleep(General.TIME_UNIT);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (!paused) {
                gameManager.nextTimeUnit();
                TeamTypes winner = null;
                if (gameManager.getBoard().getTeamAncient(General.SENTINEL).isDead())
                    winner = TeamTypes.SCOURGE;
                if (gameManager.getBoard().getTeamAncient(General.SCOURGE).isDead())
                    winner = TeamTypes.SENTINEL;
                if (winner != null) {
                    dotAServer.broadcastToAll(new NetworkData(NetworkMessages.END_GAME, winner));
                    break;
                }
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        dotAServer.sendUpdatedDataForMap();
                    }
                }).start();
            }
        }
    }

    public void pauseGame() {
        paused = true;
    }

    public void resumeGame() {
        paused = false;
    }
}
