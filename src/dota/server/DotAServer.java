package dota.server;

import dota.UI.menu.Controller;
import dota.client.objects.DotAUser;
import dota.game.core.GameManager;
import dota.utils.MapFileLoader;
import dota.utils.types.TeamTypes;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.*;

public class DotAServer {
    public static final int PORT = 8085;
    private GameRunner gameRunner;
    private GameManager gameManager;
    private ServerSocket serverSocket;
    private Set<Player> players = Collections.synchronizedSet(new HashSet<>());
    private String mapPath = "statics/map1.dtm";

    private Controller controller;

    public DotAServer(String mapPath, Controller controller) {
        this.mapPath = mapPath;
        this.controller = controller;
    }

    public void start() throws IOException {
        gameManager = new GameManager();
        HandleClientCommand.setGameManager(gameManager);
        try {
            MapFileLoader map = MapFileLoader.newMapLoader(mapPath);
            setGameManager(map);
        } catch (FileNotFoundException e) {
            System.err.println("Error caught while loading map: " + e.getMessage());
        }

        serverSocket = new ServerSocket(PORT);
        AcceptPlayer.newAcceptPlayer(serverSocket, DotAServer.this).start();
    }

    public void startGame() {
        gameRunner = GameRunner.newGameRunner(gameManager, DotAServer.this);
//        System.out.println("GAME IS ON!");
        gameRunner.start();
    }

    public NetworkData getAllPlayersNameData() {
        List<DotAUser> playerNames = new ArrayList<>();

        synchronized (players) {
            for (Player player : players)
                playerNames.add(player.getUser());
        }

        return new NetworkData(NetworkMessages.PLAYER_LIST, playerNames);
    }

    /**
     * @param teamID return all players if null
     * @return returns list of corresponding players
     */
    private List<String> getTeamPlayerNames(Integer teamID) {
        List<String> playerNames = new ArrayList<>();

        synchronized (players) {
            for (Player player : players)
                if (teamID == null || teamID == player.getTeamID())
                    playerNames.add(player.getPlayerName());
        }

        return playerNames;
    }

    public void broadcastToAll(Object message) {
        synchronized (players) {
            for (Player player : players)
                player.tellPlayer(message);
        }
    }

    public void addPlayer(Player player) {
        synchronized (players) {
            players.add(player);
        }
        updateControllerTeamPlayersList(player.getTeamID());
    }

    public void removePlayer(Player player) {
        synchronized (players) {
            players.remove(player);
        }
        updateControllerTeamPlayersList(player.getTeamID());
    }

    public void updateControllerTeamPlayersList(int teamID) {
        List<String> teamPlayerNames = getTeamPlayerNames(teamID);
        TeamTypes teamType = TeamTypes.getTeamType(teamID);
//        System.out.println("Sending to controller " + teamPlayerNames + " team type " + teamType);
        controller.resetPlayers(teamPlayerNames, teamType);
    }

    public String getMapPath() {
        return mapPath;
    }

    public GameManager getGameManager() {
        return gameManager;
    }

    private void setGameManager(MapFileLoader map) {
        gameManager.buildFromMapFileLoader(map);
    }

    public GameRunner getGameRunner() {
        return gameRunner;
    }

    public void sendUpdatedDataForMap() {
        synchronized (players) {
            for (Player player : players)
                player.sendUpdatedData();
        }
    }
}
