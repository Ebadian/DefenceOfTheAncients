package dota.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class MapFileLoader {
    private int rows, columns;
    private Scanner scanner;
    private ArrayList<ArrayList<ArrayList<Coordinate>>> paths;
    private ArrayList<ArrayList<Coordinate[][]>> barracks;
    private ArrayList<Coordinate[][]> ancient;
    private ArrayList<Coordinate> goldMines;

    private MapFileLoader(String mapFilePath) throws FileNotFoundException {
        scanner = new Scanner(new FileInputStream(mapFilePath));

        rows = scanner.nextInt();
        columns = scanner.nextInt();

        readPaths();

        ancient = new ArrayList<>();
        barracks = new ArrayList<>();

        // Read First team base
        ancient.add(readBuilding());
        barracks.add(readBarracks());

        // Read Second Team Base
        ancient.add(readBuilding());
        barracks.add(readBarracks());

        goldMines = readArrayCoordinate();
    }

    public static MapFileLoader newMapLoader(String path) throws FileNotFoundException {
        MapFileLoader mapFileLoader = new MapFileLoader(path);
        return mapFileLoader;
    }

    private Coordinate readCoordinate() {
        int x = scanner.nextInt();
        int y = scanner.nextInt();
        return new Coordinate(x, y);
    }

    private ArrayList<Coordinate> readArrayCoordinate() {
        int length = scanner.nextInt();

        ArrayList<Coordinate> coordinates = new ArrayList<>();
        for (int i = 0; i < length; i++)
            coordinates.add(readCoordinate());

        return coordinates;
    }

    private void readPaths() {
        ArrayList<ArrayList<ArrayList<Coordinate>>> paths = new ArrayList<>();

        int numPaths = scanner.nextInt();
        for (int o = 0; o < numPaths; o++) {
            int numLanes = scanner.nextInt();
            ArrayList<ArrayList<Coordinate>> path = new ArrayList<>();
            for (int i = 0; i < numLanes; i++)
                path.add(readArrayCoordinate());
            paths.add(path);
        }

        this.paths = paths;
    }

    private Coordinate[][] readBuilding() {
        int rows = scanner.nextInt();
        int columns = scanner.nextInt();
        Coordinate[][] building = new Coordinate[rows][columns];

        for (int i = 0; i < rows; i++)
            for (int j = 0; j < columns; j++)
                building[i][j] = readCoordinate();

        return building;
    }

    private ArrayList<Coordinate[][]> readBarracks() {
        int numBarracks = scanner.nextInt();

        ArrayList<Coordinate[][]> barracks = new ArrayList<>();
        for (int i = 0; i < numBarracks; i++)
            barracks.add(readBuilding());

        return barracks;
    }

    public int getRows() {
        return rows;
    }

    public int getColumns() {
        return columns;
    }

    public ArrayList<ArrayList<ArrayList<Coordinate>>> getPaths() {
        return paths;
    }

    public ArrayList<ArrayList<Coordinate[][]>> getBarracks() {
        return barracks;
    }

    public ArrayList<Coordinate[][]> getBarrack(int teamID) {
        return barracks.get(teamID);
    }

    public ArrayList<Coordinate[][]> getAncients() {
        return ancient;
    }

    public Coordinate[][] getAncient(int teamID) {
        return ancient.get(teamID);
    }

    public ArrayList<Coordinate> getGoldMines() {
        return goldMines;
    }
}
