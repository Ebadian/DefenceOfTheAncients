package dota.utils.types;

import java.io.Serializable;

public enum UnitTypes implements Serializable {
    HERO(0), TOWER(1), ATTACKERUNIT(2);

    private int num;

    UnitTypes(int num) {
        this.num = num;
    }

    public int getNum() {
        return num;
    }
}
