package dota.utils.types;

import dota.UI.settings.Images;

import java.io.Serializable;

/**
 * different kinds of towers the game has
 */
public enum DefenceTowerTypes implements Serializable {
    FIRE_TOWER(Images.FIRE_TOWER_URL, "FireTower"),
    STONE_TOWER(Images.STONE_TOWER_URL, "StoneTower"),
    BLACK_TOWER(Images.BLACK_TOWER_URL, "BlackTower"),
    POISON_TOWER(Images.POISON_TOWER_URL, "PoisonTower");

    private String url, name;

    DefenceTowerTypes(String url, String name) {
        this.url = url;
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }
}
