package dota.utils.types;

import dota.UI.settings.Images;

import java.io.Serializable;

/**
 * Different kinds of upgrading a user can apply to a tower
 */
public enum DefenceTowerUpgradeTypes implements Serializable {
    DAMAGE_UPGRADE(Images.UPGRADE_DAMAGE_URL, "Damage Up"), RANGE_UPGRADE(Images.UPGRADE_RANGE_URL, "Range Up");

    private String url;
    private String name;

    DefenceTowerUpgradeTypes(String url, String name) {
        this.url = url;
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }
}
