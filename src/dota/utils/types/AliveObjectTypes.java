package dota.utils.types;

import java.io.Serializable;

/**
 * different kinds of alive objects game has
 */
public enum AliveObjectTypes implements Serializable {
    HERO, INFANTRY, CAVALRY, DEFENCE_TOWER, ANCIENT, BARRACK
}
