package dota.utils.types;

import dota.UI.settings.Images;

import java.io.Serializable;

/**
 * different kinds of attacker units
 */
public enum AttackerUnitTypes implements Serializable {
    INFANTRY(Images.INFANTRY_URL, "Infantry"), CAVALRY(Images.CAVALRY_URL, "Cavalry");

    private String url;
    private String name;

    AttackerUnitTypes(String url, String name) {
        this.url = url;
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }
}
