package dota.utils.types;

import dota.UI.settings.Images;
import dota.game.objects.alive.buildings.Ancient;
import dota.game.objects.alive.buildings.Barrack;
import dota.game.objects.alive.forces.AttackerUnit;
import dota.game.objects.alive.forces.DefenceTower;
import dota.game.objects.alive.forces.Hero;
import dota.game.objects.map.GoldMine;
import javafx.scene.image.Image;

import java.io.Serializable;

public enum GameObjectTypes implements Serializable {
    HERO(0, Images.HERO_URL, Hero.class),
    ATTACKER_UNIT(0, Images.ATTACKER_UNIT_URL, AttackerUnit.class),
    DEFENCE_TOWER(0, Images.DEFENCE_TOWER_URL, DefenceTower.class),
    GOLDMINE(1, Images.GOLDMINE_URL, GoldMine.class),
    ANCIENT(1, Images.ANCIENT_URL, Ancient.class),
    BARRACK(1, Images.BARRACK_URL, Barrack.class);

    private int type;
    private String url;
    private Class className;
    private Image smallImage, largeImage;

    GameObjectTypes(int type, String url, Class className) {
        this.type = type;
        this.url = url;
        this.className = className;
        smallImage = new Image(url, Images.IMAGE_SMALL_SIZE, Images.IMAGE_SMALL_SIZE, false, true, true);
        largeImage = new Image(url, Images.IMAGE_LARGE_SIZE, Images.IMAGE_LARGE_SIZE, false, true, true);
    }

    public int getType() {
        return type;
    }

    public String getUrl() {
        return url;
    }

    public Class getClassName() {
        return className;
    }

    public Image getSmallImage() {
        return smallImage;
    }

    public Image getLargeImage() {
        return largeImage;
    }
}
