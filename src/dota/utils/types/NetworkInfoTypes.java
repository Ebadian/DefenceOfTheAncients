package dota.utils.types;

import java.io.Serializable;

public class NetworkInfoTypes implements Serializable {
    public static final String GAME_ID = "GAME_ID";
    public static final String GOLD_MINE_OWNER = "GOLD_MINE_OWNER";
    public static final String IS_ALIVE = "alive";
    public static final String HEALTH = "health";
    public static final String RANGE = "range";
    public static final String RELOAD_TIME = "time";
    public static final String ATTACK = "attack";
    public static final String SPEED = "speed";
    public static final String VALUE = "value";
    public static final String TEAM_ID = "id";
    public static final String PLACE = "place";
    public static final String ATTACKER_UNIT_TYPE = "attackerUnitType";
}
