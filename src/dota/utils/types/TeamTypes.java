package dota.utils.types;


import dota.UI.settings.Images;
import dota.game.settings.General;

import java.io.Serializable;


public enum TeamTypes implements Serializable {
    SENTINEL(General.SENTINEL, Images.SENTINEL_URL, "Sentinel"), SCOURGE(General.SCOURGE, Images.SCOURGE_URL, "Scourge");

    private int num;
    private String url;
    private String name;

    TeamTypes(int num, String url, String name) {
        this.num = num;
        this.url = url;
        this.name = name;
    }

    public static TeamTypes getTeamType(Integer teamID) {
        if (teamID == null)
            return null;
        for (TeamTypes t : TeamTypes.values())
            if (t.num == teamID)
                return t;
        return null;
    }

    public int getNum() {
        return num;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }
}
