package dota.utils.types;

import dota.UI.settings.Images;

import java.io.Serializable;

/**
 * different kinds of upgrades that can be applied to attacker units
 */
public enum AttackerUnitUpgradeTypes implements Serializable {
    DAMAGE_UPGRADE(Images.UPGRADE_DAMAGE_URL, "Damage Up"),
    HEALTH_UPGRADE(Images.UPGRADE_HEALTH_URL, "Health Up");

    private String url;
    private String name;

    AttackerUnitUpgradeTypes(String url, String name) {
        this.url = url;
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }
}
