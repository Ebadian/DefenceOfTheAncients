package dota.utils.types;

import java.io.Serializable;

/**
 * InfoTypes are string constants used to communicate with Judge getting GameObject info.
 */
public final class JudgeInfoTypes implements Serializable {
    public static final String HEALTH = "HEALTH";
    public static final String RANGE = "range";
    public static final String RELOAD_TIME = "time";
    public static final String ATTACK = "attack";
    public static final String SPEED = "speed";
    public static final String ROW = "row";
    public static final String COLUMN = "col";
    public static final String VALUE = "value";
    public static final String TEAM_ID = "id";
    public static final String IS_ALIVE = "alive";
    public static final String TANK_ATTACK = "TA";
    public static final String INFANTRY_ATTACK = "IA";
}
