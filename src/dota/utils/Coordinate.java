package dota.utils;

import java.io.Serializable;

public class Coordinate implements Serializable {
    private final int x, y;

    public Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Coordinate(Coordinate coordinate) {
        this.x = coordinate.getX();
        this.y = coordinate.getY();
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    /**
     * equality of X, Y is needed
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Coordinate that = (Coordinate) o;

        if (x != that.x) return false;
        return y == that.y;
    }

    /**
     * addition of coordinates as vectors
     *
     * @param coordinate other point for addition
     * @return a new coordinate as result of addition
     */
    public Coordinate plus(Coordinate coordinate) {
        return new Coordinate(x + coordinate.x, y + coordinate.y);
    }

    /**
     * calculate euclidean distance of points
     *
     * @param coordinate coordinate to get distance from
     * @return distance of points
     */
    public double euclideanDistance(Coordinate coordinate) {
        int dx = x - coordinate.x;
        int dy = y - coordinate.y;
        return Math.sqrt(dx * dx + dy * dy);
    }

    /**
     * calculate manhattan distance of points
     *
     * @param coordinate coordinate to get distance from
     * @return distance of points
     */
    public int manhattanDistance(Coordinate coordinate) {
        return Math.abs(x - coordinate.x) + Math.abs(y - coordinate.y);
    }

    /**
     * calculate distance of points by maximum of deltaX and deltaY
     *
     * @param coordinate coordinate to get distance from
     * @return distance of points
     */
    public int maxDxDyDistance(Coordinate coordinate) {
        return Math.max(Math.abs(x - coordinate.x), Math.abs(y - coordinate.y));
    }

    @Override
    public String toString() {
        return "Coordinate{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
