package dota.client;

public class NetworkCommandMessages {

    public static final String COMMAND_TYPE = "CommandType";
    public static final String CREATE_ATTACKER_UNIT = "CreateAttackerUnit";
    public static final String CREATE_DEFENCE_TOWER = "CreateDefenceTower";
    public static final String ATTACKER_UNIT_UPGRADE_GAME_OBJECT = "UpgradeGameObject";
    public static final String UPGRADE_DEFENCE_TOWER = "UpgradeDefenceTower";
    public static final String SET_HERO_TARGET = "SetHeroTarget";
    public static final String COMMAND_LANE_POSITION = "CommandLanePosition";
    public static final String ATTACKER_UNIT_TYPE = "AttackerUnitType";
    public static final String ABSOLUTE_POSITION = "PositionXY";
    public static final String DEFENCE_TOWER_TYPE = "DefenceTowerType";
}
