package dota.client;

import dota.UI.utililty.Responder;
import dota.client.objects.ClientTeamData;
import dota.common.GameObjectID;
import dota.game.board.Board;
import dota.game.board.Cell;
import dota.game.board.Team;
import dota.game.objects.GameObject;
import dota.game.objects.alive.AliveObject;
import dota.game.objects.map.GoldMine;
import dota.game.objects.map.Lane;
import dota.game.objects.map.Path;
import dota.game.settings.General;
import dota.game.settings.upgrade.towers.RangeSightConstants;
import dota.server.NetworkMessages;
import dota.utils.Coordinate;
import dota.utils.MapFileLoader;
import dota.utils.types.NetworkInfoTypes;
import dota.utils.types.TeamTypes;

import java.util.*;

public class ClientGameManager {
    private int time;
    private Board board;
    private Team clientTeam;

    private GameObjectID[] pathGameObjectIDs;
    private GameObjectID[] goldMineGameObjectIDs;
    private ArrayList<GameObjectID[]> lanesIDs;
    private Map<GameObjectID, Map<String, Object>> aliveObjectData = new HashMap<>();

    private Vector<GameObjectID> gameObjectIDsInCell[][];
    private ClientTeamData[] teamsData = new ClientTeamData[2];

    public synchronized void clearAliveObjectData() {
        aliveObjectData.clear();
    }

    public synchronized void addAliveObjectData(GameObjectID gameObjectID, Map<String, Object> data) {
        aliveObjectData.put(gameObjectID, data);
    }

    public void setUp() {
    }

    public boolean isVisibleByTeam(int teamID, Coordinate t) {
        return isVisibleByTeam(teamID, t.getX(), t.getY());
    }

    public boolean isVisibleByTeam(int teamIDTof, int x, int y) {
        Integer teamID = teamIDTof;
        Coordinate init = new Coordinate(x, y);

        if (Responder.isInHalfPath(TeamTypes.getTeamType(teamID), init))
            return true;

        for (int i = -7; i < 7; i++)
            for (int j = -7; j < 7; j++) {
                Coordinate coordinate = new Coordinate(x + i, y + j);
                if (board.isInMap(coordinate) && coordinate.maxDxDyDistance(init) <= 7) {
                    for (GameObjectID gameObjectID : getGameObjectsInCell(coordinate))
                        if (AliveObject.class.isAssignableFrom(gameObjectID.getType()) &&
                                teamID.equals(getAliveObjectData(gameObjectID, NetworkInfoTypes.TEAM_ID)))
                            return true;
                }
            }

        return false;
    }

    public ArrayList<GameObjectID[]> getLanesIDs() {
        return lanesIDs;
    }

    public void setLanesIDs(ArrayList<GameObjectID[]> lanesIDs) {
        this.lanesIDs = lanesIDs;
    }

    public void setMapDimensions(int rows, int columns) {
        board = new Board(rows, columns);
        gameObjectIDsInCell = new Vector[rows][columns];
        for (int i = 0; i < rows; i++)
            for (int j = 0; j < columns; j++)
                gameObjectIDsInCell[i][j] = new Vector<>();
    }

    public synchronized Object getAliveObjectData(GameObjectID gameObjectID, String query) {
        try {
            if (aliveObjectData == null)
                return null;
            if (!aliveObjectData.containsKey(gameObjectID))
                return null;
            if (aliveObjectData.get(gameObjectID) == null)
                return null;
            if (!aliveObjectData.get(gameObjectID).containsKey(query))
                return null;
            return aliveObjectData.get(gameObjectID).get(query);
        } catch (Exception e) {
            return null;
        }
    }

    public void updateGameObjectsInCells() {
        for (int i = 0; i < getBoardRows(); i++)
            for (int j = 0; j < getBoardColumns(); j++)
                gameObjectIDsInCell[i][j].clear();
        for (GameObjectID gameObjectID : aliveObjectData.keySet()) {
            List<Coordinate> place = (List<Coordinate>) getAliveObjectData(gameObjectID, NetworkInfoTypes.PLACE);
            if (place == null)
                continue;
            if (gameObjectID.getType() == GoldMine.class &&
                    getAliveObjectData(gameObjectID, NetworkMessages.GOLDMINE_DESTROYED) != null &&
                    (Boolean) getAliveObjectData(gameObjectID, NetworkMessages.GOLDMINE_DESTROYED) == true)
                continue;
            for (Coordinate coordinate : place) {
                gameObjectIDsInCell[coordinate.getX()][coordinate.getY()].add(gameObjectID);
            }
        }
    }

    public Vector<GameObjectID> getGameObjectsInCell(int x, int y) {
        return (Vector<GameObjectID>) gameObjectIDsInCell[x][y].clone();
    }

    public Vector<GameObjectID> getGameObjectsInCell(Coordinate coordinate) {
        return getGameObjectsInCell(coordinate.getX(), coordinate.getY());
    }

    private GameObjectID[][] mapToLane;
    public TeamTypes[][] mapCellLaneToTeam;

    public void setPaths(ArrayList<ArrayList<ArrayList<Coordinate>>> pathCoordinates) {
        Path[] boardPaths = convertToPaths(pathCoordinates);
        board.setPaths(boardPaths);
    }

    public void setMapToLane() {
        mapToLane = new GameObjectID[getBoardRows()][getBoardColumns()];
        mapCellLaneToTeam = new TeamTypes[getBoardRows()][getBoardColumns()];

        for (int i = 0; i < board.getPaths().length; i++)
            for (int j = 0; j < board.getPathsLanes(i).length; j++) {
                List<Coordinate> laneCoords = board.getPathsLanes(i)[j].getAllLaneCoordinates();
                int index = 0;
                for (Coordinate coordinate : laneCoords) {
                    mapToLane[coordinate.getX()][coordinate.getY()] = lanesIDs.get(i)[j];
                    mapCellLaneToTeam[coordinate.getX()][coordinate.getY()] =
                            index * 2 < laneCoords.size() ? TeamTypes.SENTINEL : TeamTypes.SCOURGE;
                    index++;
                }
            }
    }

    public TeamTypes getMapCellLaneToTeam(Coordinate coordinate) {
        return mapCellLaneToTeam[coordinate.getX()][coordinate.getY()];
    }

    public GameObjectID getLaneAccordingTo(int x, int y) {
        return mapToLane[x][y];
    }

    public GameObjectID getLaneAccordingTo(Coordinate coordinate) {
        return getLaneAccordingTo(coordinate.getX(), coordinate.getY());
    }

    public void addGoldMines(ArrayList<Coordinate> coordinates) {
        for (Coordinate coordinate : coordinates)
            addGoldMine(coordinate);
    }

    public void addGoldMine(Coordinate coordinate) {
        board.addGoldMineByCoordinate(coordinate);
    }

    public void setTeam(int teamID, Coordinate[][] ancient, ArrayList<Coordinate[][]> barracks) {
        Cell[][] teamAncientCells = board.convertToCells(ancient);
        ArrayList<Cell[][]> teamBarracks = new ArrayList<>();

        for (Coordinate[][] barrack : barracks)
            teamBarracks.add(board.convertToCells(barrack));

        board.createTeam(teamID, teamID, teamAncientCells, teamBarracks);

        for (int i = 0; i < board.getTeams().length; i++)
            teamsData[i] = new ClientTeamData(board.getTeam(i));
    }

    private Path[] convertToPaths(ArrayList<ArrayList<ArrayList<Coordinate>>> pathsCoordinates) {
        Path[] paths = new Path[pathsCoordinates.size()];

        for (int i = 0; i < paths.length; i++)
            paths[i] = convertToPath(pathsCoordinates.get(i));

        return paths;
    }

    private Path convertToPath(ArrayList<ArrayList<Coordinate>> pathCoordinates) {
        Lane[] pathLanes = new Lane[pathCoordinates.size()];

        for (int i = 0; i < pathCoordinates.size(); i++)
            pathLanes[i] = convertToLane(pathCoordinates.get(i));

        return new Path(pathLanes);
    }

    private Lane convertToLane(ArrayList<Coordinate> laneCoordinates) {
        Cell[] laneCells = new Cell[laneCoordinates.size()];

        for (int i = 0; i < laneCells.length; i++)
            laneCells[i] = board.getCell(laneCoordinates.get(i));

        return new Lane(laneCells);
    }

    public int getBoardColumns() {
        return board.getColumns();
    }

    public int getBoardRows() {
        return board.getRows();
    }

    public GameObjectID getGoldMineID(int goldMineIndex) {
        return convertToGameObjectID(board.getGoldMine(goldMineIndex));
    }

    public GameObjectID[] getGoldMineIDs() {
        return convertToGameObjectID(board.getGoldMines());
    }

    public GameObjectID getTeamAncientID(int teamIndex) {
        return convertToGameObjectID(board.getTeamAncient(teamIndex));
    }

    public GameObjectID[] getTeamBarracksID(int teamIndex) {
        return convertToGameObjectID(board.getTeamBarracks(teamIndex));
    }

    public GameObjectID[] getPathLanesIDs(int pathIndex) {
        return convertToGameObjectID(board.getPathsLanes(pathIndex));
    }

    public GameObjectID getPathID(int pathIndex) {
        return convertToGameObjectID(board.getPath(pathIndex));
    }

    public GameObjectID[] getPathIDs() {
        return convertToGameObjectID(board.getPaths());
    }

    public GameObjectID getTeamHeroID(int teamID) {
        return convertToGameObjectID(board.getTeam(teamID).getHero());
    }

    private GameObjectID convertToGameObjectID(GameObject gameObject) {
        return gameObject.getID();
    }

    private GameObjectID[] convertToGameObjectID(GameObject[] gameObjects) {
        GameObjectID[] gameObjectIDs = new GameObjectID[gameObjects.length];

        for (int i = 0; i < gameObjects.length; i++)
            gameObjectIDs[i] = gameObjects[i].getID();

        return gameObjectIDs;
    }

    public int getMoney(int teamIndex) {
        return board.getTeam(teamIndex).getMoney();
    }

    public Team getClientTeam() {
        return clientTeam;
    }

    public void setClientTeam(Team clientTeam) {
        this.clientTeam = clientTeam;
    }

    public void buildFromMapFileLoader(MapFileLoader map) {
        setMapDimensions(map.getRows(), map.getColumns());
        setPaths(map.getPaths());
        addTeam(General.SENTINEL, map);
        addTeam(General.SCOURGE, map);
        addGoldMines(map.getGoldMines());
    }

    private void addTeam(int teamID, MapFileLoader map) {
        setTeam(teamID, map.getAncient(teamID), map.getBarrack(teamID));
    }

    public ClientTeamData getTeamsData(int teamID) {
        return teamsData[teamID];
    }

    public void setPathGameObjectIDs(GameObjectID[] pathGameObjectIDs) {
        this.pathGameObjectIDs = pathGameObjectIDs;
    }

    public void setGoldMineGameObjectIDs(GameObjectID[] goldMineGameObjectIDs) {
        this.goldMineGameObjectIDs = goldMineGameObjectIDs;
    }

    public Board getBoard() {
        return board;
    }
}
