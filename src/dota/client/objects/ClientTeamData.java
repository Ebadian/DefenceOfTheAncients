package dota.client.objects;

import dota.common.GameObjectID;
import dota.game.board.Team;
import dota.game.objects.alive.forces.AttackerUnit;
import dota.game.objects.alive.forces.DefenceTower;
import dota.game.objects.alive.forces.Hero;

import java.util.Map;

public class ClientTeamData {
    private Team team;
    private GameObjectID[] barracks;
    private GameObjectID ancient;
    private Map<GameObjectID, AttackerUnit> attackerUnits;
    private Map<GameObjectID, DefenceTower> defenceTowerMap;
    private Map<GameObjectID, Hero> heros;

    public ClientTeamData(Team team) {
        this.team = team;
    }

    public GameObjectID[] getBarracks() {
        return barracks;
    }

    public void setBarracks(GameObjectID[] barracks) {
        this.barracks = barracks;
    }

    public GameObjectID getAncient() {
        return ancient;
    }

    public void setAncient(GameObjectID ancient) {
        this.ancient = ancient;
    }

    public Team getTeam() {
        return team;
    }
}
