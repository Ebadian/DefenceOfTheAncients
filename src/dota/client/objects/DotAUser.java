package dota.client.objects;

import java.io.Serializable;

public class DotAUser implements Serializable {
    private static int playerIDCounter = 0;
    private int playerID;
    private int teamID;
    private String playerName;

    public DotAUser(int teamID, String playerName) {
        this.teamID = teamID;
        this.playerName = playerName;
        this.playerID = playerIDCounter++;
    }

    public int getPlayerID() {
        return playerID;
    }

    public int getTeamID() {
        return teamID;
    }

    public String getPlayerName() {
        return playerName;
    }
}
