package dota.client;

import dota.UI.menu.Controller;
import dota.UI.utililty.Commander;
import dota.UI.utililty.Responder;
import dota.client.objects.ClientTeamData;
import dota.client.objects.DotAUser;
import dota.common.GameObjectID;
import dota.game.objects.map.Lane;
import dota.game.objects.map.Path;
import dota.game.settings.General;
import dota.server.DotAServer;
import dota.server.NetworkData;
import dota.server.NetworkMessages;
import dota.utils.Coordinate;
import dota.utils.MapFileLoader;
import dota.utils.types.NetworkInfoTypes;
import dota.utils.types.TeamTypes;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DotAClient {
    private ObjectInputStream in;
    private ObjectOutputStream out;
    private String serverAddress;
    private Controller controller;

    private DotAUser user;

    private ClientGameManager clientGameManager;

    public DotAClient(String serverAddress, String playerName, int teamID, Controller controller) {
        this.serverAddress = serverAddress;
        this.clientGameManager = new ClientGameManager();
        this.controller = controller;

        this.user = new DotAUser(teamID, playerName);
        Commander.setDotAUser(user);
        Responder.setClient(DotAClient.this);
        Responder.setTeam(TeamTypes.getTeamType(user.getTeamID()));
    }

    public ClientGameManager getClientGameManager() {
        return clientGameManager;
    }

    public String getPlayerName() {
        return user.getPlayerName();
    }

    public int getPlayerTeamID() {
        return user.getTeamID();
    }

    public Controller getController() {
        return controller;
    }

    public int getTeamID() {
        return user.getTeamID();
    }

    public void run() throws IOException {
        Socket socket = new Socket(serverAddress, DotAServer.PORT);
        out = new ObjectOutputStream(socket.getOutputStream());
        tellServer(getPlayerName());
        tellServer(getPlayerTeamID());
        out.flush();
        in = new ObjectInputStream(socket.getInputStream());

        ServerListener serverListener = new ServerListener();
        serverListener.start();
    }

    public synchronized void tellServer(Object message) {
        try {
            out.writeObject(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void initMap() {
        List<Coordinate> allPathCells = new ArrayList<>();

        for (Path path : clientGameManager.getBoard().getPaths())
            for (Lane lane : path.getLanes())
                allPathCells.addAll(lane.getAllLaneCoordinates());

        Commander.setPathCoordinates(allPathCells);
    }

    class ServerListener extends Thread {
        public ServerListener() {
            this.setDaemon(true);
        }

        @Override
        public void run() {
            try {
                NetworkData data;
                while ((data = (NetworkData) in.readObject()) != null) {
//                    System.out.println("NetworkData " + data);

                    handleEndGame(data);
                    handlePlayerListRequest(data);
                    handleMapInitRequest(data);
                    handleUpdatedDataInMap(data);
                }
            } catch (ClassNotFoundException cn) {
                System.err.println("Class not found");
                cn.printStackTrace();
            } catch (IOException e) {
                System.err.println("Error while receiving data from server");
                e.printStackTrace();
            }
        }

        private void handleEndGame(NetworkData data) {
            if (data.contains(NetworkMessages.END_GAME)) {
//                System.out.println("WON THE GAME");
                TeamTypes winner = (TeamTypes) data.get(NetworkMessages.END_GAME);
                Responder.setWinner(winner);
            }
        }

        private void handleUpdatedDataInMap(NetworkData data) {
            if (data.contains(NetworkMessages.ALIVE_OBJECT_DATA)) {
                Responder.setMoney((Integer) data.get(NetworkMessages.TEAM_MONEY + getTeamID()));

                List<Map<String, Object>> aliveObjectDataList = (List<Map<String, Object>>)
                        data.get(NetworkMessages.ALIVE_OBJECT_DATA);

                clientGameManager.clearAliveObjectData();

                for (Map<String, Object> aliveObjectData : aliveObjectDataList)
                    clientGameManager.addAliveObjectData((GameObjectID) aliveObjectData.get(NetworkInfoTypes.GAME_ID),
                            aliveObjectData);

                clientGameManager.updateGameObjectsInCells();

                Commander.updateBoardUI(clientGameManager);
            }
        }

        private void handlePlayerListRequest(NetworkData data) {
            if (data.contains(NetworkMessages.PLAYER_LIST)) {
                List<DotAUser> users = (List<DotAUser>) data.get(NetworkMessages.PLAYER_LIST);
                for (TeamTypes teamType : TeamTypes.values()) {
                    List<String> names = new ArrayList<>();
                    for (DotAUser user : users)
                        if (user.getTeamID() == teamType.getNum())
                            names.add(user.getPlayerName());
                    controller.resetPlayers(names, teamType);
                }
            }
        }

        private void handleMapInitRequest(NetworkData data) {
            if (data.contains(NetworkMessages.MAP_PATH)) {
                String mapPath = (String) data.get(NetworkMessages.MAP_PATH);
                try {
                    MapFileLoader map = MapFileLoader.newMapLoader(mapPath);
                    clientGameManager.buildFromMapFileLoader(map);

                    clientGameManager.setGoldMineGameObjectIDs((GameObjectID[]) data.get(NetworkMessages.GOLD_MINES));
                    clientGameManager.setPathGameObjectIDs((GameObjectID[]) data.get(NetworkMessages.PATHS));
                    clientGameManager.setLanesIDs((ArrayList<GameObjectID[]>) data.get(NetworkMessages.PATH_LANE_IDS));
                    clientGameManager.setMapToLane();

                    setInitialTeamData(data, General.SENTINEL);
                    setInitialTeamData(data, General.SCOURGE);
                } catch (FileNotFoundException e) {
                    System.err.println("Error opening file " + e.getMessage());
                }

                initMap();
            }
        }

        private void setInitialTeamData(NetworkData data, int teamID) {
            ClientTeamData teamData = clientGameManager.getTeamsData(teamID);
            teamData.setAncient((GameObjectID) data.get(NetworkMessages.ANCIENT + teamID));
            teamData.setBarracks((GameObjectID[]) data.get(NetworkMessages.BARRACKS + teamID));
//            System.out.println(teamData.getAncient());
        }
    }
}
