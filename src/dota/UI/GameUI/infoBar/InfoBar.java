package dota.UI.GameUI.infoBar;

import dota.UI.utililty.Responder;
import dota.common.GameObjectID;
import dota.utils.types.GameObjectTypes;
import dota.utils.types.TeamTypes;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.util.Vector;

public class InfoBar extends VBox {
    private static final GameObjectTypes[] types
            = new GameObjectTypes[]{GameObjectTypes.HERO, GameObjectTypes.DEFENCE_TOWER, GameObjectTypes.ATTACKER_UNIT};
    private static TeamTypes[] teamTypes = TeamTypes.values();
    private final int SIZE_TEAMS = 2, SIZE_ITEMS = 3, WIDTH = 160, HEIGH = 80;
    private ImageView[][] imageViews = new ImageView[SIZE_TEAMS][SIZE_ITEMS];
    private TextArea[][] info = new TextArea[SIZE_TEAMS][SIZE_ITEMS];
    private Label teamName = new Label("No Team"), money = new Label("0");

    public InfoBar() {
        Responder.setInfoBar(this);
        initImageViews();
        initInfo();
        initLabels();

        setPadding(new Insets(5, 10, 5, 10));

        getChildren().add(new HBox(teamName, money));
        for (int i = 0; i < SIZE_TEAMS; i++) {
            getChildren().add(new ImageView(new Image(teamTypes[i].getUrl(), WIDTH * 3 / 2, HEIGH, false, true, true)));
            for (int j = 0; j < SIZE_ITEMS; j++) {
                getChildren().add(new HBox(imageViews[i][j], info[i][j]));
            }
        }
    }

    private void initLabels() {
        teamName.setStyle("-fx-text-fill: red; -fx-background-color: darkblue; " +
                "-fx-highlight-text-fill: brown; -fx-text-alignment: center;" +
                " -fx-font-size: 30;");
        money.setStyle("-fx-text-fill: gold; -fx-background-color: gray;" +
                "-fx-text-alignment: center;" +
                "-fx-font-size: 30;");
    }

    private void initInfo() {
        for (int i = 0; i < SIZE_TEAMS; i++)
            for (int j = 0; j < SIZE_ITEMS; j++) {
                info[i][j] = new TextArea();
                info[i][j].setEditable(false);

                info[i][j].setMaxWidth(WIDTH);
                info[i][j].setMaxHeight(HEIGH);
            }
    }

    private void initImageViews() {
        for (int i = 0; i < SIZE_TEAMS; i++)
            for (int j = 0; j < SIZE_ITEMS; j++)
                imageViews[i][j] = new ImageView(new Image(types[j].getUrl(), WIDTH / 2, HEIGH, false, true, true));
    }

    public void update(Vector<GameObjectID> gameObjectIDs) {
        for (int i = 0; i < SIZE_TEAMS; i++) {
            for (int j = 0; j < SIZE_TEAMS; j++) {
                info[i][j].setText("");
            }
        }
        for (GameObjectID gameObjectID : gameObjectIDs) {
            TeamTypes team = Responder.getGameObjectTeam(gameObjectID);
            GameObjectTypes type = Responder.getGameObjectType(gameObjectID);
            Integer hp = Responder.getAliveObjectHP(gameObjectID);

            if (hp == null)
                continue;

            info[team.getNum()][getNum(type)].appendText("HP = " + hp + "\n");
        }
    }

    private Integer getNum(GameObjectTypes type) {
        for (int i = 0; i < types.length; i++)
            if (type == types[i])
                return i;
        return null;
    }

    public void setTeam(TeamTypes team) {
        teamName.setText(team.getName());
    }

    public synchronized void setMoney(int money) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                InfoBar.this.money.setText(money + "");
            }
        });
    }
}
