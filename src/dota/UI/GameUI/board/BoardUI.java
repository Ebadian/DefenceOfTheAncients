package dota.UI.GameUI.board;

import dota.UI.GameUI.utility.CellUI;
import dota.UI.GameUI.utility.DefaultCell;
import dota.UI.GameUI.utility.PathCell;
import dota.UI.utililty.Commander;
import dota.utils.Coordinate;
import javafx.geometry.Pos;
import javafx.scene.layout.GridPane;

import java.util.Collection;

public class BoardUI extends GridPane {
    private final int LEN;
    private CellUI[][] cellUIs;
    private boolean isPathCell[][];

    public BoardUI(int LEN) {
        this.LEN = LEN;
        fillBoardWithCells();

        setGridLinesVisible(true);
        setAlignment(Pos.CENTER);
    }

    private void fillBoardWithCells() {
        isPathCell = new boolean[LEN][LEN];
        cellUIs = new CellUI[LEN][LEN];
        setIsPathCell();

        for (int i = 0; i < LEN; i++) {
            for (int j = 0; j < LEN; j++) {
                if (isPathCell[i][j])
                    cellUIs[i][j] = new PathCell(i, j);
                else
                    cellUIs[i][j] = new DefaultCell(i, j);
                add(cellUIs[i][j], j, i);
            }
        }
    }

    private void setIsPathCell() {
        Collection<Coordinate> pathCoordinates = Commander.getPathCoordinates();
        for (Coordinate coordinate : pathCoordinates)
            isPathCell[coordinate.getX()][coordinate.getY()] = true;
    }

    public CellUI getCell(int x, int y) {
        return cellUIs[x][y];
    }
}
