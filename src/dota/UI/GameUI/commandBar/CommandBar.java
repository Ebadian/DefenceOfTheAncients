package dota.UI.GameUI.commandBar;

import dota.UI.utililty.Responder;
import dota.utils.types.*;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Font;

public class CommandBar extends VBox {
    private static final GameObjectTypes[] GAME_OBJECT_TYPES
            = new GameObjectTypes[]{GameObjectTypes.ATTACKER_UNIT, GameObjectTypes.DEFENCE_TOWER};
    private static final AttackerUnitTypes[] ATTACKER_UNIT_TYPES
            = new AttackerUnitTypes[]{AttackerUnitTypes.INFANTRY, AttackerUnitTypes.CAVALRY};
    private static final AttackerUnitUpgradeTypes[] ATTACKER_UNIT_UPGRADE_TYPES
            = new AttackerUnitUpgradeTypes[]{AttackerUnitUpgradeTypes.DAMAGE_UPGRADE, AttackerUnitUpgradeTypes.HEALTH_UPGRADE};
    private static final DefenceTowerUpgradeTypes[] DEFENCE_TOWER_UPGRADE_TYPES
            = new DefenceTowerUpgradeTypes[]{DefenceTowerUpgradeTypes.DAMAGE_UPGRADE, DefenceTowerUpgradeTypes.RANGE_UPGRADE};
    private final int SIZE_ITEMS = 2, SIZE_CHOICES = 2, WIDTH = 170;
    private DefenceTowerTypes[] DEFENCE_TOWER_TYPES;
    private ImageView[] imageViews = new ImageView[SIZE_ITEMS];
    private Button[][] createButtons = new Button[SIZE_ITEMS][SIZE_CHOICES],
            upgradeButtons = new Button[SIZE_ITEMS][SIZE_CHOICES];
    private Button pause = new Button("Pause"), resume = new Button("Resume");
    private TeamTypes team;

    public CommandBar() {
        Responder.setCommandBar(this);

        initImageViews();
        initUpgradeButtons();
        initCreateButtons();
        setButtonsStyles();

        setButtonAvailability(false, false);

        addControlButtons ();
        for (int i = 0; i < SIZE_ITEMS; i++) {
            getChildren().add(imageViews[i]);
            getChildren().add(new HBox(createButtons[i][0], createButtons[i][1]));
            getChildren().add(new HBox(upgradeButtons[i][0], upgradeButtons[i][1]));
        }
    }

    private void addControlButtons() {
        pause.setOnAction(event -> Responder.pauseGame ());
        resume.setOnAction(event -> Responder.resumeGame ());
        resume.setDisable(true);

        getChildren().add(new HBox(pause, resume));
    }

    private void initCreateButtons() {
        for (int i = 0; i < SIZE_ITEMS; i++)
            for (int j = 0; j < SIZE_CHOICES; j++) {
                createButtons[i][j] = new Button();
                Image image;
                if (i == 0) {
                    image = new Image(ATTACKER_UNIT_TYPES[j].getUrl(), WIDTH / 2.0, WIDTH / 2.0, false, true, true);
//                    createButtons[i][j].setText(ATTACKER_UNIT_TYPES[j].getName ());
                }
                else {
                    image = null;
                }
                createButtons[i][j].setFont(Font.font(30));
                createButtons[i][j].setGraphic(new ImageView(image));
                createButtons[i][j].setPadding(Insets.EMPTY);

                createButtons[i][j].setOnAction(event -> handleCreateButton(event));
            }
    }

    private void handleCreateButton(ActionEvent event) {
        Object source = event.getSource();

        for (int i = 0; i < SIZE_ITEMS; i++)
            for (int j = 0; j < SIZE_CHOICES; j++)
                if (source == createButtons[i][j]) {
                    GameObjectTypes type = GAME_OBJECT_TYPES[i];
                    if (type == GameObjectTypes.ATTACKER_UNIT)
                        Responder.createGameObject(ATTACKER_UNIT_TYPES[j]);
                    else {
                        Responder.createGameObject(DEFENCE_TOWER_TYPES[j]);
                        setButtonAvailability(true, true);
                    }
                    return;
                }
    }


    private void initUpgradeButtons() {
        for (int i = 0; i < SIZE_ITEMS; i++)
            for (int j = 0; j < SIZE_CHOICES; j++) {
                upgradeButtons[i][j] = new Button();
                Image image;
                if (i == 0) {
                    image = new Image(ATTACKER_UNIT_UPGRADE_TYPES[j].getUrl(), WIDTH / 2.0, WIDTH / 2.0, false, true, true);
//                    upgradeButtons[i][j].setText(ATTACKER_UNIT_UPGRADE_TYPES[j].getName());
                }
                else {
                    image = new Image(DEFENCE_TOWER_UPGRADE_TYPES[j].getUrl(), WIDTH / 2.0, WIDTH / 2.0, false, true, true);
//                    upgradeButtons[i][j].setText(DEFENCE_TOWER_UPGRADE_TYPES[j].getName());
                }
                upgradeButtons[i][j].setFont(Font.font(30));
//                upgradeButtons[i][j].setBackground(new BackgroundImage (image, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, new BackgroundSize(WIDTH / 2, WIDTH / 2, false, false, true, true)));
                upgradeButtons[i][j].setGraphic(new ImageView(image));
                upgradeButtons[i][j].setPadding(Insets.EMPTY);

                upgradeButtons[i][j].setOnAction(event -> handleUpgradeButton(event));
            }
    }

    private void handleUpgradeButton(ActionEvent event) {
        Object source = event.getSource();

        for (int i = 0; i < SIZE_ITEMS; i++)
            for (int j = 0; j < SIZE_CHOICES; j++)
                if (source == upgradeButtons[i][j]) {
                    GameObjectTypes type = GAME_OBJECT_TYPES[i];
                    if (type == GameObjectTypes.ATTACKER_UNIT)
                        Responder.upgradeGameObject(ATTACKER_UNIT_UPGRADE_TYPES[j]);
                    else
                        Responder.upgradeGameObject(DEFENCE_TOWER_UPGRADE_TYPES[j]);
                    return;
                }
    }

    private void initImageViews() {
        for (int i = 0; i < SIZE_ITEMS; i++) {
            Image image = new Image(GAME_OBJECT_TYPES[i].getUrl(), WIDTH, WIDTH, false, true, true);
            imageViews[i] = new ImageView(image);
        }
    }

    public void setButtonAvailability(boolean isInHalfPath, boolean hasTower) {
        if (!isInHalfPath)
            for (int i = 0; i < SIZE_ITEMS; i++)
                for (int j = 0; j < SIZE_CHOICES; j++) {
                    createButtons[i][j].setDisable(true);

                    upgradeButtons[i][j].setDisable(true);
                }
        else
            for (int i = 0; i < SIZE_CHOICES; i++) {
                createButtons[0][i].setDisable(false);
                createButtons[1][i].setDisable(hasTower);

                upgradeButtons[0][i].setDisable(false);
                upgradeButtons[1][i].setDisable(!hasTower);
            }
        upgradeButtons[0][0].setDisable(false);
        upgradeButtons[0][1].setDisable(false);
    }

    private void setButtonsStyles() {
        String style = "    -fx-background-color: \n" +
                "        #ecebe9,\n" +
                "        rgba(0,0,0,0.05),\n" +
                "        linear-gradient(#dcca8a, #c7a740),\n" +
                "        linear-gradient(#f9f2d6 0%, #f4e5bc 20%, #e6c75d 80%, #e2c045 100%),\n" +
                "        linear-gradient(#f6ebbe, #e6c34d);\n" +
                "    -fx-background-insets: 0, 1 1 1,1,1,1;\n" +
                "    -fx-background-radius: 50;\n" +
                "    -fx-padding: 5 10 5 10;\n" +
                "    -fx-font-family: \"Helvetica\";\n" +
                "    -fx-font-size: 18px;\n" +
                "    -fx-text-fill: #311c09;\n" +
//                "    -fx-effect: innershadow( three-pass-box , rgba(0,0,0,0.1) , 2, 0.0 , 0 , 1);" +
                "-fx-animated: true;";
        for (int i = 0; i < SIZE_ITEMS; i++) {
            for (int j = 0; j < SIZE_CHOICES; j++) {
                createButtons[i][j].setStyle(style);
                upgradeButtons[i][j].setStyle(style);
            }
        }
        pause.setStyle(style);
        resume.setStyle(style);
    }

    public void setTeam(TeamTypes team) {
        this.team = team;
        if (team == TeamTypes.SENTINEL)
            DEFENCE_TOWER_TYPES = new DefenceTowerTypes[]{DefenceTowerTypes.FIRE_TOWER, DefenceTowerTypes.STONE_TOWER};
        else
            DEFENCE_TOWER_TYPES = new DefenceTowerTypes[]{DefenceTowerTypes.BLACK_TOWER, DefenceTowerTypes.POISON_TOWER};
        for (int j = 0; j < SIZE_CHOICES; j++) {
            int i = 1;

            Image image = new Image(DEFENCE_TOWER_TYPES[j].getUrl(), WIDTH / 2, WIDTH / 2, false, true, true);
            createButtons[i][j].setGraphic(new ImageView(image));
//            createButtons[i][j].setText(DEFENCE_TOWER_TYPES[j].getName());
        }
    }

    public void resumeGame() {
        resume.setDisable(true);
        pause.setDisable(false);
    }

    public void pauseGame() {
        pause.setDisable(true);
        resume.setDisable(false);
    }
}
