package dota.UI.GameUI.utility;

import dota.UI.settings.Images;
import dota.UI.utililty.Responder;
import dota.common.GameObjectID;
import dota.utils.Coordinate;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;

import java.util.Vector;

abstract public class CellUI extends Button {
    protected static final int LEN = 18;
    private final int X, Y, SIZE_X, SIZE_Y;
    protected ImageView[][] imageViews;
    private boolean inVision = true;
    private boolean hasEnemy = false, hasFriend = false;

    public CellUI(int x, int y, int SIZE_X, int SIZE_Y) {
        X = x;
        Y = y;
        this.SIZE_X = SIZE_X;
        this.SIZE_Y = SIZE_Y;
        initLayout();
        setOnMouseClicked(event -> handleButton(event));
    }

    private void handleButton(MouseEvent event) {
        Coordinate coordinate = new Coordinate(X, Y);
        if (event.getButton() == MouseButton.PRIMARY)
            Responder.setCurrentCoordinate(coordinate);
        else
            Responder.setHeroTarget(coordinate);
    }

    public boolean isInVision() {
        return inVision;
    }

    public void setInVision(boolean inVision) {
        if (this.inVision == inVision)
            return;
        this.inVision = inVision;
//        if (inVision)
//            this.setOpacity(1.0);
//        else
//            this.setOpacity(0.2);
    }

    private void initLayout() {
        imageViews = new ImageView[SIZE_X][SIZE_Y];
        makeImageViewsDefault();
        GridPane gridPane = new GridPane();
        for (int i = 0; i < SIZE_X; i++) {
            for (int j = 0; j < SIZE_Y; j++) {
                gridPane.add(imageViews[i][j], i, j);
            }
        }

        setGraphic(gridPane);
        gridPane.setGridLinesVisible(true);
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setPadding(Insets.EMPTY);
        setPadding(Insets.EMPTY);
    }

    protected void makeImageViewsDefault() {
        for (int i = 0; i < SIZE_X; i++)
            for (int j = 0; j < SIZE_Y; j++) {
                if (imageViews[i][j] == null)
                    imageViews[i][j] = new ImageView();
                if (SIZE_X == 3)
                    imageViews[i][j].setImage(Images.DEFAULT_SMALL);
                else if (SIZE_X == 2)
                    imageViews[i][j].setImage(Images.DEFAULT_LARGE);
            }
    }

    abstract public void updateView(Vector<GameObjectID> gameObjectIDs);
}
