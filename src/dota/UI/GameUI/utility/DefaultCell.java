package dota.UI.GameUI.utility;

import dota.UI.utililty.Responder;
import dota.common.GameObjectID;
import dota.game.objects.GameObject;
import dota.utils.types.GameObjectTypes;
import dota.utils.types.TeamTypes;
import javafx.application.Platform;
import javafx.scene.image.Image;

import java.util.Vector;

public class DefaultCell extends CellUI {
    private final static int SIZE_X = 2, SIZE_Y = 2;

    public DefaultCell(int X, int Y) {
        super(X, Y, SIZE_X, SIZE_Y);
    }


    @Override
    public void updateView(Vector<GameObjectID> gameObjectIDs) throws RuntimeException {
        makeImageViewsDefault();
        boolean[][] marked = new boolean[SIZE_X][SIZE_Y];
        for (GameObjectID gameObjectID : gameObjectIDs) {
            GameObjectTypes gameObjectType = Responder.getGameObjectType(gameObjectID);
            int r = gameObjectType.getType();
            Image image = gameObjectType.getLargeImage();

            if (gameObjectType.getType() == 1) {
                for (int c = 0; c < SIZE_Y; c++)
                    if (!marked[r][c]) {
                        marked[r][c] = true;
                        imageViews[r][c].setImage(image);
                    }
            }
            else {
                if (gameObjectType != GameObjectTypes.HERO)
                    throw new RuntimeException("An AttackerUnit or DefenceTower wants to be shown in a DefaultCell");
                TeamTypes team = Responder.getGameObjectTeam(gameObjectID);

                if (team == null)
                    continue;

                int c = team.getNum();

                if (!marked[r][c]) {
                    marked[r][c] = true;
                    imageViews[r][c].setImage(image);
                }
            }
        }
    }
}
