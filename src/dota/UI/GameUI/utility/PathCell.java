package dota.UI.GameUI.utility;

import dota.UI.settings.Images;
import dota.UI.utililty.Responder;
import dota.common.GameObjectID;
import dota.utils.types.GameObjectTypes;
import dota.utils.types.TeamTypes;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.Vector;

public class PathCell extends CellUI {
    private final static int SIZE_X = 3, SIZE_Y = 3;

    public PathCell(int X, int Y) {
        super(X, Y, SIZE_X, SIZE_Y);
    }

    @Override
    public void updateView(Vector<GameObjectID> gameObjectIDs) {
        makeImageViewsDefault();
        boolean[][] marked = new boolean[SIZE_X][SIZE_Y];
        for (GameObjectID gameObjectID : gameObjectIDs) {
            TeamTypes team = Responder.getGameObjectTeam(gameObjectID);

            if (team == null)
                continue;

            GameObjectTypes gameObjectType = Responder.getGameObjectType(gameObjectID);

            int c = 2 * team.getNum();
            int r = -1;
            int rLen = 0;
            switch (gameObjectType) {
                case HERO:
                    r = 0;
                    rLen = 1;
                    break;
                case DEFENCE_TOWER:
                    r = 1;
                    rLen = 1;
                    break;
                case ATTACKER_UNIT:
                    r = 2;
                    rLen = 1;
                    break;
                default:
                    r = 0;
                    rLen = 3;
                    break;
            }
            Image image = gameObjectType.getSmallImage();

            for (int i = 0; i < rLen; i++)
                if (!marked[r + i][c]) {
                    marked[r + i][c] = true;
                    imageViews[r + i][c].setImage(image);
                }
        }
    }

    @Override
    protected void makeImageViewsDefault() {
        for (int i = 0; i < SIZE_X; i++)
            for (int j = 0; j < SIZE_Y; j++) {
                if (imageViews[i][j] == null)
                    imageViews[i][j] = new ImageView();
                if (SIZE_X == 3)
                    imageViews[i][j].setImage(Images.PATH_SMALL);
                else if (SIZE_X == 2)
                    imageViews[i][j].setImage(Images.PATH_LARGE);
            }
    }
}

