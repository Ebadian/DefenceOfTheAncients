package dota.UI.GameUI;

import dota.UI.GameUI.board.BoardUI;
import dota.UI.GameUI.commandBar.CommandBar;
import dota.UI.GameUI.infoBar.InfoBar;
import dota.UI.utililty.Responder;
import dota.utils.types.TeamTypes;
import javafx.scene.Node;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

public class GameUI extends BorderPane {
    private final int LEN;
    private TeamTypes team;
    private BoardUI board;
    private InfoBar infoBar;
    private CommandBar commandBar;

    public GameUI(int LEN) {
        Responder.setGameUI (this);
        this.LEN = LEN;

        setCenter(createBoard());
        setLeft(createCommandBar());
        setRight(createInfoBar());
        setTeam(Responder.getTeam());
    }

    private Node createInfoBar() {
        infoBar = new InfoBar();

        return infoBar;
    }

    public TeamTypes getTeam() {
        if (team == null) {
            System.err.println("Team should not be null");
            return TeamTypes.SENTINEL;
        }
        return team;
    }

    public void setTeam(TeamTypes team) {
        this.team = team;
        infoBar.setTeam(team);
        commandBar.setTeam (team);
    }

    private VBox createCommandBar() {
        commandBar = new CommandBar();
        return commandBar;
    }

    private synchronized GridPane createBoard() {
        board = new BoardUI(LEN);
        return board;
    }

    public synchronized BoardUI getBoard() {
        return board;
    }
}
