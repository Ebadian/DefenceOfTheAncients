package dota.UI.utililty;

import dota.UI.GameUI.GameUI;
import dota.UI.GameUI.commandBar.CommandBar;
import dota.UI.GameUI.infoBar.InfoBar;
import dota.client.DotAClient;
import dota.client.NetworkCommandMessages;
import dota.common.GameObjectID;
import dota.game.objects.alive.forces.DefenceTower;
import dota.server.NetworkData;
import dota.utils.Coordinate;
import dota.utils.types.*;
import javafx.application.Platform;
import javafx.scene.control.Alert;

import java.util.Vector;

public class Responder {
    private static Coordinate currentCoordinate = null;
    private static CommandBar commandBar = null;

    private static InfoBar infoBar = null;
    private static TeamTypes team;
    private static DotAClient client;
    private static GameUI gameUI;

    public static TeamTypes getGameObjectTeam(GameObjectID gameObjectID) {
        return TeamTypes.getTeamType((Integer) client.getClientGameManager().
                getAliveObjectData(gameObjectID, NetworkInfoTypes.TEAM_ID));
    }

    public static void setClient(DotAClient client) {
        Responder.client = client;
    }

    public static GameObjectTypes getGameObjectType(GameObjectID gameObjectID) {
        Class className = gameObjectID.getType();

        for (GameObjectTypes gameObjectType : GameObjectTypes.values())
            if (className == gameObjectType.getClassName())
                return gameObjectType;
        return null;
    }

    public static void createGameObject(AttackerUnitTypes attackerUnitTypes) {
//        System.out.println("UI: Create AttackerUnit: " + attackerUnitTypes.getName());
        if (currentCoordinate == null)
            return;

        NetworkData data = new NetworkData();

        data.put(NetworkCommandMessages.COMMAND_TYPE, NetworkCommandMessages.CREATE_ATTACKER_UNIT);
        data.put(NetworkCommandMessages.COMMAND_LANE_POSITION,
                client.getClientGameManager().getLaneAccordingTo(currentCoordinate));
        data.put(NetworkCommandMessages.ATTACKER_UNIT_TYPE, attackerUnitTypes);

        tellServerOnThread(data);
    }

    private static void tellServerOnThread(final NetworkData data) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                client.tellServer(data);
            }
        }).start();
    }

    public static void createGameObject(DefenceTowerTypes defenceTowerType) {
//        System.out.println("UI: Create DefenceTower: " + defenceTowerType.getName());
        if (currentCoordinate == null)
            return;

        NetworkData data = new NetworkData();

        data.put(NetworkCommandMessages.COMMAND_TYPE, NetworkCommandMessages.CREATE_DEFENCE_TOWER);
        data.put(NetworkCommandMessages.ABSOLUTE_POSITION, getCurrentCoordinate());
        data.put(NetworkCommandMessages.DEFENCE_TOWER_TYPE, defenceTowerType);

        tellServerOnThread(data);
    }

    public static void upgradeGameObject(AttackerUnitUpgradeTypes attackerUnitUpgradeType) {
//        System.out.println("UI: Upgrade AttackerUnit: " + attackerUnitUpgradeType.getName());

        NetworkData data = new NetworkData();

        data.put(NetworkCommandMessages.COMMAND_TYPE, NetworkCommandMessages.ATTACKER_UNIT_UPGRADE_GAME_OBJECT);
    }

    //The specific Tower on the currentCoordinate should be upgraded
    public static void upgradeGameObject(DefenceTowerUpgradeTypes defenceTowerUpgradeType) {
//        System.out.println("UI: Upgrade DefenceTower: " + defenceTowerUpgradeType.getName());

        NetworkData data = new NetworkData();

        data.put(NetworkCommandMessages.COMMAND_TYPE, NetworkCommandMessages.UPGRADE_DEFENCE_TOWER);
    }

    private static boolean hasTower(Coordinate coordinate) {
        for (GameObjectID gameObjectID : client.getClientGameManager().getGameObjectsInCell(coordinate))
            if (DefenceTower.class.isAssignableFrom(gameObjectID.getType()) &&
                    (Integer) client.getClientGameManager().getAliveObjectData(gameObjectID, NetworkInfoTypes.TEAM_ID)
                            == team.getNum())
                return true;

        return false;
    }

    //Should be in the first half of the path to return true
    public synchronized static boolean isInHalfPath(TeamTypes team, Coordinate coordinate) {
        return client.getClientGameManager().getLaneAccordingTo(coordinate) != null &&
                client.getClientGameManager().getMapCellLaneToTeam(coordinate) == team;
    }

    public static Coordinate getCurrentCoordinate() {
        return currentCoordinate;
    }

    //Enable Disable buttons
    public static void setCurrentCoordinate(Coordinate currentCoordinate) {
        Responder.currentCoordinate = currentCoordinate;
        if (commandBar != null)
            commandBar.setButtonAvailability(isInHalfPath(team, currentCoordinate), hasTower(currentCoordinate));
        if (infoBar != null)
            infoBar.update(getGameObjectIDs(currentCoordinate));
    }

    private static Vector<GameObjectID> getGameObjectIDs(Coordinate coordinate) {
//        System.out.println("UI needs GameObjectIDs of " + coordinate);

        return client.getClientGameManager().getGameObjectsInCell(coordinate);
    }

    public static void setHeroTarget(Coordinate coordinate) {
//        System.out.println("UI: set hero's target to: " + coordinate);
        NetworkData data = new NetworkData();

        data.put(NetworkCommandMessages.COMMAND_TYPE, NetworkCommandMessages.SET_HERO_TARGET);
        data.put(NetworkCommandMessages.ABSOLUTE_POSITION, coordinate);

        tellServerOnThread(data);
    }

    public static void setCommandBar(CommandBar commandBar) {
        Responder.commandBar = commandBar;
    }

    public static void setInfoBar(InfoBar infoBar) {
        Responder.infoBar = infoBar;
    }

    public static Integer getAliveObjectHP(GameObjectID gameObjectID) {
//        System.out.println("AI needs GameObjectID's HP");

        return (Integer) client.getClientGameManager().getAliveObjectData(gameObjectID, NetworkInfoTypes.HEALTH);
    }

    public static void setMoney(Integer money) {
        if (infoBar == null)
            return;
        if (money == null)
            money = 0;
        infoBar.setMoney(money);
    }

    public static void setTeam(TeamTypes team) {
        Responder.team = team;
    }

    public static TeamTypes getTeam() {
        return team;
    }

    public static void setGameUI(GameUI gameUI) {
        Responder.gameUI = gameUI;
    }

    public static void setWinner(TeamTypes team) {
        Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Client");
                    alert.setHeaderText("GameOver");
                    alert.setContentText(team.getName() + " wins!!!");

                    alert.showAndWait();

                    Platform.exit();
                    System.exit(0);
                }
            }
        );
    }

    public static void pauseGame() {
        //Fill Network
    }

    public static void resumeGame() {
        //Fill Network
    }

    // Fill Network : call it when game is resumed somewhere
    public static void resumeGameInThisClient () {
        commandBar.resumeGame ();
    }

    // Fille Network : call it when game is paused somewhere
    public static void pauseGameInThisClinet () {
        commandBar.pauseGame ();
    }
}
