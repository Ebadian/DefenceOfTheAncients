package dota.UI.utililty;

import dota.UI.GameUI.board.BoardUI;
import dota.client.ClientGameManager;
import dota.client.objects.DotAUser;
import dota.common.GameObjectID;
import dota.utils.Coordinate;
import javafx.application.Platform;

import java.util.List;
import java.util.Vector;

public class Commander {
    private static BoardUI boardUI;

    private static DotAUser dotAUser;

    public static void setDotAUser(DotAUser dotAUser) {
        Commander.dotAUser = dotAUser;
    }

    private static List<Coordinate> pathCoordinates;

    public static BoardUI getBoardUI() {
        return boardUI;
    }

    public static void setBoardUI(BoardUI boardUI) {
        Commander.boardUI = boardUI;
    }

    public static List<Coordinate> getPathCoordinates() {
        return pathCoordinates;
    }

    public static void setPathCoordinates(List<Coordinate> pathCoordinates) {
        Commander.pathCoordinates = pathCoordinates;
    }

    public static void updateBoardUI(ClientGameManager clientGameManager) {
        if (boardUI == null)
            return;

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < clientGameManager.getBoardRows(); i++)
                    for (int j = 0; j < clientGameManager.getBoardColumns(); j++) {
                        if (clientGameManager.isVisibleByTeam(dotAUser.getTeamID(), i, j)) {
                            boardUI.getCell(i, j).setInVision(true);
                            Vector<GameObjectID> gameObjectIDs = clientGameManager.getGameObjectsInCell(i, j);
                            boardUI.getCell(i, j).updateView(gameObjectIDs);
                        }
                        else {
                            boardUI.getCell(i, j).setInVision(false);
                            boardUI.getCell(i, j).updateView(new Vector<>());
                        }
                    }
            }
        });
    }
}
