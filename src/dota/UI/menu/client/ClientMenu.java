package dota.UI.menu.client;

import dota.utils.types.TeamTypes;
import javafx.application.Platform;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.TextInputDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ClientMenu {
    private TextInputDialog inputName, inputIP;
    private ChoiceDialog<String> chooseTeam;
    private String name, IP;
    private TeamTypes team;

    public ClientMenu() {
        initTextInputDialogs();
        initChoiceDialogs();
    }

    private void initTextInputDialogs() {
        inputName = new TextInputDialog("Player");
        inputName.setTitle("Client");
        inputName.setHeaderText("Please input your name");
        inputName.setContentText("Name:");

        inputIP = new TextInputDialog("127.0.0.1");
        inputIP.setTitle("Client");
        inputIP.setHeaderText("Please input server's IP");
        inputIP.setContentText("IP:");
    }

    private void initChoiceDialogs() {
        List<String> choices = new ArrayList<>();
        for (TeamTypes team : TeamTypes.values())
            choices.add(team.getName());

        chooseTeam = new ChoiceDialog<>(null, choices);
        chooseTeam.setTitle("Client");
        chooseTeam.setHeaderText("Choose which team you want to play in");
        chooseTeam.setContentText("Team:");
    }

    public void showAndWait() {
        Optional<String> result;
        result = inputName.showAndWait();
        result.ifPresent(string -> name = string);
        if (name == null || name == "") {
            Platform.exit();
            System.exit(0);
        }

        result = chooseTeam.showAndWait();
        if (result.isPresent()) {
            for (TeamTypes team : TeamTypes.values())
                if (team.getName().equals(result.get()))
                    this.team = team;
        }
        else {
            Platform.exit();
            System.exit(0);
        }

        result = inputIP.showAndWait();
        result.ifPresent(s -> IP = s);
        if (IP == null || IP == "") {
            Platform.exit();
            System.exit(0);
        }
    }

    public String getIP() {
        return IP;
    }

    public String getName() {
        return name;
    }

    public TeamTypes getTeam() {
        return team;
    }
}
