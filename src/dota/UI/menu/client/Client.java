package dota.UI.menu.client;

import dota.UI.menu.Controller;
import dota.client.DotAClient;
import dota.utils.types.TeamTypes;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Client extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = initStartMenu();
        primaryStage.setTitle("Dota-Client");

        primaryStage.setScene(new Scene(root, 640, 480));
        primaryStage.show();
    }

    private Parent initStartMenu() {
        ClientMenu clientMenu = new ClientMenu();
        clientMenu.showAndWait();

        String name = clientMenu.getName();
        String IP = clientMenu.getIP();
        TeamTypes team = clientMenu.getTeam();

        Controller controller = new Controller(false);

        DotAClient dotAClient = new DotAClient(IP, name, team.getNum(), controller);

        try {
            dotAClient.run();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return controller;
    }

}
