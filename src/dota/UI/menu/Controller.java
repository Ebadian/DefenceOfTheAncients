package dota.UI.menu;

import dota.UI.GameUI.GameUI;
import dota.UI.menu.server.MenuResponder;
import dota.UI.utililty.Commander;
import dota.utils.types.TeamTypes;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.List;

public class Controller extends VBox {
    private Button start, pause, resume;
    private TextArea[] textAreas = new TextArea[2];
    private boolean isServer;
    private GameUI gameUI;

    public Controller(boolean isServer) {
        this.isServer = isServer;
        initButtons();
        initTextAreas();
        setLayout();
    }

    private void setLayout() {
        HBox hBox = new HBox(textAreas);
        getChildren().add(hBox);

        ButtonBar buttonBar = new ButtonBar();
        if (isServer)
            buttonBar.getButtons().addAll(start, pause, resume);
        else
            buttonBar.getButtons().addAll(start);
        getChildren().add(buttonBar);
    }

    private void initTextAreas() {
        for (TeamTypes team : TeamTypes.values()) {
            textAreas[team.getNum()] = new TextArea(team.getName() + " players:\n");
            textAreas[team.getNum()].setEditable(false);
        }
    }

    private void initButtons() {
        start = new Button("Start");
        start.setOnAction(event -> startGame());

        pause = new Button("Pause");
        pause.setOnAction(event -> pauseGame());
        pause.setDisable(true);

        resume = new Button("Resume");
        resume.setOnAction(event -> resumeGame());
        resume.setDisable(true);
    }

    private void resumeGame() {
        resume.setDisable(true);
        pause.setDisable(false);

        MenuResponder.resumeGame();
    }

    private void pauseGame() {
        pause.setDisable(true);
        resume.setDisable(false);

        MenuResponder.pauseGame();
    }

    private void startGame() {
        start.setDisable(true);
        pause.setDisable(false);
        
        if (!isServer) {
            Stage stage = (Stage) getScene().getWindow();

            gameUI = new GameUI(41);
            stage.setScene(new Scene(gameUI, 1200, 800));
            stage.setOnCloseRequest(event -> {
                Platform.exit();
                System.exit(0);
            });

            Commander.setBoardUI(gameUI.getBoard());
        } else {
            MenuResponder.startGame();
        }
    }

    public void resetPlayers(List<String> names, TeamTypes teamType) {
        String allNames = teamType.getName() + " players:\n";

        for (String name : names)
            allNames += name + "\n";

        textAreas[teamType.getNum()].setText(allNames);
    }
}
