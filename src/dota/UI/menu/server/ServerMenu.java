package dota.UI.menu.server;

import dota.Settings;
import javafx.application.Platform;
import javafx.scene.control.ChoiceDialog;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public class ServerMenu {
    private ChoiceDialog<String> choiceDialog;
    private String mapName = null;

    public ServerMenu() {
        initChoiceDialogs();
    }

    private void initChoiceDialogs() {
        choiceDialog = new ChoiceDialog<>(null, initChoices());
        choiceDialog.setTitle("Server");
        choiceDialog.setHeaderText("Choose the map to start Server");
        choiceDialog.setContentText("Map:");
    }

    private Collection initChoices() {
        List<String> choices = new ArrayList<>();
        File mapFolder = new File(Settings.MAPS_ADDRESS);
        for (File file : mapFolder.listFiles())
            if (file.getName().endsWith(".dtm"))
                choices.add(file.getName());
        return choices;
    }

    public void showAndWait() {
        Optional<String> result = choiceDialog.showAndWait();
        result.ifPresent(s -> mapName = s);
        if (!mapName.endsWith(".dtm")) {
            Platform.exit();
            System.exit(0);
        }
    }

    public String getMapName() {
        return mapName;
    }
}
