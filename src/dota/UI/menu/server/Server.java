package dota.UI.menu.server;

import dota.Settings;
import dota.UI.menu.Controller;
import dota.server.DotAServer;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Server extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = initRoot();
        primaryStage.setTitle("Dota_Server");

        primaryStage.setScene(new Scene(root, 640, 480));
        primaryStage.show();
    }

    private Parent initRoot() {
        return run();
    }

    private Controller run() {
        ServerMenu serverMenu = new ServerMenu();
        serverMenu.showAndWait();

        String mapName = serverMenu.getMapName();
        Controller controller = new Controller(true);
        MenuResponder.setController(controller);
        MenuResponder.setDotAServer(new DotAServer(Settings.MAPS_ADDRESS + mapName, controller));
        try {
            MenuResponder.getDotAServer().start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return controller;
    }
}
