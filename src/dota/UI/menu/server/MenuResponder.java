package dota.UI.menu.server;

import dota.UI.menu.Controller;
import dota.server.DotAServer;

public class MenuResponder {
    public static Controller controller;
    public static DotAServer dotAServer;

    public static void startGame() {
        dotAServer.startGame();
    }

    public static void resumeGame() {
        dotAServer.getGameRunner().resumeGame();
    }

    public static void pauseGame() {
        dotAServer.getGameRunner().pauseGame();
    }

    public static Controller getController() {
        return controller;
    }

    public static void setController(Controller controller) {
        MenuResponder.controller = controller;
    }

    public static DotAServer getDotAServer() {
        return dotAServer;
    }

    public static void setDotAServer(DotAServer dotaServer) {
        MenuResponder.dotAServer = dotaServer;
    }
}
