package dota.UI.settings;

import javafx.scene.image.Image;

public class Images {
    public static final int IMAGE_SIZE_WIDTH = 150, IMAGE_SIZE_HEIGHT = 70;
    public static final int CELLSIZE = 18, IMAGE_SMALL_SIZE = CELLSIZE / 3, IMAGE_LARGE_SIZE = CELLSIZE / 2;

    public static final String PIC_DIRECTORY = "dota/statics/pic/";
    public static final String UPGRADE_DAMAGE_URL = PIC_DIRECTORY + "DamageUpgrade.jpg";
    public static final String UPGRADE_HEALTH_URL = PIC_DIRECTORY + "HealthUpgrade.jpg";
    public static final String UPGRADE_RANGE_URL = PIC_DIRECTORY + "RangeUpgrade.jpg";
    public static final String CREATE_URL = PIC_DIRECTORY + "Create.jpg";
    public static final String DEFAULT_URL = PIC_DIRECTORY + "Default.jpg";
    public static final String HERO_URL = PIC_DIRECTORY + "Hero.jpg";
    public static final String ATTACKER_UNIT_URL = PIC_DIRECTORY + "AttackerUnit.jpg";
    public static final String DEFENCE_TOWER_URL = PIC_DIRECTORY + "DefenceTower.jpg";
    public static final String GOLDMINE_URL = PIC_DIRECTORY + "GoldMine.jpg";
    public static final String ANCIENT_URL = PIC_DIRECTORY + "Ancient.jpg";
    public static final String BARRACK_URL = PIC_DIRECTORY + "Barrack.jpg";
    public static final String INFANTRY_URL = PIC_DIRECTORY + "Infantry.jpg";
    public static final String CAVALRY_URL = PIC_DIRECTORY + "Cavalry.jpg";
    public static final String FIRE_TOWER_URL = PIC_DIRECTORY + "FireTower.jpg";
    public static final String STONE_TOWER_URL = PIC_DIRECTORY + "StoneTower.jpg";
    public static final String BLACK_TOWER_URL = PIC_DIRECTORY + "BlackTower.jpg";
    public static final String POISON_TOWER_URL = PIC_DIRECTORY + "PoisonTower.jpg";
    public static final String SENTINEL_URL = PIC_DIRECTORY + "Sentinel.jpg";
    public static final String SCOURGE_URL = PIC_DIRECTORY + "Scourge.jpg";
    public static final String PATH_URL = PIC_DIRECTORY + "Path.jpg";

    public static final Image DEFAULT_SMALL = new Image(PIC_DIRECTORY + "Default.jpg", IMAGE_SMALL_SIZE, IMAGE_SMALL_SIZE, false, true, true);
    public static final Image DEFAULT_LARGE = new Image(PIC_DIRECTORY + "Default.jpg", IMAGE_LARGE_SIZE, IMAGE_LARGE_SIZE, false, true, true);

    public static final Image PATH_SMALL = new Image(PIC_DIRECTORY + "Path.jpg", IMAGE_SMALL_SIZE, IMAGE_SMALL_SIZE, false, true, true);
    public static final Image PATH_LARGE = new Image(PIC_DIRECTORY + "Path.jpg", IMAGE_LARGE_SIZE, IMAGE_LARGE_SIZE, false, true, true);
}
