package dota.judge;

import dota.common.Cell;
import dota.common.GameObjectID;
import dota.common.exceptions.DotaExceptionBase;
import dota.game.core.GameManager;
import dota.game.utils.location.Direction;
import dota.utils.Coordinate;
import dota.utils.types.AttackerUnitTypes;
import dota.utils.types.DefenceTowerTypes;

import java.util.ArrayList;
import java.util.HashMap;

public class Judge extends JudgeAbstract {
    private GameManager gameManager;

    public Judge() {
        gameManager = new GameManager();
    }

    @Override
    public void loadMap(int columns, int rows, ArrayList<ArrayList<Cell>> path1, ArrayList<ArrayList<Cell>> path2,
                        ArrayList<ArrayList<Cell>> path3, Cell[][] ancient1, Cell[][] ancient2,
                        ArrayList<Cell[][]> barraks1, ArrayList<Cell[][]> barraks2, ArrayList<Cell> goldMines) {

        gameManager.setMapDimensions(columns, rows);

//        System.out.println("map is: x * y");
//        System.out.println(columns + " " + rows);

        ArrayList<ArrayList<ArrayList<Coordinate>>> gamePaths = new ArrayList<>();
        gamePaths.add(convertAACell(path1));
        gamePaths.add(convertAACell(path2));
        gamePaths.add(convertAACell(path3));
/*
        System.out.println("paths");
        System.out.println(gamePaths.size());
        for (ArrayList<ArrayList<Coordinate>> path : gamePaths) {
            System.out.println("new path with size");
            System.out.println(path.size());
            for (ArrayList<Coordinate> lane : path) {
                System.out.println(lane.size());
                for (Coordinate coordinate : lane)
                    System.out.println(coordinate.getX() + " " + coordinate.getY());
            }
        }
*/
        gameManager.setPaths(gamePaths);

        Coordinate[][] team1Ancient = convertCell(ancient1);
        ArrayList<Coordinate[][]> team1Barracks = convertACell2d(barraks1);
/*
        System.out.println("ancient1");
        System.out.println(team1Ancient.length + " " + team1Ancient[0].length);
        for (Coordinate[] coordinates : team1Ancient)
            for (Coordinate coordinate : coordinates)
                System.out.println(coordinate.getX() + " " + coordinate.getY());
        System.out.println("barracks1");
        System.out.println(team1Barracks.size());
        for (Coordinate[][] barrack : team1Barracks) {
            System.out.println(barrack.length + " " + barrack[0].length);
            for (Coordinate[] coordinates : barrack)
                for (Coordinate coordinate : coordinates)
                    System.out.println(coordinate.getX() + " " + coordinate.getY());
        }
*/
        gameManager.setTeam(TEAM_SENTINEL, team1Ancient, team1Barracks);

        Coordinate[][] team2Ancient = convertCell(ancient2);
        ArrayList<Coordinate[][]> team2Barracks = convertACell2d(barraks2);
/*
        System.out.println("ancient2");
        System.out.println(team1Ancient.length + " " + team1Ancient[0].length);
        for (Coordinate[] coordinates : team2Ancient)
            for (Coordinate coordinate : coordinates)
                System.out.println(coordinate.getX() + " " + coordinate.getY());
        System.out.println("barracks2");
        System.out.println(team2Barracks.size());
        for (Coordinate[][] barrack : team2Barracks) {
            System.out.println(barrack.length + " " + barrack[0].length);
            for (Coordinate[] coordinates : barrack)
                for (Coordinate coordinate : coordinates)
                    System.out.println(coordinate.getX() + " " + coordinate.getY());
        }
*/
        gameManager.setTeam(TEAM_SCOURGE, team2Ancient, team2Barracks);

        ArrayList<Coordinate> mapGoldMines = convertACell(goldMines);

/*        System.out.println("gold mines");
        System.out.println(mapGoldMines.size());
        for (Coordinate coordinate : mapGoldMines)
            System.out.println(coordinate.getX() + " " + coordinate.getY());
*/
        gameManager.addGoldMines(mapGoldMines);
    }

    private ArrayList<Coordinate[][]> convertACell2d(ArrayList<Cell[][]> cellsArray) {
        ArrayList<Coordinate[][]> arrayCoordinates = new ArrayList<>();

        for (Cell[][] cells : cellsArray)
            arrayCoordinates.add(convertCell(cells));

        return arrayCoordinates;
    }

    private ArrayList<ArrayList<Coordinate>> convertAACell(ArrayList<ArrayList<Cell>> cellsArray) {
        ArrayList<ArrayList<Coordinate>> arrayCoordinates = new ArrayList<>();

        for (ArrayList<Cell> cells : cellsArray)
            arrayCoordinates.add(convertACell(cells));

        return arrayCoordinates;
    }

    private ArrayList<Coordinate> convertACell(ArrayList<Cell> cellsArray) {
        ArrayList<Coordinate> arrayCoordinates = new ArrayList<>();

        for (Cell cell : cellsArray)
            arrayCoordinates.add(convertCell(cell));

        return arrayCoordinates;
    }

    private Coordinate convertCell(Cell cell) {
        return new Coordinate(cell.getRow(), cell.getColumn());
    }

    private Coordinate[] convertCell(Cell[] cells) {
        Coordinate[] coordinates = new Coordinate[cells.length];

        for (int i = 0; i < cells.length; i++)
            coordinates[i] = convertCell(cells[i]);

        return coordinates;
    }

    private Coordinate[][] convertCell(Cell[][] cells) {
        Coordinate[][] coordinates = new Coordinate[cells.length][];

        for (int i = 0; i < cells.length; i++)
            coordinates[i] = convertCell(cells[i]);

        return coordinates;
    }

    @Override
    public int getMapWidth() {
        return gameManager.getBoardWidth();
    }

    @Override
    public int getMapHeight() {
        return gameManager.getBoardHeight();
    }

    @Override
    public GameObjectID getGoldMineID(int goldMineNumber) throws DotaExceptionBase {
        try {
            return gameManager.getGoldMineID(goldMineNumber);
        } catch (Exception e) {
            System.err.println("Error getGoldMineID: " + e.getMessage());
            throw new DotaExceptionBase();
        }
    }

    @Override
    public GameObjectID[] getBuildingID(int teamID, int buildingType) throws DotaExceptionBase {
        if (buildingType == BUILDING_TYPE_ANCIENT)
            return new GameObjectID[]{gameManager.getTeamAncientID(teamID)};
        else if (buildingType == BUILDING_TYPE_BARRAKS)
            return gameManager.getTeamBarracksID(teamID);
        else
            throw new DotaExceptionBase();
    }

    @Override
    public GameObjectID getPathID(int pathNumber) {
        return gameManager.getPathID(pathNumber);
    }

    @Override
    public GameObjectID[] getLaneID(int pathNumber) {
        return gameManager.getPathLanesIDs(pathNumber);
    }

    @Override
    public GameObjectID getHeroID(int teamID, int heroID) {
        return gameManager.getTeamHeroID(teamID);
    }

    @Override
    public void setup() {
        gameManager.setUp();
    }

    @Override
    public GameObjectID createAttacker(int teamID, int attackerType, GameObjectID path, GameObjectID lane,
                                       int rowNumber, int colNumber) throws DotaExceptionBase {
        AttackerUnitTypes attackerUnitType;

        if (attackerType == ATTACKER_INFANTRY)
            attackerUnitType = AttackerUnitTypes.INFANTRY;
        else if (attackerType == ATTACKER_TANK)
            attackerUnitType = AttackerUnitTypes.CAVALRY;
        else
            throw new DotaExceptionBase();

        try {
            return gameManager.createAttackerUnit(teamID, attackerUnitType, lane);
        } catch (Exception e) {
            System.err.println("Error while creating Attacker unit. Message: " + e.getMessage());
            throw new DotaExceptionBase();
        }
    }

    @Override
    public GameObjectID createTower(int teamID, int towerType, GameObjectID path, GameObjectID lane, int index,
                                    int rowNumber, int colNumber) throws DotaExceptionBase {
        DefenceTowerTypes defenceTowerType;

        if (towerType == TOWER_BLACK)
            defenceTowerType = DefenceTowerTypes.BLACK_TOWER;
        else if (towerType == TOWER_FIRE)
            defenceTowerType = DefenceTowerTypes.FIRE_TOWER;
        else if (towerType == TOWER_POISON)
            defenceTowerType = DefenceTowerTypes.POISON_TOWER;
        else if (towerType == TOWER_STONE)
            defenceTowerType = DefenceTowerTypes.STONE_TOWER;
        else
            throw new DotaExceptionBase();

        try {
            return gameManager.createDefenceTower(teamID, defenceTowerType,
                    new Coordinate(rowNumber, colNumber));
        } catch (Exception e) {
            System.err.println("Error while creating Defence tower. Message: " + e.getMessage());
            throw new DotaExceptionBase();
        }
    }

    @Override
    public void purchaseAttackersPowerup(int teamID, int powerupType) throws DotaExceptionBase {

    }

    @Override
    public void purchaseTowerPowerup(int teamID, GameObjectID towerID, int powerupType) throws DotaExceptionBase {

    }

    @Override
    public GameObjectID heroMove(GameObjectID hero, Cell dest, int initDirection) throws DotaExceptionBase {
        Direction direction;
        if (initDirection == DIRECTION_DOWN)
            direction = Direction.DOWN;
        else if (initDirection == DIRECTION_LEFT)
            direction = Direction.LEFT;
        else if (initDirection == DIRECTION_RIGHT)
            direction = Direction.RIGHT;
        else if (initDirection == DIRECTION_UP)
            direction = Direction.UP;
        else
            throw new DotaExceptionBase();

        try {
            gameManager.moveHero(hero, direction);
        } catch (Exception e) {
            System.err.println("Hero move failed " + e.getMessage());
            throw new DotaExceptionBase();
        }

        return null;
    }

    @Override
    public GameObjectID heroAttack(GameObjectID hero, Cell target) throws DotaExceptionBase {
        try {
            gameManager.attackHero(hero, convertCell(target));
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return null;
    }

    @Override
    public int getMoney(int teamID) {
        return gameManager.getMoney(teamID);
    }

    @Override
    public ArrayList<Integer> getAttackerPowerups(int teamID) {
        return null;
    }

    @Override
    public ArrayList<GameObjectID> getTeamGoldMines(int teamID) throws DotaExceptionBase {
        return null;
    }

    @Override
    public HashMap<String, Integer> getInfo(GameObjectID id) throws DotaExceptionBase {
        return gameManager.getInfo(id);
    }

    @Override
    public GameObjectID[] getInRange(GameObjectID id) throws DotaExceptionBase {
        return new GameObjectID[0];
    }

    @Override
    public GameObjectID getTarget(GameObjectID id) throws DotaExceptionBase {
        return null;
    }

    @Override
    public void next50milis() {
        gameManager.nextTimeUnit();
    }

    @Override
    public void startTimer() {
    }

    @Override
    public void pauseTimer() {
    }

    @Override
    public float getTime() {
        return 0;
    }

    @Override
    public void setMoney(int teamID, int amount) {
    }

    @Override
    public void updateInfo(GameObjectID id, String infoKey, Integer infoValue) throws DotaExceptionBase {

    }

    @Override
    public void updateInfo(GameObjectID id, HashMap<String, Integer> newInfo) throws DotaExceptionBase {

    }
}
